# Add-on-Series-C

# LORA + WiFi + NRF24 + IR Reciver + 2xNeoPixel 

## Test the soldered modules working or not using the this example code, [here](https://gitlab.com/mutantC/add-on-series-c/-/blob/main/module%20tester/Add-on-Series-C%20tester.ino)


## Compatible with mutantC v3/v2/v4

[See parts list from more info](https://gitlab.com/mutantC/add-on-series-c/-/blob/main/parts_list)

It has this module support (Click the blue color name to show the module)
- [LORA](https://www.ebay.com/sch/i.html?_nkw=RFM95W) (433MHz or 900MHz)
- [ESP-32](https://www.ebay.com/sch/i.html?_nkw=esp32)
- [NRF24](https://www.ebay.com/sch/i.html?_nkw=NRF24L01+%202.4GHz%20Wireless%20RF%20Transceiver%20Module) (antenna built-in)
- [IR Reciver](https://www.aliexpress.com/item/4000184301216.html)
- [NeoPixel](https://www.ebay.com/sch/i.html?_nkw=mpu6050) (WS2812B 3535(SK6812 Mini-HS)


<img src="top.png" width="500">
<img src="bottom.png" width="500">
<img src="pic_main.png" width="500">
<img src="position.png" width="500">
<img src="show_off.png" width="500">
