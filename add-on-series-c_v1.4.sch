<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE eagle SYSTEM "eagle.dtd">
<eagle version="9.7.0">
<drawing>
<settings>
<setting alwaysvectorfont="no"/>
<setting verticaltext="up"/>
</settings>
<grid distance="2.54" unitdist="mm" unit="mm" style="lines" multiple="1" display="no" altdistance="0.254" altunitdist="mm" altunit="mm"/>
<layers>
<layer number="1" name="Top" color="4" fill="1" visible="no" active="no"/>
<layer number="2" name="Route2" color="16" fill="3" visible="no" active="no"/>
<layer number="3" name="Route3" color="17" fill="3" visible="no" active="no"/>
<layer number="4" name="Route4" color="18" fill="4" visible="no" active="no"/>
<layer number="5" name="Route5" color="19" fill="4" visible="no" active="no"/>
<layer number="6" name="Route6" color="25" fill="8" visible="no" active="no"/>
<layer number="7" name="Route7" color="26" fill="8" visible="no" active="no"/>
<layer number="8" name="Route8" color="27" fill="2" visible="no" active="no"/>
<layer number="9" name="Route9" color="28" fill="2" visible="no" active="no"/>
<layer number="10" name="Route10" color="29" fill="7" visible="no" active="no"/>
<layer number="11" name="Route11" color="30" fill="7" visible="no" active="no"/>
<layer number="12" name="Route12" color="20" fill="5" visible="no" active="no"/>
<layer number="13" name="Route13" color="21" fill="5" visible="no" active="no"/>
<layer number="14" name="Route14" color="22" fill="6" visible="no" active="no"/>
<layer number="15" name="Route15" color="23" fill="6" visible="no" active="no"/>
<layer number="16" name="Bottom" color="1" fill="1" visible="no" active="no"/>
<layer number="17" name="Pads" color="2" fill="1" visible="no" active="no"/>
<layer number="18" name="Vias" color="2" fill="1" visible="no" active="no"/>
<layer number="19" name="Unrouted" color="6" fill="1" visible="no" active="no"/>
<layer number="20" name="Dimension" color="24" fill="1" visible="no" active="no"/>
<layer number="21" name="tPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="22" name="bPlace" color="7" fill="1" visible="no" active="no"/>
<layer number="23" name="tOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="24" name="bOrigins" color="15" fill="1" visible="no" active="no"/>
<layer number="25" name="tNames" color="7" fill="1" visible="no" active="no"/>
<layer number="26" name="bNames" color="7" fill="1" visible="no" active="no"/>
<layer number="27" name="tValues" color="7" fill="1" visible="no" active="no"/>
<layer number="28" name="bValues" color="7" fill="1" visible="no" active="no"/>
<layer number="29" name="tStop" color="7" fill="3" visible="no" active="no"/>
<layer number="30" name="bStop" color="7" fill="6" visible="no" active="no"/>
<layer number="31" name="tCream" color="7" fill="4" visible="no" active="no"/>
<layer number="32" name="bCream" color="7" fill="5" visible="no" active="no"/>
<layer number="33" name="tFinish" color="6" fill="3" visible="no" active="no"/>
<layer number="34" name="bFinish" color="6" fill="6" visible="no" active="no"/>
<layer number="35" name="tGlue" color="7" fill="4" visible="no" active="no"/>
<layer number="36" name="bGlue" color="7" fill="5" visible="no" active="no"/>
<layer number="37" name="tTest" color="7" fill="1" visible="no" active="no"/>
<layer number="38" name="bTest" color="7" fill="1" visible="no" active="no"/>
<layer number="39" name="tKeepout" color="4" fill="11" visible="no" active="no"/>
<layer number="40" name="bKeepout" color="1" fill="11" visible="no" active="no"/>
<layer number="41" name="tRestrict" color="4" fill="10" visible="no" active="no"/>
<layer number="42" name="bRestrict" color="1" fill="10" visible="no" active="no"/>
<layer number="43" name="vRestrict" color="2" fill="10" visible="no" active="no"/>
<layer number="44" name="Drills" color="7" fill="1" visible="no" active="no"/>
<layer number="45" name="Holes" color="7" fill="1" visible="no" active="no"/>
<layer number="46" name="Milling" color="3" fill="1" visible="no" active="no"/>
<layer number="47" name="Measures" color="7" fill="1" visible="no" active="no"/>
<layer number="48" name="Document" color="7" fill="1" visible="no" active="no"/>
<layer number="49" name="Reference" color="7" fill="1" visible="no" active="no"/>
<layer number="50" name="dxf" color="7" fill="1" visible="no" active="no"/>
<layer number="51" name="tDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="52" name="bDocu" color="7" fill="1" visible="no" active="no"/>
<layer number="53" name="tGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="54" name="bGND_GNDA" color="7" fill="9" visible="no" active="no"/>
<layer number="56" name="wert" color="7" fill="1" visible="no" active="no"/>
<layer number="57" name="tCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="58" name="bCAD" color="7" fill="1" visible="no" active="no"/>
<layer number="59" name="tCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="60" name="bCarbon" color="7" fill="1" visible="no" active="no"/>
<layer number="61" name="stand" color="7" fill="1" visible="no" active="no"/>
<layer number="88" name="SimResults" color="9" fill="1" visible="yes" active="yes"/>
<layer number="89" name="SimProbes" color="9" fill="1" visible="yes" active="yes"/>
<layer number="90" name="Modules" color="5" fill="1" visible="yes" active="yes"/>
<layer number="91" name="Nets" color="2" fill="1" visible="yes" active="yes"/>
<layer number="92" name="Busses" color="1" fill="1" visible="yes" active="yes"/>
<layer number="93" name="Pins" color="2" fill="1" visible="no" active="yes"/>
<layer number="94" name="Symbols" color="4" fill="1" visible="yes" active="yes"/>
<layer number="95" name="Names" color="7" fill="1" visible="yes" active="yes"/>
<layer number="96" name="Values" color="7" fill="1" visible="yes" active="yes"/>
<layer number="97" name="Info" color="7" fill="1" visible="yes" active="yes"/>
<layer number="98" name="Guide" color="6" fill="1" visible="yes" active="yes"/>
<layer number="99" name="SpiceOrder" color="7" fill="1" visible="no" active="no"/>
<layer number="100" name="Muster" color="7" fill="1" visible="no" active="no"/>
<layer number="101" name="Patch_Top" color="7" fill="4" visible="no" active="yes"/>
<layer number="102" name="Vscore" color="7" fill="1" visible="no" active="yes"/>
<layer number="103" name="fp3" color="7" fill="1" visible="no" active="yes"/>
<layer number="104" name="Name" color="7" fill="1" visible="no" active="yes"/>
<layer number="105" name="Beschreib" color="7" fill="1" visible="no" active="yes"/>
<layer number="106" name="BGA-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="107" name="BD-Top" color="7" fill="1" visible="no" active="yes"/>
<layer number="108" name="fp8" color="7" fill="1" visible="no" active="yes"/>
<layer number="109" name="fp9" color="7" fill="1" visible="no" active="yes"/>
<layer number="110" name="fp0" color="7" fill="1" visible="no" active="yes"/>
<layer number="111" name="LPC17xx" color="7" fill="1" visible="no" active="yes"/>
<layer number="112" name="tSilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="113" name="ReferenceLS" color="7" fill="1" visible="no" active="no"/>
<layer number="114" name="Badge_Outline" color="7" fill="1" visible="yes" active="yes"/>
<layer number="115" name="ReferenceISLANDS" color="7" fill="1" visible="yes" active="yes"/>
<layer number="116" name="Patch_BOT" color="7" fill="4" visible="no" active="yes"/>
<layer number="117" name="mPads" color="7" fill="1" visible="yes" active="yes"/>
<layer number="118" name="Rect_Pads" color="7" fill="1" visible="no" active="no"/>
<layer number="119" name="mUnrouted" color="7" fill="1" visible="yes" active="yes"/>
<layer number="120" name="mDimension" color="7" fill="1" visible="yes" active="yes"/>
<layer number="121" name="_tsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="122" name="_bsilk" color="7" fill="1" visible="no" active="yes"/>
<layer number="123" name="tTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="124" name="bTestmark" color="7" fill="1" visible="no" active="yes"/>
<layer number="125" name="_tNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="126" name="_bNames" color="7" fill="1" visible="no" active="yes"/>
<layer number="127" name="_tValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="128" name="_bValues" color="7" fill="1" visible="no" active="yes"/>
<layer number="129" name="Mask" color="7" fill="1" visible="yes" active="yes"/>
<layer number="130" name="mbStop" color="7" fill="1" visible="yes" active="yes"/>
<layer number="131" name="tAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="132" name="bAdjust" color="7" fill="1" visible="no" active="yes"/>
<layer number="133" name="mtFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="134" name="mbFinish" color="7" fill="1" visible="yes" active="yes"/>
<layer number="135" name="mtGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="136" name="mbGlue" color="7" fill="1" visible="yes" active="yes"/>
<layer number="137" name="mtTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="138" name="mbTest" color="7" fill="1" visible="yes" active="yes"/>
<layer number="139" name="mtKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="140" name="mbKeepout" color="7" fill="1" visible="yes" active="yes"/>
<layer number="141" name="mtRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="142" name="mbRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="143" name="mvRestrict" color="7" fill="1" visible="yes" active="yes"/>
<layer number="144" name="Drill_legend" color="7" fill="1" visible="no" active="yes"/>
<layer number="145" name="mHoles" color="7" fill="1" visible="yes" active="yes"/>
<layer number="146" name="mMilling" color="7" fill="1" visible="yes" active="yes"/>
<layer number="147" name="mMeasures" color="7" fill="1" visible="yes" active="yes"/>
<layer number="148" name="mDocument" color="7" fill="1" visible="yes" active="yes"/>
<layer number="149" name="mReference" color="7" fill="1" visible="yes" active="yes"/>
<layer number="150" name="Notes" color="7" fill="1" visible="no" active="yes"/>
<layer number="151" name="HeatSink" color="7" fill="1" visible="no" active="yes"/>
<layer number="152" name="_bDocu" color="7" fill="1" visible="no" active="yes"/>
<layer number="153" name="FabDoc1" color="7" fill="1" visible="no" active="no"/>
<layer number="154" name="FabDoc2" color="7" fill="1" visible="no" active="no"/>
<layer number="155" name="FabDoc3" color="7" fill="15" visible="no" active="no"/>
<layer number="166" name="AntennaArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="168" name="4mmHeightArea" color="7" fill="1" visible="yes" active="yes"/>
<layer number="191" name="mNets" color="7" fill="1" visible="yes" active="yes"/>
<layer number="192" name="mBusses" color="7" fill="1" visible="yes" active="yes"/>
<layer number="193" name="mPins" color="7" fill="1" visible="yes" active="yes"/>
<layer number="194" name="mSymbols" color="7" fill="1" visible="yes" active="yes"/>
<layer number="195" name="mNames" color="7" fill="1" visible="yes" active="yes"/>
<layer number="196" name="mValues" color="7" fill="1" visible="yes" active="yes"/>
<layer number="199" name="Contour" color="7" fill="1" visible="no" active="yes"/>
<layer number="200" name="200bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="201" name="201bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="202" name="202bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="203" name="203bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="204" name="204bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="205" name="205bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="206" name="206bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="207" name="207bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="208" name="208bmp" color="7" fill="10" visible="no" active="yes"/>
<layer number="209" name="209bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="210" name="210bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="211" name="211bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="212" name="212bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="213" name="213bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="214" name="214bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="215" name="215bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="216" name="216bmp" color="7" fill="1" visible="no" active="yes"/>
<layer number="217" name="217bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="218" name="218bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="219" name="219bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="220" name="220bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="221" name="221bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="222" name="222bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="223" name="223bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="224" name="224bmp" color="7" fill="1" visible="no" active="no"/>
<layer number="225" name="225bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="226" name="226bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="227" name="227bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="228" name="228bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="229" name="229bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="230" name="230bmp" color="7" fill="1" visible="yes" active="yes"/>
<layer number="231" name="Eagle3D_PG1" color="7" fill="1" visible="no" active="no"/>
<layer number="232" name="Eagle3D_PG2" color="7" fill="1" visible="no" active="no"/>
<layer number="233" name="Eagle3D_PG3" color="7" fill="1" visible="no" active="no"/>
<layer number="248" name="Housing" color="7" fill="1" visible="no" active="yes"/>
<layer number="249" name="Edge" color="7" fill="1" visible="no" active="yes"/>
<layer number="250" name="Descript" color="7" fill="1" visible="yes" active="yes"/>
<layer number="251" name="SMDround" color="7" fill="1" visible="yes" active="yes"/>
<layer number="254" name="cooling" color="7" fill="1" visible="no" active="yes"/>
<layer number="255" name="routoute" color="7" fill="1" visible="yes" active="yes"/>
</layers>
<schematic xreflabel="%F%N/%S.%C%R" xrefpart="/%S.%C%R">
<libraries>
<library name="ch" urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A">
<packages>
<package name="MUTANTC_TEXT">
<rectangle x1="4.99" y1="-0.65" x2="5.09" y2="-0.55" layer="25"/>
<rectangle x1="10.49" y1="-0.65" x2="10.59" y2="-0.55" layer="25"/>
<rectangle x1="4.99" y1="-0.55" x2="5.29" y2="-0.45" layer="25"/>
<rectangle x1="10.49" y1="-0.55" x2="10.79" y2="-0.45" layer="25"/>
<rectangle x1="4.99" y1="-0.45" x2="5.39" y2="-0.35" layer="25"/>
<rectangle x1="10.49" y1="-0.45" x2="10.99" y2="-0.35" layer="25"/>
<rectangle x1="4.99" y1="-0.35" x2="5.39" y2="-0.25" layer="25"/>
<rectangle x1="10.49" y1="-0.35" x2="10.99" y2="-0.25" layer="25"/>
<rectangle x1="4.99" y1="-0.25" x2="5.39" y2="-0.15" layer="25"/>
<rectangle x1="10.49" y1="-0.25" x2="10.99" y2="-0.15" layer="25"/>
<rectangle x1="4.99" y1="-0.15" x2="5.39" y2="-0.05" layer="25"/>
<rectangle x1="10.49" y1="-0.15" x2="10.99" y2="-0.05" layer="25"/>
<rectangle x1="4.99" y1="-0.05" x2="5.39" y2="0.05" layer="25"/>
<rectangle x1="10.49" y1="-0.05" x2="10.99" y2="0.05" layer="25"/>
<rectangle x1="4.99" y1="0.05" x2="5.39" y2="0.15" layer="25"/>
<rectangle x1="10.49" y1="0.05" x2="10.99" y2="0.15" layer="25"/>
<rectangle x1="0.09" y1="0.15" x2="0.39" y2="0.25" layer="25"/>
<rectangle x1="3.29" y1="0.05" x2="4.39" y2="0.15" layer="25"/>
<rectangle x1="4.99" y1="0.15" x2="5.39" y2="0.25" layer="25"/>
<rectangle x1="7.19" y1="0.15" x2="7.49" y2="0.25" layer="25"/>
<rectangle x1="9.09" y1="0.15" x2="9.39" y2="0.25" layer="25"/>
<rectangle x1="10.49" y1="0.15" x2="10.99" y2="0.25" layer="25"/>
<rectangle x1="12.59" y1="0.15" x2="13.19" y2="0.25" layer="25"/>
<rectangle x1="0.09" y1="0.25" x2="0.59" y2="0.35" layer="25"/>
<rectangle x1="3.19" y1="0.15" x2="4.49" y2="0.25" layer="25"/>
<rectangle x1="4.99" y1="0.25" x2="5.39" y2="0.35" layer="25"/>
<rectangle x1="6.39" y1="0.25" x2="7.69" y2="0.35" layer="25"/>
<rectangle x1="7.99" y1="0.25" x2="8.39" y2="0.35" layer="25"/>
<rectangle x1="9.09" y1="0.25" x2="9.59" y2="0.35" layer="25"/>
<rectangle x1="10.49" y1="0.25" x2="10.99" y2="0.35" layer="25"/>
<rectangle x1="12.19" y1="0.25" x2="13.59" y2="0.35" layer="25"/>
<rectangle x1="0.09" y1="0.35" x2="0.59" y2="0.45" layer="25"/>
<rectangle x1="3.09" y1="0.25" x2="4.59" y2="0.35" layer="25"/>
<rectangle x1="4.99" y1="0.35" x2="5.39" y2="0.45" layer="25"/>
<rectangle x1="6.29" y1="0.35" x2="7.69" y2="0.45" layer="25"/>
<rectangle x1="7.99" y1="0.35" x2="8.39" y2="0.45" layer="25"/>
<rectangle x1="9.09" y1="0.35" x2="9.59" y2="0.45" layer="25"/>
<rectangle x1="10.49" y1="0.35" x2="10.99" y2="0.45" layer="25"/>
<rectangle x1="12.09" y1="0.35" x2="13.69" y2="0.45" layer="25"/>
<rectangle x1="0.09" y1="0.45" x2="0.59" y2="0.55" layer="25"/>
<rectangle x1="3.09" y1="0.35" x2="4.59" y2="0.45" layer="25"/>
<rectangle x1="4.99" y1="0.45" x2="5.39" y2="0.55" layer="25"/>
<rectangle x1="6.19" y1="0.45" x2="7.69" y2="0.55" layer="25"/>
<rectangle x1="7.99" y1="0.45" x2="8.39" y2="0.55" layer="25"/>
<rectangle x1="9.09" y1="0.45" x2="9.59" y2="0.55" layer="25"/>
<rectangle x1="10.49" y1="0.45" x2="10.99" y2="0.55" layer="25"/>
<rectangle x1="11.99" y1="0.45" x2="13.79" y2="0.55" layer="25"/>
<rectangle x1="0.09" y1="0.55" x2="0.59" y2="0.65" layer="25"/>
<rectangle x1="3.09" y1="0.45" x2="3.49" y2="0.55" layer="25"/>
<rectangle x1="4.19" y1="0.45" x2="4.59" y2="0.55" layer="25"/>
<rectangle x1="4.99" y1="0.55" x2="5.39" y2="0.65" layer="25"/>
<rectangle x1="6.19" y1="0.55" x2="7.69" y2="0.65" layer="25"/>
<rectangle x1="7.99" y1="0.55" x2="8.39" y2="0.65" layer="25"/>
<rectangle x1="9.09" y1="0.55" x2="9.59" y2="0.65" layer="25"/>
<rectangle x1="10.49" y1="0.55" x2="10.99" y2="0.65" layer="25"/>
<rectangle x1="11.99" y1="0.55" x2="13.79" y2="0.65" layer="25"/>
<rectangle x1="0.09" y1="0.65" x2="0.59" y2="0.75" layer="25"/>
<rectangle x1="3.09" y1="0.55" x2="3.49" y2="0.65" layer="25"/>
<rectangle x1="4.19" y1="0.55" x2="4.59" y2="0.65" layer="25"/>
<rectangle x1="4.99" y1="0.65" x2="5.39" y2="0.75" layer="25"/>
<rectangle x1="6.19" y1="0.65" x2="6.59" y2="0.75" layer="25"/>
<rectangle x1="7.19" y1="0.65" x2="7.69" y2="0.75" layer="25"/>
<rectangle x1="7.99" y1="0.65" x2="8.39" y2="0.75" layer="25"/>
<rectangle x1="9.09" y1="0.65" x2="9.59" y2="0.75" layer="25"/>
<rectangle x1="10.49" y1="0.65" x2="10.99" y2="0.75" layer="25"/>
<rectangle x1="11.99" y1="0.65" x2="12.39" y2="0.75" layer="25"/>
<rectangle x1="13.39" y1="0.65" x2="13.79" y2="0.75" layer="25"/>
<rectangle x1="0.09" y1="0.75" x2="0.59" y2="0.85" layer="25"/>
<rectangle x1="3.09" y1="0.65" x2="3.49" y2="0.75" layer="25"/>
<rectangle x1="4.19" y1="0.65" x2="4.59" y2="0.75" layer="25"/>
<rectangle x1="4.99" y1="0.75" x2="5.39" y2="0.85" layer="25"/>
<rectangle x1="6.19" y1="0.75" x2="6.59" y2="0.85" layer="25"/>
<rectangle x1="7.19" y1="0.75" x2="7.69" y2="0.85" layer="25"/>
<rectangle x1="7.99" y1="0.75" x2="8.39" y2="0.85" layer="25"/>
<rectangle x1="9.09" y1="0.75" x2="9.59" y2="0.85" layer="25"/>
<rectangle x1="10.49" y1="0.75" x2="10.99" y2="0.85" layer="25"/>
<rectangle x1="11.99" y1="0.75" x2="12.39" y2="0.85" layer="25"/>
<rectangle x1="0.09" y1="0.85" x2="0.59" y2="0.95" layer="25"/>
<rectangle x1="3.09" y1="0.75" x2="3.49" y2="0.85" layer="25"/>
<rectangle x1="4.19" y1="0.75" x2="4.59" y2="0.85" layer="25"/>
<rectangle x1="4.99" y1="0.85" x2="5.39" y2="0.95" layer="25"/>
<rectangle x1="6.19" y1="0.85" x2="6.59" y2="0.95" layer="25"/>
<rectangle x1="7.19" y1="0.85" x2="7.69" y2="0.95" layer="25"/>
<rectangle x1="7.99" y1="0.85" x2="8.39" y2="0.95" layer="25"/>
<rectangle x1="9.09" y1="0.85" x2="9.59" y2="0.95" layer="25"/>
<rectangle x1="10.49" y1="0.85" x2="10.99" y2="0.95" layer="25"/>
<rectangle x1="11.99" y1="0.85" x2="12.39" y2="0.95" layer="25"/>
<rectangle x1="0.09" y1="0.95" x2="0.59" y2="1.05" layer="25"/>
<rectangle x1="3.09" y1="0.85" x2="3.49" y2="0.95" layer="25"/>
<rectangle x1="4.19" y1="0.85" x2="4.59" y2="0.95" layer="25"/>
<rectangle x1="4.99" y1="0.95" x2="5.39" y2="1.05" layer="25"/>
<rectangle x1="6.19" y1="0.95" x2="6.59" y2="1.05" layer="25"/>
<rectangle x1="7.19" y1="0.95" x2="7.69" y2="1.05" layer="25"/>
<rectangle x1="7.99" y1="0.95" x2="8.39" y2="1.05" layer="25"/>
<rectangle x1="11.99" y1="0.95" x2="12.39" y2="1.05" layer="25"/>
<rectangle x1="0.09" y1="1.05" x2="0.59" y2="1.15" layer="25"/>
<rectangle x1="1.29" y1="1.05" x2="1.69" y2="1.15" layer="25"/>
<rectangle x1="2.39" y1="1.05" x2="2.89" y2="1.15" layer="25"/>
<rectangle x1="3.09" y1="0.95" x2="3.49" y2="1.05" layer="25"/>
<rectangle x1="4.19" y1="0.95" x2="4.59" y2="1.05" layer="25"/>
<rectangle x1="4.99" y1="1.05" x2="5.39" y2="1.15" layer="25"/>
<rectangle x1="6.19" y1="1.05" x2="6.59" y2="1.15" layer="25"/>
<rectangle x1="7.19" y1="1.05" x2="7.69" y2="1.15" layer="25"/>
<rectangle x1="7.99" y1="1.05" x2="8.39" y2="1.15" layer="25"/>
<rectangle x1="11.99" y1="1.05" x2="12.39" y2="1.15" layer="25"/>
<rectangle x1="0.09" y1="1.15" x2="0.59" y2="1.25" layer="25"/>
<rectangle x1="1.29" y1="1.15" x2="1.69" y2="1.25" layer="25"/>
<rectangle x1="2.39" y1="1.15" x2="2.89" y2="1.25" layer="25"/>
<rectangle x1="3.09" y1="1.05" x2="3.49" y2="1.15" layer="25"/>
<rectangle x1="4.19" y1="1.05" x2="4.59" y2="1.15" layer="25"/>
<rectangle x1="4.99" y1="1.15" x2="5.39" y2="1.25" layer="25"/>
<rectangle x1="6.19" y1="1.15" x2="6.69" y2="1.25" layer="25"/>
<rectangle x1="7.09" y1="1.15" x2="7.69" y2="1.25" layer="25"/>
<rectangle x1="7.99" y1="1.15" x2="8.39" y2="1.25" layer="25"/>
<rectangle x1="11.99" y1="1.15" x2="12.39" y2="1.25" layer="25"/>
<rectangle x1="13.49" y1="1.15" x2="13.79" y2="1.25" layer="25"/>
<rectangle x1="0.09" y1="1.25" x2="0.59" y2="1.35" layer="25"/>
<rectangle x1="1.29" y1="1.25" x2="1.69" y2="1.35" layer="25"/>
<rectangle x1="2.39" y1="1.25" x2="2.89" y2="1.35" layer="25"/>
<rectangle x1="3.09" y1="1.15" x2="3.49" y2="1.25" layer="25"/>
<rectangle x1="4.99" y1="1.25" x2="5.39" y2="1.35" layer="25"/>
<rectangle x1="6.19" y1="1.25" x2="7.69" y2="1.35" layer="25"/>
<rectangle x1="7.99" y1="1.25" x2="8.39" y2="1.35" layer="25"/>
<rectangle x1="11.99" y1="1.25" x2="12.39" y2="1.35" layer="25"/>
<rectangle x1="0.09" y1="1.35" x2="0.59" y2="1.45" layer="25"/>
<rectangle x1="1.29" y1="1.35" x2="1.69" y2="1.45" layer="25"/>
<rectangle x1="2.39" y1="1.35" x2="2.89" y2="1.45" layer="25"/>
<rectangle x1="3.09" y1="1.25" x2="3.49" y2="1.35" layer="25"/>
<rectangle x1="4.99" y1="1.35" x2="5.39" y2="1.45" layer="25"/>
<rectangle x1="6.19" y1="1.35" x2="7.69" y2="1.45" layer="25"/>
<rectangle x1="7.99" y1="1.35" x2="8.39" y2="1.45" layer="25"/>
<rectangle x1="9.09" y1="1.35" x2="9.59" y2="1.45" layer="25"/>
<rectangle x1="10.49" y1="1.35" x2="10.99" y2="1.45" layer="25"/>
<rectangle x1="11.99" y1="1.35" x2="12.39" y2="1.45" layer="25"/>
<rectangle x1="0.09" y1="1.45" x2="0.59" y2="1.55" layer="25"/>
<rectangle x1="1.29" y1="1.45" x2="1.69" y2="1.55" layer="25"/>
<rectangle x1="2.39" y1="1.45" x2="2.89" y2="1.55" layer="25"/>
<rectangle x1="3.09" y1="1.35" x2="3.49" y2="1.45" layer="25"/>
<rectangle x1="4.99" y1="1.45" x2="5.39" y2="1.55" layer="25"/>
<rectangle x1="6.29" y1="1.45" x2="7.69" y2="1.55" layer="25"/>
<rectangle x1="7.99" y1="1.45" x2="8.39" y2="1.55" layer="25"/>
<rectangle x1="9.09" y1="1.45" x2="9.59" y2="1.55" layer="25"/>
<rectangle x1="10.49" y1="1.45" x2="10.99" y2="1.55" layer="25"/>
<rectangle x1="11.99" y1="1.45" x2="12.39" y2="1.55" layer="25"/>
<rectangle x1="0.09" y1="1.55" x2="0.59" y2="1.65" layer="25"/>
<rectangle x1="1.29" y1="1.55" x2="1.69" y2="1.65" layer="25"/>
<rectangle x1="2.39" y1="1.55" x2="2.89" y2="1.65" layer="25"/>
<rectangle x1="3.09" y1="1.45" x2="3.49" y2="1.55" layer="25"/>
<rectangle x1="4.99" y1="1.55" x2="5.39" y2="1.65" layer="25"/>
<rectangle x1="6.49" y1="1.55" x2="7.69" y2="1.65" layer="25"/>
<rectangle x1="7.99" y1="1.55" x2="8.39" y2="1.65" layer="25"/>
<rectangle x1="9.09" y1="1.55" x2="9.59" y2="1.65" layer="25"/>
<rectangle x1="10.49" y1="1.55" x2="10.99" y2="1.65" layer="25"/>
<rectangle x1="11.99" y1="1.55" x2="12.39" y2="1.65" layer="25"/>
<rectangle x1="0.09" y1="1.65" x2="0.59" y2="1.75" layer="25"/>
<rectangle x1="1.29" y1="1.65" x2="1.69" y2="1.75" layer="25"/>
<rectangle x1="2.39" y1="1.65" x2="2.89" y2="1.75" layer="25"/>
<rectangle x1="3.09" y1="1.55" x2="3.49" y2="1.65" layer="25"/>
<rectangle x1="4.99" y1="1.65" x2="5.39" y2="1.75" layer="25"/>
<rectangle x1="7.19" y1="1.65" x2="7.69" y2="1.75" layer="25"/>
<rectangle x1="7.99" y1="1.65" x2="8.49" y2="1.75" layer="25"/>
<rectangle x1="9.09" y1="1.65" x2="9.59" y2="1.75" layer="25"/>
<rectangle x1="10.49" y1="1.65" x2="10.99" y2="1.75" layer="25"/>
<rectangle x1="11.99" y1="1.65" x2="12.39" y2="1.75" layer="25"/>
<rectangle x1="0.09" y1="1.75" x2="0.59" y2="1.85" layer="25"/>
<rectangle x1="1.19" y1="1.75" x2="1.79" y2="1.85" layer="25"/>
<rectangle x1="2.39" y1="1.75" x2="2.79" y2="1.85" layer="25"/>
<rectangle x1="3.09" y1="1.65" x2="3.49" y2="1.75" layer="25"/>
<rectangle x1="4.99" y1="1.75" x2="5.39" y2="1.85" layer="25"/>
<rectangle x1="7.19" y1="1.75" x2="7.69" y2="1.85" layer="25"/>
<rectangle x1="7.99" y1="1.75" x2="8.49" y2="1.85" layer="25"/>
<rectangle x1="9.09" y1="1.75" x2="9.59" y2="1.85" layer="25"/>
<rectangle x1="10.49" y1="1.75" x2="10.99" y2="1.85" layer="25"/>
<rectangle x1="11.99" y1="1.75" x2="12.39" y2="1.85" layer="25"/>
<rectangle x1="0.19" y1="1.85" x2="2.79" y2="1.95" layer="25"/>
<rectangle x1="3.09" y1="1.75" x2="3.49" y2="1.85" layer="25"/>
<rectangle x1="4.69" y1="1.85" x2="6.09" y2="1.95" layer="25"/>
<rectangle x1="6.39" y1="1.85" x2="7.69" y2="1.95" layer="25"/>
<rectangle x1="7.99" y1="1.85" x2="9.59" y2="1.95" layer="25"/>
<rectangle x1="9.79" y1="1.85" x2="11.59" y2="1.95" layer="25"/>
<rectangle x1="11.99" y1="1.85" x2="12.39" y2="1.95" layer="25"/>
<rectangle x1="0.19" y1="1.95" x2="2.79" y2="2.05" layer="25"/>
<rectangle x1="3.09" y1="1.85" x2="3.49" y2="1.95" layer="25"/>
<rectangle x1="6.49" y1="1.95" x2="7.59" y2="2.05" layer="25"/>
<rectangle x1="8.09" y1="1.95" x2="9.49" y2="2.05" layer="25"/>
<rectangle x1="9.79" y1="1.95" x2="11.69" y2="2.05" layer="25"/>
<rectangle x1="11.99" y1="1.95" x2="12.39" y2="2.05" layer="25"/>
<rectangle x1="13.39" y1="1.95" x2="13.59" y2="2.05" layer="25"/>
<rectangle x1="0.29" y1="2.05" x2="2.69" y2="2.15" layer="25"/>
<rectangle x1="3.09" y1="1.95" x2="3.49" y2="2.05" layer="25"/>
<rectangle x1="4.69" y1="2.05" x2="6.19" y2="2.15" layer="25"/>
<rectangle x1="6.49" y1="2.05" x2="7.59" y2="2.15" layer="25"/>
<rectangle x1="8.09" y1="2.05" x2="9.49" y2="2.15" layer="25"/>
<rectangle x1="9.79" y1="2.05" x2="11.69" y2="2.15" layer="25"/>
<rectangle x1="11.99" y1="2.05" x2="12.39" y2="2.15" layer="25"/>
<rectangle x1="0.39" y1="2.15" x2="1.39" y2="2.25" layer="25"/>
<rectangle x1="1.59" y1="2.15" x2="2.59" y2="2.25" layer="25"/>
<rectangle x1="4.69" y1="2.15" x2="6.19" y2="2.25" layer="25"/>
<rectangle x1="6.59" y1="2.15" x2="7.49" y2="2.25" layer="25"/>
<rectangle x1="8.19" y1="2.15" x2="9.29" y2="2.25" layer="25"/>
<rectangle x1="9.89" y1="2.15" x2="11.69" y2="2.25" layer="25"/>
<rectangle x1="11.99" y1="2.15" x2="12.39" y2="2.25" layer="25"/>
<rectangle x1="11.99" y1="2.25" x2="12.39" y2="2.35" layer="25"/>
<rectangle x1="11.99" y1="2.35" x2="12.39" y2="2.45" layer="25"/>
<rectangle x1="11.99" y1="2.45" x2="12.39" y2="2.55" layer="25"/>
<rectangle x1="13.39" y1="2.45" x2="13.79" y2="2.55" layer="25"/>
<rectangle x1="11.99" y1="2.55" x2="13.79" y2="2.65" layer="25"/>
<rectangle x1="11.99" y1="2.65" x2="13.79" y2="2.75" layer="25"/>
<rectangle x1="12.09" y1="2.75" x2="13.69" y2="2.85" layer="25"/>
<rectangle x1="12.19" y1="2.85" x2="13.59" y2="2.95" layer="25"/>
<rectangle x1="12.49" y1="2.95" x2="13.29" y2="3.05" layer="25"/>
<rectangle x1="7.19" y1="0.05" x2="7.29" y2="0.15" layer="25"/>
<rectangle x1="7.99" y1="0.05" x2="8.09" y2="0.15" layer="25"/>
<rectangle x1="9.09" y1="0.05" x2="9.19" y2="0.15" layer="25"/>
<rectangle x1="0.09" y1="0.05" x2="0.19" y2="0.15" layer="25"/>
<rectangle x1="3.39" y1="2.15" x2="3.49" y2="2.25" layer="25"/>
<rectangle x1="6.19" y1="2.15" x2="6.29" y2="2.25" layer="25"/>
<rectangle x1="9.79" y1="2.15" x2="9.89" y2="2.25" layer="25"/>
<rectangle x1="11.69" y1="2.15" x2="11.79" y2="2.25" layer="25"/>
<rectangle x1="13.39" y1="1.85" x2="13.49" y2="1.95" layer="25"/>
<rectangle x1="13.69" y1="1.25" x2="13.79" y2="1.35" layer="25"/>
<rectangle x1="13.39" y1="2.35" x2="13.79" y2="2.45" layer="25"/>
<rectangle x1="13.39" y1="2.25" x2="13.79" y2="2.35" layer="25"/>
<rectangle x1="13.39" y1="2.15" x2="13.79" y2="2.25" layer="25"/>
<rectangle x1="13.39" y1="2.05" x2="13.79" y2="2.15" layer="25"/>
<rectangle x1="13.39" y1="0.75" x2="13.79" y2="0.85" layer="25"/>
<rectangle x1="13.39" y1="0.85" x2="13.79" y2="0.95" layer="25"/>
<rectangle x1="13.39" y1="0.95" x2="13.79" y2="1.05" layer="25"/>
<rectangle x1="13.39" y1="1.05" x2="13.79" y2="1.15" layer="25"/>
<rectangle x1="13.29" y1="0.65" x2="13.39" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="0.65" x2="12.49" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="0.75" x2="12.49" y2="0.85" layer="25"/>
<rectangle x1="12.49" y1="0.65" x2="12.59" y2="0.75" layer="25"/>
<rectangle x1="12.39" y1="2.45" x2="12.49" y2="2.55" layer="25"/>
<rectangle x1="12.39" y1="2.35" x2="12.49" y2="2.45" layer="25"/>
<rectangle x1="12.49" y1="2.45" x2="12.59" y2="2.55" layer="25"/>
<rectangle x1="13.29" y1="2.45" x2="13.39" y2="2.55" layer="25"/>
<rectangle x1="13.59" y1="1.95" x2="13.69" y2="2.05" layer="25"/>
<rectangle x1="4.69" y1="1.95" x2="6.19" y2="2.05" layer="25"/>
<rectangle x1="3.19" y1="2.05" x2="3.49" y2="2.15" layer="25"/>
<rectangle x1="7.99" y1="0.15" x2="8.29" y2="0.25" layer="25"/>
<rectangle x1="12.39" y1="0.15" x2="12.69" y2="0.25" layer="25"/>
<rectangle x1="12.39" y1="0.85" x2="12.49" y2="0.95" layer="25"/>
<rectangle x1="12.39" y1="2.27" x2="12.49" y2="2.37" layer="25"/>
</package>
<package name="MUTANTC_LOGO">
<rectangle x1="1.87" y1="0.05" x2="1.93" y2="0.07" layer="25"/>
<rectangle x1="1.61" y1="0.07" x2="2.17" y2="0.09" layer="25"/>
<rectangle x1="1.51" y1="0.09" x2="2.27" y2="0.11" layer="25"/>
<rectangle x1="1.43" y1="0.11" x2="2.35" y2="0.13" layer="25"/>
<rectangle x1="1.35" y1="0.13" x2="2.41" y2="0.15" layer="25"/>
<rectangle x1="1.29" y1="0.15" x2="2.47" y2="0.17" layer="25"/>
<rectangle x1="1.25" y1="0.17" x2="2.53" y2="0.19" layer="25"/>
<rectangle x1="1.19" y1="0.19" x2="2.59" y2="0.21" layer="25"/>
<rectangle x1="1.15" y1="0.21" x2="2.63" y2="0.23" layer="25"/>
<rectangle x1="1.11" y1="0.23" x2="2.67" y2="0.25" layer="25"/>
<rectangle x1="1.07" y1="0.25" x2="2.71" y2="0.27" layer="25"/>
<rectangle x1="1.03" y1="0.27" x2="1.31" y2="0.29" layer="25"/>
<rectangle x1="1.53" y1="0.27" x2="2.75" y2="0.29" layer="25"/>
<rectangle x1="0.99" y1="0.29" x2="1.21" y2="0.31" layer="25"/>
<rectangle x1="1.63" y1="0.29" x2="2.79" y2="0.31" layer="25"/>
<rectangle x1="0.95" y1="0.31" x2="1.15" y2="0.33" layer="25"/>
<rectangle x1="1.69" y1="0.31" x2="2.81" y2="0.33" layer="25"/>
<rectangle x1="0.93" y1="0.33" x2="1.11" y2="0.35" layer="25"/>
<rectangle x1="1.73" y1="0.33" x2="2.85" y2="0.35" layer="25"/>
<rectangle x1="0.89" y1="0.35" x2="1.05" y2="0.37" layer="25"/>
<rectangle x1="1.77" y1="0.35" x2="2.87" y2="0.37" layer="25"/>
<rectangle x1="0.87" y1="0.37" x2="1.03" y2="0.39" layer="25"/>
<rectangle x1="1.81" y1="0.37" x2="2.91" y2="0.39" layer="25"/>
<rectangle x1="0.83" y1="0.39" x2="0.99" y2="0.41" layer="25"/>
<rectangle x1="1.85" y1="0.39" x2="2.93" y2="0.41" layer="25"/>
<rectangle x1="0.81" y1="0.41" x2="0.95" y2="0.43" layer="25"/>
<rectangle x1="1.87" y1="0.41" x2="2.97" y2="0.43" layer="25"/>
<rectangle x1="0.79" y1="0.43" x2="0.93" y2="0.45" layer="25"/>
<rectangle x1="1.89" y1="0.43" x2="2.99" y2="0.45" layer="25"/>
<rectangle x1="0.75" y1="0.45" x2="0.91" y2="0.47" layer="25"/>
<rectangle x1="1.93" y1="0.45" x2="3.01" y2="0.47" layer="25"/>
<rectangle x1="0.73" y1="0.47" x2="0.89" y2="0.49" layer="25"/>
<rectangle x1="1.95" y1="0.47" x2="3.03" y2="0.49" layer="25"/>
<rectangle x1="0.71" y1="0.49" x2="0.87" y2="0.51" layer="25"/>
<rectangle x1="1.97" y1="0.49" x2="3.05" y2="0.51" layer="25"/>
<rectangle x1="0.69" y1="0.51" x2="0.85" y2="0.53" layer="25"/>
<rectangle x1="1.99" y1="0.51" x2="3.09" y2="0.53" layer="25"/>
<rectangle x1="0.67" y1="0.53" x2="0.83" y2="0.55" layer="25"/>
<rectangle x1="2.01" y1="0.53" x2="3.11" y2="0.55" layer="25"/>
<rectangle x1="0.65" y1="0.55" x2="0.81" y2="0.57" layer="25"/>
<rectangle x1="2.01" y1="0.55" x2="3.13" y2="0.57" layer="25"/>
<rectangle x1="0.63" y1="0.57" x2="0.79" y2="0.59" layer="25"/>
<rectangle x1="2.03" y1="0.57" x2="3.15" y2="0.59" layer="25"/>
<rectangle x1="0.61" y1="0.59" x2="0.77" y2="0.61" layer="25"/>
<rectangle x1="2.05" y1="0.59" x2="3.17" y2="0.61" layer="25"/>
<rectangle x1="0.59" y1="0.61" x2="0.77" y2="0.63" layer="25"/>
<rectangle x1="2.07" y1="0.61" x2="3.19" y2="0.63" layer="25"/>
<rectangle x1="0.57" y1="0.63" x2="0.75" y2="0.65" layer="25"/>
<rectangle x1="2.07" y1="0.63" x2="3.21" y2="0.65" layer="25"/>
<rectangle x1="0.55" y1="0.65" x2="0.73" y2="0.67" layer="25"/>
<rectangle x1="2.09" y1="0.65" x2="3.21" y2="0.67" layer="25"/>
<rectangle x1="0.53" y1="0.67" x2="0.73" y2="0.69" layer="25"/>
<rectangle x1="2.09" y1="0.67" x2="3.23" y2="0.69" layer="25"/>
<rectangle x1="0.51" y1="0.69" x2="0.71" y2="0.71" layer="25"/>
<rectangle x1="2.11" y1="0.69" x2="3.25" y2="0.71" layer="25"/>
<rectangle x1="0.49" y1="0.71" x2="0.71" y2="0.73" layer="25"/>
<rectangle x1="2.11" y1="0.71" x2="3.27" y2="0.73" layer="25"/>
<rectangle x1="0.47" y1="0.73" x2="0.69" y2="0.75" layer="25"/>
<rectangle x1="2.13" y1="0.73" x2="3.29" y2="0.75" layer="25"/>
<rectangle x1="0.45" y1="0.75" x2="0.69" y2="0.77" layer="25"/>
<rectangle x1="2.13" y1="0.75" x2="3.31" y2="0.77" layer="25"/>
<rectangle x1="0.45" y1="0.77" x2="0.67" y2="0.79" layer="25"/>
<rectangle x1="2.13" y1="0.77" x2="3.31" y2="0.79" layer="25"/>
<rectangle x1="0.43" y1="0.79" x2="0.67" y2="0.81" layer="25"/>
<rectangle x1="2.15" y1="0.79" x2="3.33" y2="0.81" layer="25"/>
<rectangle x1="0.41" y1="0.81" x2="0.65" y2="0.83" layer="25"/>
<rectangle x1="2.15" y1="0.81" x2="2.67" y2="0.83" layer="25"/>
<rectangle x1="2.71" y1="0.81" x2="3.35" y2="0.83" layer="25"/>
<rectangle x1="0.41" y1="0.83" x2="0.65" y2="0.85" layer="25"/>
<rectangle x1="2.15" y1="0.83" x2="2.65" y2="0.85" layer="25"/>
<rectangle x1="2.75" y1="0.83" x2="3.37" y2="0.85" layer="25"/>
<rectangle x1="0.39" y1="0.85" x2="0.65" y2="0.87" layer="25"/>
<rectangle x1="2.17" y1="0.85" x2="2.63" y2="0.87" layer="25"/>
<rectangle x1="2.77" y1="0.85" x2="3.37" y2="0.87" layer="25"/>
<rectangle x1="0.37" y1="0.87" x2="0.65" y2="0.89" layer="25"/>
<rectangle x1="2.17" y1="0.87" x2="2.61" y2="0.89" layer="25"/>
<rectangle x1="2.81" y1="0.87" x2="3.39" y2="0.89" layer="25"/>
<rectangle x1="0.35" y1="0.89" x2="0.63" y2="0.91" layer="25"/>
<rectangle x1="2.17" y1="0.89" x2="2.61" y2="0.91" layer="25"/>
<rectangle x1="2.83" y1="0.89" x2="3.39" y2="0.91" layer="25"/>
<rectangle x1="0.35" y1="0.91" x2="0.63" y2="0.93" layer="25"/>
<rectangle x1="2.17" y1="0.91" x2="2.57" y2="0.93" layer="25"/>
<rectangle x1="2.85" y1="0.91" x2="3.41" y2="0.93" layer="25"/>
<rectangle x1="0.33" y1="0.93" x2="0.63" y2="0.95" layer="25"/>
<rectangle x1="2.17" y1="0.93" x2="2.47" y2="0.95" layer="25"/>
<rectangle x1="2.89" y1="0.93" x2="3.43" y2="0.95" layer="25"/>
<rectangle x1="0.33" y1="0.95" x2="0.63" y2="0.97" layer="25"/>
<rectangle x1="2.17" y1="0.95" x2="2.35" y2="0.97" layer="25"/>
<rectangle x1="2.91" y1="0.95" x2="3.43" y2="0.97" layer="25"/>
<rectangle x1="0.31" y1="0.97" x2="0.63" y2="0.99" layer="25"/>
<rectangle x1="2.19" y1="0.97" x2="2.25" y2="0.99" layer="25"/>
<rectangle x1="2.93" y1="0.97" x2="3.45" y2="0.99" layer="25"/>
<rectangle x1="0.29" y1="0.99" x2="0.61" y2="1.01" layer="25"/>
<rectangle x1="2.91" y1="0.99" x2="3.45" y2="1.01" layer="25"/>
<rectangle x1="0.29" y1="1.01" x2="0.61" y2="1.03" layer="25"/>
<rectangle x1="2.89" y1="1.01" x2="3.47" y2="1.03" layer="25"/>
<rectangle x1="0.27" y1="1.03" x2="0.61" y2="1.05" layer="25"/>
<rectangle x1="2.87" y1="1.03" x2="3.47" y2="1.05" layer="25"/>
<rectangle x1="0.27" y1="1.05" x2="0.61" y2="1.07" layer="25"/>
<rectangle x1="2.87" y1="1.05" x2="3.49" y2="1.07" layer="25"/>
<rectangle x1="0.25" y1="1.07" x2="0.61" y2="1.09" layer="25"/>
<rectangle x1="2.85" y1="1.07" x2="3.49" y2="1.09" layer="25"/>
<rectangle x1="0.25" y1="1.09" x2="0.61" y2="1.11" layer="25"/>
<rectangle x1="2.83" y1="1.09" x2="3.51" y2="1.11" layer="25"/>
<rectangle x1="0.23" y1="1.11" x2="0.61" y2="1.13" layer="25"/>
<rectangle x1="2.81" y1="1.11" x2="3.51" y2="1.13" layer="25"/>
<rectangle x1="0.23" y1="1.13" x2="0.61" y2="1.15" layer="25"/>
<rectangle x1="2.81" y1="1.13" x2="3.53" y2="1.15" layer="25"/>
<rectangle x1="0.21" y1="1.15" x2="0.61" y2="1.17" layer="25"/>
<rectangle x1="2.79" y1="1.15" x2="3.53" y2="1.17" layer="25"/>
<rectangle x1="0.21" y1="1.17" x2="0.63" y2="1.19" layer="25"/>
<rectangle x1="2.77" y1="1.17" x2="3.55" y2="1.19" layer="25"/>
<rectangle x1="0.21" y1="1.19" x2="0.63" y2="1.21" layer="25"/>
<rectangle x1="2.75" y1="1.19" x2="3.55" y2="1.21" layer="25"/>
<rectangle x1="0.19" y1="1.21" x2="0.63" y2="1.23" layer="25"/>
<rectangle x1="2.75" y1="1.21" x2="3.55" y2="1.23" layer="25"/>
<rectangle x1="0.19" y1="1.23" x2="0.63" y2="1.25" layer="25"/>
<rectangle x1="2.31" y1="1.23" x2="2.35" y2="1.25" layer="25"/>
<rectangle x1="2.73" y1="1.23" x2="3.57" y2="1.25" layer="25"/>
<rectangle x1="0.17" y1="1.25" x2="0.63" y2="1.27" layer="25"/>
<rectangle x1="2.21" y1="1.25" x2="2.33" y2="1.27" layer="25"/>
<rectangle x1="2.71" y1="1.25" x2="3.57" y2="1.27" layer="25"/>
<rectangle x1="0.17" y1="1.27" x2="0.63" y2="1.29" layer="25"/>
<rectangle x1="2.15" y1="1.27" x2="2.31" y2="1.29" layer="25"/>
<rectangle x1="2.69" y1="1.27" x2="3.59" y2="1.29" layer="25"/>
<rectangle x1="0.17" y1="1.29" x2="0.65" y2="1.31" layer="25"/>
<rectangle x1="2.15" y1="1.29" x2="2.29" y2="1.31" layer="25"/>
<rectangle x1="2.69" y1="1.29" x2="3.59" y2="1.31" layer="25"/>
<rectangle x1="0.15" y1="1.31" x2="0.65" y2="1.33" layer="25"/>
<rectangle x1="2.13" y1="1.31" x2="2.29" y2="1.33" layer="25"/>
<rectangle x1="2.69" y1="1.31" x2="3.59" y2="1.33" layer="25"/>
<rectangle x1="0.15" y1="1.33" x2="0.65" y2="1.35" layer="25"/>
<rectangle x1="2.13" y1="1.33" x2="2.27" y2="1.35" layer="25"/>
<rectangle x1="2.69" y1="1.33" x2="3.61" y2="1.35" layer="25"/>
<rectangle x1="0.15" y1="1.35" x2="0.67" y2="1.37" layer="25"/>
<rectangle x1="2.13" y1="1.35" x2="2.25" y2="1.37" layer="25"/>
<rectangle x1="2.69" y1="1.35" x2="3.61" y2="1.37" layer="25"/>
<rectangle x1="0.13" y1="1.37" x2="0.67" y2="1.39" layer="25"/>
<rectangle x1="2.11" y1="1.37" x2="2.23" y2="1.39" layer="25"/>
<rectangle x1="2.69" y1="1.37" x2="3.61" y2="1.39" layer="25"/>
<rectangle x1="0.13" y1="1.39" x2="0.67" y2="1.41" layer="25"/>
<rectangle x1="2.11" y1="1.39" x2="2.23" y2="1.41" layer="25"/>
<rectangle x1="2.69" y1="1.39" x2="3.61" y2="1.41" layer="25"/>
<rectangle x1="0.13" y1="1.41" x2="0.69" y2="1.43" layer="25"/>
<rectangle x1="2.09" y1="1.41" x2="2.21" y2="1.43" layer="25"/>
<rectangle x1="2.69" y1="1.41" x2="3.63" y2="1.43" layer="25"/>
<rectangle x1="0.13" y1="1.43" x2="0.69" y2="1.45" layer="25"/>
<rectangle x1="2.09" y1="1.43" x2="2.19" y2="1.45" layer="25"/>
<rectangle x1="2.69" y1="1.43" x2="3.63" y2="1.45" layer="25"/>
<rectangle x1="0.11" y1="1.45" x2="0.71" y2="1.47" layer="25"/>
<rectangle x1="2.07" y1="1.45" x2="2.17" y2="1.47" layer="25"/>
<rectangle x1="2.69" y1="1.45" x2="3.63" y2="1.47" layer="25"/>
<rectangle x1="0.11" y1="1.47" x2="0.71" y2="1.49" layer="25"/>
<rectangle x1="2.07" y1="1.47" x2="2.15" y2="1.49" layer="25"/>
<rectangle x1="2.69" y1="1.47" x2="3.63" y2="1.49" layer="25"/>
<rectangle x1="0.11" y1="1.49" x2="0.73" y2="1.51" layer="25"/>
<rectangle x1="2.05" y1="1.49" x2="2.15" y2="1.51" layer="25"/>
<rectangle x1="2.69" y1="1.49" x2="3.65" y2="1.51" layer="25"/>
<rectangle x1="0.11" y1="1.51" x2="0.75" y2="1.53" layer="25"/>
<rectangle x1="2.03" y1="1.51" x2="2.13" y2="1.53" layer="25"/>
<rectangle x1="2.69" y1="1.51" x2="3.65" y2="1.53" layer="25"/>
<rectangle x1="0.09" y1="1.53" x2="0.75" y2="1.55" layer="25"/>
<rectangle x1="2.01" y1="1.53" x2="2.11" y2="1.55" layer="25"/>
<rectangle x1="2.69" y1="1.53" x2="3.65" y2="1.55" layer="25"/>
<rectangle x1="0.09" y1="1.55" x2="0.77" y2="1.57" layer="25"/>
<rectangle x1="2.01" y1="1.55" x2="2.09" y2="1.57" layer="25"/>
<rectangle x1="2.69" y1="1.55" x2="3.65" y2="1.57" layer="25"/>
<rectangle x1="0.09" y1="1.57" x2="0.79" y2="1.59" layer="25"/>
<rectangle x1="1.99" y1="1.57" x2="2.09" y2="1.59" layer="25"/>
<rectangle x1="2.69" y1="1.57" x2="3.65" y2="1.59" layer="25"/>
<rectangle x1="0.09" y1="1.59" x2="0.81" y2="1.61" layer="25"/>
<rectangle x1="1.97" y1="1.59" x2="2.07" y2="1.61" layer="25"/>
<rectangle x1="2.69" y1="1.59" x2="3.65" y2="1.61" layer="25"/>
<rectangle x1="0.09" y1="1.61" x2="0.83" y2="1.63" layer="25"/>
<rectangle x1="1.95" y1="1.61" x2="2.05" y2="1.63" layer="25"/>
<rectangle x1="2.43" y1="1.61" x2="2.45" y2="1.63" layer="25"/>
<rectangle x1="2.69" y1="1.61" x2="3.67" y2="1.63" layer="25"/>
<rectangle x1="0.07" y1="1.63" x2="0.85" y2="1.65" layer="25"/>
<rectangle x1="1.93" y1="1.63" x2="2.03" y2="1.65" layer="25"/>
<rectangle x1="2.41" y1="1.63" x2="2.45" y2="1.65" layer="25"/>
<rectangle x1="2.69" y1="1.63" x2="3.67" y2="1.65" layer="25"/>
<rectangle x1="0.07" y1="1.65" x2="0.87" y2="1.67" layer="25"/>
<rectangle x1="1.91" y1="1.65" x2="2.03" y2="1.67" layer="25"/>
<rectangle x1="2.41" y1="1.65" x2="2.45" y2="1.67" layer="25"/>
<rectangle x1="2.69" y1="1.65" x2="3.67" y2="1.67" layer="25"/>
<rectangle x1="0.07" y1="1.67" x2="0.89" y2="1.69" layer="25"/>
<rectangle x1="1.89" y1="1.67" x2="2.01" y2="1.69" layer="25"/>
<rectangle x1="2.39" y1="1.67" x2="2.45" y2="1.69" layer="25"/>
<rectangle x1="2.69" y1="1.67" x2="3.67" y2="1.69" layer="25"/>
<rectangle x1="0.07" y1="1.69" x2="0.91" y2="1.71" layer="25"/>
<rectangle x1="1.85" y1="1.69" x2="1.99" y2="1.71" layer="25"/>
<rectangle x1="2.37" y1="1.69" x2="2.45" y2="1.71" layer="25"/>
<rectangle x1="2.69" y1="1.69" x2="3.67" y2="1.71" layer="25"/>
<rectangle x1="0.07" y1="1.71" x2="0.93" y2="1.73" layer="25"/>
<rectangle x1="1.83" y1="1.71" x2="1.97" y2="1.73" layer="25"/>
<rectangle x1="2.35" y1="1.71" x2="2.45" y2="1.73" layer="25"/>
<rectangle x1="2.69" y1="1.71" x2="3.67" y2="1.73" layer="25"/>
<rectangle x1="0.07" y1="1.73" x2="0.97" y2="1.75" layer="25"/>
<rectangle x1="1.81" y1="1.73" x2="1.97" y2="1.75" layer="25"/>
<rectangle x1="2.35" y1="1.73" x2="2.45" y2="1.75" layer="25"/>
<rectangle x1="2.69" y1="1.73" x2="3.67" y2="1.75" layer="25"/>
<rectangle x1="0.07" y1="1.75" x2="0.99" y2="1.77" layer="25"/>
<rectangle x1="1.77" y1="1.75" x2="1.95" y2="1.77" layer="25"/>
<rectangle x1="2.33" y1="1.75" x2="2.45" y2="1.77" layer="25"/>
<rectangle x1="2.69" y1="1.75" x2="3.67" y2="1.77" layer="25"/>
<rectangle x1="0.07" y1="1.77" x2="1.03" y2="1.79" layer="25"/>
<rectangle x1="1.73" y1="1.77" x2="1.93" y2="1.79" layer="25"/>
<rectangle x1="2.31" y1="1.77" x2="2.45" y2="1.79" layer="25"/>
<rectangle x1="2.69" y1="1.77" x2="3.67" y2="1.79" layer="25"/>
<rectangle x1="0.07" y1="1.79" x2="1.09" y2="1.81" layer="25"/>
<rectangle x1="1.69" y1="1.79" x2="1.91" y2="1.81" layer="25"/>
<rectangle x1="2.29" y1="1.79" x2="2.45" y2="1.81" layer="25"/>
<rectangle x1="2.69" y1="1.79" x2="3.67" y2="1.81" layer="25"/>
<rectangle x1="0.07" y1="1.81" x2="1.13" y2="1.83" layer="25"/>
<rectangle x1="1.63" y1="1.81" x2="1.89" y2="1.83" layer="25"/>
<rectangle x1="2.29" y1="1.81" x2="2.45" y2="1.83" layer="25"/>
<rectangle x1="2.69" y1="1.81" x2="3.69" y2="1.83" layer="25"/>
<rectangle x1="0.07" y1="1.83" x2="1.21" y2="1.85" layer="25"/>
<rectangle x1="1.55" y1="1.83" x2="1.89" y2="1.85" layer="25"/>
<rectangle x1="2.27" y1="1.83" x2="2.45" y2="1.85" layer="25"/>
<rectangle x1="2.69" y1="1.83" x2="3.69" y2="1.85" layer="25"/>
<rectangle x1="0.05" y1="1.85" x2="1.35" y2="1.87" layer="25"/>
<rectangle x1="1.39" y1="1.85" x2="1.87" y2="1.87" layer="25"/>
<rectangle x1="2.25" y1="1.85" x2="2.45" y2="1.87" layer="25"/>
<rectangle x1="2.69" y1="1.85" x2="3.69" y2="1.87" layer="25"/>
<rectangle x1="0.05" y1="1.87" x2="1.85" y2="1.89" layer="25"/>
<rectangle x1="2.23" y1="1.87" x2="2.45" y2="1.89" layer="25"/>
<rectangle x1="2.69" y1="1.87" x2="3.69" y2="1.89" layer="25"/>
<rectangle x1="0.05" y1="1.89" x2="1.83" y2="1.91" layer="25"/>
<rectangle x1="2.23" y1="1.89" x2="2.45" y2="1.91" layer="25"/>
<rectangle x1="2.69" y1="1.89" x2="3.69" y2="1.91" layer="25"/>
<rectangle x1="0.05" y1="1.91" x2="1.83" y2="1.93" layer="25"/>
<rectangle x1="2.21" y1="1.91" x2="2.45" y2="1.93" layer="25"/>
<rectangle x1="2.69" y1="1.91" x2="3.69" y2="1.93" layer="25"/>
<rectangle x1="0.05" y1="1.93" x2="1.81" y2="1.95" layer="25"/>
<rectangle x1="2.19" y1="1.93" x2="2.45" y2="1.95" layer="25"/>
<rectangle x1="2.69" y1="1.93" x2="3.69" y2="1.95" layer="25"/>
<rectangle x1="0.05" y1="1.95" x2="1.79" y2="1.97" layer="25"/>
<rectangle x1="2.17" y1="1.95" x2="2.45" y2="1.97" layer="25"/>
<rectangle x1="2.69" y1="1.95" x2="3.67" y2="1.97" layer="25"/>
<rectangle x1="0.05" y1="1.97" x2="1.77" y2="1.99" layer="25"/>
<rectangle x1="2.15" y1="1.97" x2="2.45" y2="1.99" layer="25"/>
<rectangle x1="2.69" y1="1.97" x2="3.67" y2="1.99" layer="25"/>
<rectangle x1="0.07" y1="1.99" x2="1.77" y2="2.01" layer="25"/>
<rectangle x1="2.15" y1="1.99" x2="2.45" y2="2.01" layer="25"/>
<rectangle x1="2.69" y1="1.99" x2="3.67" y2="2.01" layer="25"/>
<rectangle x1="0.07" y1="2.01" x2="1.75" y2="2.03" layer="25"/>
<rectangle x1="2.13" y1="2.01" x2="2.45" y2="2.03" layer="25"/>
<rectangle x1="2.69" y1="2.01" x2="3.67" y2="2.03" layer="25"/>
<rectangle x1="0.07" y1="2.03" x2="1.73" y2="2.05" layer="25"/>
<rectangle x1="2.11" y1="2.03" x2="2.43" y2="2.05" layer="25"/>
<rectangle x1="2.77" y1="2.03" x2="3.67" y2="2.05" layer="25"/>
<rectangle x1="0.07" y1="2.05" x2="1.71" y2="2.07" layer="25"/>
<rectangle x1="2.09" y1="2.05" x2="2.39" y2="2.07" layer="25"/>
<rectangle x1="2.83" y1="2.05" x2="3.67" y2="2.07" layer="25"/>
<rectangle x1="0.07" y1="2.07" x2="1.69" y2="2.09" layer="25"/>
<rectangle x1="2.09" y1="2.07" x2="2.35" y2="2.09" layer="25"/>
<rectangle x1="2.87" y1="2.07" x2="3.67" y2="2.09" layer="25"/>
<rectangle x1="0.07" y1="2.09" x2="1.69" y2="2.11" layer="25"/>
<rectangle x1="2.07" y1="2.09" x2="2.31" y2="2.11" layer="25"/>
<rectangle x1="2.91" y1="2.09" x2="3.67" y2="2.11" layer="25"/>
<rectangle x1="0.07" y1="2.11" x2="1.21" y2="2.13" layer="25"/>
<rectangle x1="1.49" y1="2.11" x2="1.67" y2="2.13" layer="25"/>
<rectangle x1="2.05" y1="2.11" x2="2.27" y2="2.13" layer="25"/>
<rectangle x1="2.95" y1="2.11" x2="3.67" y2="2.13" layer="25"/>
<rectangle x1="0.07" y1="2.13" x2="1.15" y2="2.15" layer="25"/>
<rectangle x1="1.55" y1="2.13" x2="1.65" y2="2.15" layer="25"/>
<rectangle x1="2.03" y1="2.13" x2="2.25" y2="2.15" layer="25"/>
<rectangle x1="2.97" y1="2.13" x2="3.67" y2="2.15" layer="25"/>
<rectangle x1="0.07" y1="2.15" x2="1.11" y2="2.17" layer="25"/>
<rectangle x1="1.59" y1="2.15" x2="1.63" y2="2.17" layer="25"/>
<rectangle x1="2.03" y1="2.15" x2="2.23" y2="2.17" layer="25"/>
<rectangle x1="2.99" y1="2.15" x2="3.67" y2="2.17" layer="25"/>
<rectangle x1="0.07" y1="2.17" x2="1.07" y2="2.19" layer="25"/>
<rectangle x1="2.01" y1="2.17" x2="2.21" y2="2.19" layer="25"/>
<rectangle x1="3.01" y1="2.17" x2="3.65" y2="2.19" layer="25"/>
<rectangle x1="0.09" y1="2.19" x2="1.03" y2="2.21" layer="25"/>
<rectangle x1="1.99" y1="2.19" x2="2.19" y2="2.21" layer="25"/>
<rectangle x1="3.03" y1="2.19" x2="3.65" y2="2.21" layer="25"/>
<rectangle x1="0.09" y1="2.21" x2="1.01" y2="2.23" layer="25"/>
<rectangle x1="1.97" y1="2.21" x2="2.17" y2="2.23" layer="25"/>
<rectangle x1="3.05" y1="2.21" x2="3.65" y2="2.23" layer="25"/>
<rectangle x1="0.09" y1="2.23" x2="0.97" y2="2.25" layer="25"/>
<rectangle x1="1.97" y1="2.23" x2="2.15" y2="2.25" layer="25"/>
<rectangle x1="3.07" y1="2.23" x2="3.65" y2="2.25" layer="25"/>
<rectangle x1="0.09" y1="2.25" x2="0.95" y2="2.27" layer="25"/>
<rectangle x1="1.95" y1="2.25" x2="2.13" y2="2.27" layer="25"/>
<rectangle x1="3.09" y1="2.25" x2="3.65" y2="2.27" layer="25"/>
<rectangle x1="0.09" y1="2.27" x2="0.93" y2="2.29" layer="25"/>
<rectangle x1="1.93" y1="2.27" x2="2.13" y2="2.29" layer="25"/>
<rectangle x1="3.11" y1="2.27" x2="3.63" y2="2.29" layer="25"/>
<rectangle x1="0.09" y1="2.29" x2="0.91" y2="2.31" layer="25"/>
<rectangle x1="1.91" y1="2.29" x2="2.11" y2="2.31" layer="25"/>
<rectangle x1="3.11" y1="2.29" x2="3.63" y2="2.31" layer="25"/>
<rectangle x1="0.11" y1="2.31" x2="0.89" y2="2.33" layer="25"/>
<rectangle x1="1.89" y1="2.31" x2="2.11" y2="2.33" layer="25"/>
<rectangle x1="3.13" y1="2.31" x2="3.63" y2="2.33" layer="25"/>
<rectangle x1="0.11" y1="2.33" x2="0.89" y2="2.35" layer="25"/>
<rectangle x1="1.89" y1="2.33" x2="2.09" y2="2.35" layer="25"/>
<rectangle x1="3.13" y1="2.33" x2="3.63" y2="2.35" layer="25"/>
<rectangle x1="0.11" y1="2.35" x2="0.87" y2="2.37" layer="25"/>
<rectangle x1="1.87" y1="2.35" x2="2.09" y2="2.37" layer="25"/>
<rectangle x1="3.15" y1="2.35" x2="3.61" y2="2.37" layer="25"/>
<rectangle x1="0.11" y1="2.37" x2="0.85" y2="2.39" layer="25"/>
<rectangle x1="1.85" y1="2.37" x2="2.07" y2="2.39" layer="25"/>
<rectangle x1="3.15" y1="2.37" x2="3.61" y2="2.39" layer="25"/>
<rectangle x1="0.13" y1="2.39" x2="0.85" y2="2.41" layer="25"/>
<rectangle x1="1.85" y1="2.39" x2="2.07" y2="2.41" layer="25"/>
<rectangle x1="3.17" y1="2.39" x2="3.61" y2="2.41" layer="25"/>
<rectangle x1="0.13" y1="2.41" x2="0.83" y2="2.43" layer="25"/>
<rectangle x1="1.87" y1="2.41" x2="2.07" y2="2.43" layer="25"/>
<rectangle x1="3.17" y1="2.41" x2="3.61" y2="2.43" layer="25"/>
<rectangle x1="0.13" y1="2.43" x2="0.83" y2="2.45" layer="25"/>
<rectangle x1="1.87" y1="2.43" x2="2.07" y2="2.45" layer="25"/>
<rectangle x1="3.17" y1="2.43" x2="3.59" y2="2.45" layer="25"/>
<rectangle x1="0.13" y1="2.45" x2="0.81" y2="2.47" layer="25"/>
<rectangle x1="1.87" y1="2.45" x2="2.05" y2="2.47" layer="25"/>
<rectangle x1="3.19" y1="2.45" x2="3.59" y2="2.47" layer="25"/>
<rectangle x1="0.15" y1="2.47" x2="0.81" y2="2.49" layer="25"/>
<rectangle x1="1.89" y1="2.47" x2="2.05" y2="2.49" layer="25"/>
<rectangle x1="3.19" y1="2.47" x2="3.59" y2="2.49" layer="25"/>
<rectangle x1="0.15" y1="2.49" x2="0.79" y2="2.51" layer="25"/>
<rectangle x1="1.89" y1="2.49" x2="2.05" y2="2.51" layer="25"/>
<rectangle x1="3.19" y1="2.49" x2="3.57" y2="2.51" layer="25"/>
<rectangle x1="0.15" y1="2.51" x2="0.79" y2="2.53" layer="25"/>
<rectangle x1="1.89" y1="2.51" x2="2.05" y2="2.53" layer="25"/>
<rectangle x1="3.19" y1="2.51" x2="3.57" y2="2.53" layer="25"/>
<rectangle x1="0.17" y1="2.53" x2="0.79" y2="2.55" layer="25"/>
<rectangle x1="1.91" y1="2.53" x2="2.05" y2="2.55" layer="25"/>
<rectangle x1="3.19" y1="2.53" x2="3.57" y2="2.55" layer="25"/>
<rectangle x1="0.17" y1="2.55" x2="0.79" y2="2.57" layer="25"/>
<rectangle x1="1.91" y1="2.55" x2="2.05" y2="2.57" layer="25"/>
<rectangle x1="3.19" y1="2.55" x2="3.55" y2="2.57" layer="25"/>
<rectangle x1="0.19" y1="2.57" x2="0.77" y2="2.59" layer="25"/>
<rectangle x1="1.91" y1="2.57" x2="2.05" y2="2.59" layer="25"/>
<rectangle x1="3.21" y1="2.57" x2="3.55" y2="2.59" layer="25"/>
<rectangle x1="0.19" y1="2.59" x2="0.77" y2="2.61" layer="25"/>
<rectangle x1="1.91" y1="2.59" x2="2.05" y2="2.61" layer="25"/>
<rectangle x1="3.21" y1="2.59" x2="3.53" y2="2.61" layer="25"/>
<rectangle x1="0.19" y1="2.61" x2="0.77" y2="2.63" layer="25"/>
<rectangle x1="1.91" y1="2.61" x2="2.05" y2="2.63" layer="25"/>
<rectangle x1="3.21" y1="2.61" x2="3.53" y2="2.63" layer="25"/>
<rectangle x1="0.21" y1="2.63" x2="0.77" y2="2.65" layer="25"/>
<rectangle x1="1.91" y1="2.63" x2="2.05" y2="2.65" layer="25"/>
<rectangle x1="3.21" y1="2.63" x2="3.53" y2="2.65" layer="25"/>
<rectangle x1="0.21" y1="2.65" x2="0.77" y2="2.67" layer="25"/>
<rectangle x1="1.93" y1="2.65" x2="2.05" y2="2.67" layer="25"/>
<rectangle x1="3.19" y1="2.65" x2="3.51" y2="2.67" layer="25"/>
<rectangle x1="0.23" y1="2.67" x2="0.77" y2="2.69" layer="25"/>
<rectangle x1="1.93" y1="2.67" x2="2.05" y2="2.69" layer="25"/>
<rectangle x1="3.19" y1="2.67" x2="3.51" y2="2.69" layer="25"/>
<rectangle x1="0.23" y1="2.69" x2="0.77" y2="2.71" layer="25"/>
<rectangle x1="1.93" y1="2.69" x2="2.05" y2="2.71" layer="25"/>
<rectangle x1="3.19" y1="2.69" x2="3.49" y2="2.71" layer="25"/>
<rectangle x1="0.25" y1="2.71" x2="0.77" y2="2.73" layer="25"/>
<rectangle x1="1.91" y1="2.71" x2="2.07" y2="2.73" layer="25"/>
<rectangle x1="3.19" y1="2.71" x2="3.49" y2="2.73" layer="25"/>
<rectangle x1="0.25" y1="2.73" x2="0.77" y2="2.75" layer="25"/>
<rectangle x1="1.91" y1="2.73" x2="2.07" y2="2.75" layer="25"/>
<rectangle x1="3.19" y1="2.73" x2="3.47" y2="2.75" layer="25"/>
<rectangle x1="0.27" y1="2.75" x2="0.77" y2="2.77" layer="25"/>
<rectangle x1="1.91" y1="2.75" x2="2.07" y2="2.77" layer="25"/>
<rectangle x1="3.19" y1="2.75" x2="3.47" y2="2.77" layer="25"/>
<rectangle x1="0.27" y1="2.77" x2="0.77" y2="2.79" layer="25"/>
<rectangle x1="1.91" y1="2.77" x2="2.09" y2="2.79" layer="25"/>
<rectangle x1="3.17" y1="2.77" x2="3.45" y2="2.79" layer="25"/>
<rectangle x1="0.29" y1="2.79" x2="0.77" y2="2.81" layer="25"/>
<rectangle x1="1.91" y1="2.79" x2="2.09" y2="2.81" layer="25"/>
<rectangle x1="3.17" y1="2.79" x2="3.45" y2="2.81" layer="25"/>
<rectangle x1="0.29" y1="2.81" x2="0.79" y2="2.83" layer="25"/>
<rectangle x1="1.91" y1="2.81" x2="2.11" y2="2.83" layer="25"/>
<rectangle x1="3.17" y1="2.81" x2="3.43" y2="2.83" layer="25"/>
<rectangle x1="0.31" y1="2.83" x2="0.79" y2="2.85" layer="25"/>
<rectangle x1="1.89" y1="2.83" x2="2.11" y2="2.85" layer="25"/>
<rectangle x1="3.15" y1="2.83" x2="3.41" y2="2.85" layer="25"/>
<rectangle x1="0.31" y1="2.85" x2="0.79" y2="2.87" layer="25"/>
<rectangle x1="1.89" y1="2.85" x2="2.13" y2="2.87" layer="25"/>
<rectangle x1="3.15" y1="2.85" x2="3.41" y2="2.87" layer="25"/>
<rectangle x1="0.33" y1="2.87" x2="0.79" y2="2.89" layer="25"/>
<rectangle x1="1.89" y1="2.87" x2="2.13" y2="2.89" layer="25"/>
<rectangle x1="3.13" y1="2.87" x2="3.39" y2="2.89" layer="25"/>
<rectangle x1="0.35" y1="2.89" x2="0.81" y2="2.91" layer="25"/>
<rectangle x1="1.87" y1="2.89" x2="2.15" y2="2.91" layer="25"/>
<rectangle x1="3.13" y1="2.89" x2="3.39" y2="2.91" layer="25"/>
<rectangle x1="0.35" y1="2.91" x2="0.81" y2="2.93" layer="25"/>
<rectangle x1="1.87" y1="2.91" x2="2.15" y2="2.93" layer="25"/>
<rectangle x1="3.11" y1="2.91" x2="3.37" y2="2.93" layer="25"/>
<rectangle x1="0.37" y1="2.93" x2="0.83" y2="2.95" layer="25"/>
<rectangle x1="1.85" y1="2.93" x2="2.17" y2="2.95" layer="25"/>
<rectangle x1="3.09" y1="2.93" x2="3.35" y2="2.95" layer="25"/>
<rectangle x1="0.37" y1="2.95" x2="0.83" y2="2.97" layer="25"/>
<rectangle x1="1.85" y1="2.95" x2="2.19" y2="2.97" layer="25"/>
<rectangle x1="3.07" y1="2.95" x2="3.35" y2="2.97" layer="25"/>
<rectangle x1="0.39" y1="2.97" x2="0.85" y2="2.99" layer="25"/>
<rectangle x1="1.83" y1="2.97" x2="2.21" y2="2.99" layer="25"/>
<rectangle x1="3.07" y1="2.97" x2="3.33" y2="2.99" layer="25"/>
<rectangle x1="0.41" y1="2.99" x2="0.85" y2="3.01" layer="25"/>
<rectangle x1="1.83" y1="2.99" x2="2.23" y2="3.01" layer="25"/>
<rectangle x1="3.05" y1="2.99" x2="3.31" y2="3.01" layer="25"/>
<rectangle x1="0.43" y1="3.01" x2="0.87" y2="3.03" layer="25"/>
<rectangle x1="1.81" y1="3.01" x2="2.25" y2="3.03" layer="25"/>
<rectangle x1="3.03" y1="3.01" x2="3.29" y2="3.03" layer="25"/>
<rectangle x1="0.43" y1="3.03" x2="0.89" y2="3.05" layer="25"/>
<rectangle x1="1.79" y1="3.03" x2="2.27" y2="3.05" layer="25"/>
<rectangle x1="2.99" y1="3.03" x2="3.29" y2="3.05" layer="25"/>
<rectangle x1="0.45" y1="3.05" x2="0.89" y2="3.07" layer="25"/>
<rectangle x1="1.79" y1="3.05" x2="2.31" y2="3.07" layer="25"/>
<rectangle x1="2.97" y1="3.05" x2="3.27" y2="3.07" layer="25"/>
<rectangle x1="0.47" y1="3.07" x2="0.91" y2="3.09" layer="25"/>
<rectangle x1="1.77" y1="3.07" x2="2.33" y2="3.09" layer="25"/>
<rectangle x1="2.95" y1="3.07" x2="3.25" y2="3.09" layer="25"/>
<rectangle x1="0.49" y1="3.09" x2="0.93" y2="3.11" layer="25"/>
<rectangle x1="1.75" y1="3.09" x2="2.37" y2="3.11" layer="25"/>
<rectangle x1="2.91" y1="3.09" x2="3.23" y2="3.11" layer="25"/>
<rectangle x1="0.51" y1="3.11" x2="0.95" y2="3.13" layer="25"/>
<rectangle x1="1.73" y1="3.11" x2="2.41" y2="3.13" layer="25"/>
<rectangle x1="2.87" y1="3.11" x2="3.21" y2="3.13" layer="25"/>
<rectangle x1="0.53" y1="3.13" x2="0.97" y2="3.15" layer="25"/>
<rectangle x1="1.69" y1="3.13" x2="2.47" y2="3.15" layer="25"/>
<rectangle x1="2.81" y1="3.13" x2="3.19" y2="3.15" layer="25"/>
<rectangle x1="0.53" y1="3.15" x2="1.01" y2="3.17" layer="25"/>
<rectangle x1="1.67" y1="3.15" x2="2.57" y2="3.17" layer="25"/>
<rectangle x1="2.71" y1="3.15" x2="3.17" y2="3.17" layer="25"/>
<rectangle x1="0.55" y1="3.17" x2="1.03" y2="3.19" layer="25"/>
<rectangle x1="1.65" y1="3.17" x2="3.15" y2="3.19" layer="25"/>
<rectangle x1="0.57" y1="3.19" x2="1.07" y2="3.21" layer="25"/>
<rectangle x1="1.61" y1="3.19" x2="3.15" y2="3.21" layer="25"/>
<rectangle x1="0.59" y1="3.21" x2="1.11" y2="3.23" layer="25"/>
<rectangle x1="1.57" y1="3.21" x2="3.13" y2="3.23" layer="25"/>
<rectangle x1="0.61" y1="3.23" x2="1.17" y2="3.25" layer="25"/>
<rectangle x1="1.51" y1="3.23" x2="3.09" y2="3.25" layer="25"/>
<rectangle x1="0.63" y1="3.25" x2="1.25" y2="3.27" layer="25"/>
<rectangle x1="1.41" y1="3.25" x2="3.07" y2="3.27" layer="25"/>
<rectangle x1="0.65" y1="3.27" x2="3.05" y2="3.29" layer="25"/>
<rectangle x1="0.69" y1="3.29" x2="3.03" y2="3.31" layer="25"/>
<rectangle x1="0.71" y1="3.31" x2="3.01" y2="3.33" layer="25"/>
<rectangle x1="0.73" y1="3.33" x2="2.99" y2="3.35" layer="25"/>
<rectangle x1="0.75" y1="3.35" x2="2.95" y2="3.37" layer="25"/>
<rectangle x1="0.77" y1="3.37" x2="2.93" y2="3.39" layer="25"/>
<rectangle x1="0.81" y1="3.39" x2="2.91" y2="3.41" layer="25"/>
<rectangle x1="0.83" y1="3.41" x2="2.87" y2="3.43" layer="25"/>
<rectangle x1="0.87" y1="3.43" x2="2.85" y2="3.45" layer="25"/>
<rectangle x1="0.89" y1="3.45" x2="2.81" y2="3.47" layer="25"/>
<rectangle x1="0.93" y1="3.47" x2="2.79" y2="3.49" layer="25"/>
<rectangle x1="0.95" y1="3.49" x2="2.75" y2="3.51" layer="25"/>
<rectangle x1="0.99" y1="3.51" x2="2.71" y2="3.53" layer="25"/>
<rectangle x1="1.03" y1="3.53" x2="2.67" y2="3.55" layer="25"/>
<rectangle x1="1.07" y1="3.55" x2="2.63" y2="3.57" layer="25"/>
<rectangle x1="1.11" y1="3.57" x2="2.59" y2="3.59" layer="25"/>
<rectangle x1="1.15" y1="3.59" x2="2.55" y2="3.61" layer="25"/>
<rectangle x1="1.21" y1="3.61" x2="2.49" y2="3.63" layer="25"/>
<rectangle x1="1.27" y1="3.63" x2="2.45" y2="3.65" layer="25"/>
<rectangle x1="1.33" y1="3.65" x2="2.39" y2="3.67" layer="25"/>
<rectangle x1="1.39" y1="3.67" x2="2.31" y2="3.69" layer="25"/>
<rectangle x1="1.47" y1="3.69" x2="2.23" y2="3.71" layer="25"/>
<rectangle x1="1.57" y1="3.71" x2="2.13" y2="3.73" layer="25"/>
<rectangle x1="1.79" y1="3.73" x2="1.91" y2="3.75" layer="25"/>
</package>
<package name="MUTANTC_TEXT+LOGO">
<rectangle x1="9.13" y1="0.06" x2="9.23" y2="0.16" layer="25"/>
<rectangle x1="14.63" y1="0.06" x2="14.73" y2="0.16" layer="25"/>
<rectangle x1="9.13" y1="0.16" x2="9.43" y2="0.26" layer="25"/>
<rectangle x1="14.63" y1="0.16" x2="14.93" y2="0.26" layer="25"/>
<rectangle x1="9.13" y1="0.26" x2="9.53" y2="0.36" layer="25"/>
<rectangle x1="14.63" y1="0.26" x2="15.13" y2="0.36" layer="25"/>
<rectangle x1="9.13" y1="0.36" x2="9.53" y2="0.46" layer="25"/>
<rectangle x1="14.63" y1="0.36" x2="15.13" y2="0.46" layer="25"/>
<rectangle x1="9.13" y1="0.46" x2="9.53" y2="0.56" layer="25"/>
<rectangle x1="14.63" y1="0.46" x2="15.13" y2="0.56" layer="25"/>
<rectangle x1="9.13" y1="0.56" x2="9.53" y2="0.66" layer="25"/>
<rectangle x1="14.63" y1="0.56" x2="15.13" y2="0.66" layer="25"/>
<rectangle x1="9.13" y1="0.66" x2="9.53" y2="0.76" layer="25"/>
<rectangle x1="14.63" y1="0.66" x2="15.13" y2="0.76" layer="25"/>
<rectangle x1="9.13" y1="0.76" x2="9.53" y2="0.86" layer="25"/>
<rectangle x1="14.63" y1="0.76" x2="15.13" y2="0.86" layer="25"/>
<rectangle x1="4.23" y1="0.86" x2="4.53" y2="0.96" layer="25"/>
<rectangle x1="7.43" y1="0.76" x2="8.53" y2="0.86" layer="25"/>
<rectangle x1="9.13" y1="0.86" x2="9.53" y2="0.96" layer="25"/>
<rectangle x1="11.33" y1="0.86" x2="11.63" y2="0.96" layer="25"/>
<rectangle x1="13.23" y1="0.86" x2="13.53" y2="0.96" layer="25"/>
<rectangle x1="14.63" y1="0.86" x2="15.13" y2="0.96" layer="25"/>
<rectangle x1="16.73" y1="0.86" x2="17.33" y2="0.96" layer="25"/>
<rectangle x1="4.23" y1="0.96" x2="4.73" y2="1.06" layer="25"/>
<rectangle x1="7.33" y1="0.86" x2="8.63" y2="0.96" layer="25"/>
<rectangle x1="9.13" y1="0.96" x2="9.53" y2="1.06" layer="25"/>
<rectangle x1="10.53" y1="0.96" x2="11.83" y2="1.06" layer="25"/>
<rectangle x1="12.13" y1="0.96" x2="12.53" y2="1.06" layer="25"/>
<rectangle x1="13.23" y1="0.96" x2="13.73" y2="1.06" layer="25"/>
<rectangle x1="14.63" y1="0.96" x2="15.13" y2="1.06" layer="25"/>
<rectangle x1="16.33" y1="0.96" x2="17.73" y2="1.06" layer="25"/>
<rectangle x1="4.23" y1="1.06" x2="4.73" y2="1.16" layer="25"/>
<rectangle x1="7.23" y1="0.96" x2="8.73" y2="1.06" layer="25"/>
<rectangle x1="9.13" y1="1.06" x2="9.53" y2="1.16" layer="25"/>
<rectangle x1="10.43" y1="1.06" x2="11.83" y2="1.16" layer="25"/>
<rectangle x1="12.13" y1="1.06" x2="12.53" y2="1.16" layer="25"/>
<rectangle x1="13.23" y1="1.06" x2="13.73" y2="1.16" layer="25"/>
<rectangle x1="14.63" y1="1.06" x2="15.13" y2="1.16" layer="25"/>
<rectangle x1="16.23" y1="1.06" x2="17.83" y2="1.16" layer="25"/>
<rectangle x1="4.23" y1="1.16" x2="4.73" y2="1.26" layer="25"/>
<rectangle x1="7.23" y1="1.06" x2="8.73" y2="1.16" layer="25"/>
<rectangle x1="9.13" y1="1.16" x2="9.53" y2="1.26" layer="25"/>
<rectangle x1="10.33" y1="1.16" x2="11.83" y2="1.26" layer="25"/>
<rectangle x1="12.13" y1="1.16" x2="12.53" y2="1.26" layer="25"/>
<rectangle x1="13.23" y1="1.16" x2="13.73" y2="1.26" layer="25"/>
<rectangle x1="14.63" y1="1.16" x2="15.13" y2="1.26" layer="25"/>
<rectangle x1="16.13" y1="1.16" x2="17.93" y2="1.26" layer="25"/>
<rectangle x1="4.23" y1="1.26" x2="4.73" y2="1.36" layer="25"/>
<rectangle x1="7.23" y1="1.16" x2="7.63" y2="1.26" layer="25"/>
<rectangle x1="8.33" y1="1.16" x2="8.73" y2="1.26" layer="25"/>
<rectangle x1="9.13" y1="1.26" x2="9.53" y2="1.36" layer="25"/>
<rectangle x1="10.33" y1="1.26" x2="11.83" y2="1.36" layer="25"/>
<rectangle x1="12.13" y1="1.26" x2="12.53" y2="1.36" layer="25"/>
<rectangle x1="13.23" y1="1.26" x2="13.73" y2="1.36" layer="25"/>
<rectangle x1="14.63" y1="1.26" x2="15.13" y2="1.36" layer="25"/>
<rectangle x1="16.13" y1="1.26" x2="17.93" y2="1.36" layer="25"/>
<rectangle x1="4.23" y1="1.36" x2="4.73" y2="1.46" layer="25"/>
<rectangle x1="7.23" y1="1.26" x2="7.63" y2="1.36" layer="25"/>
<rectangle x1="8.33" y1="1.26" x2="8.73" y2="1.36" layer="25"/>
<rectangle x1="9.13" y1="1.36" x2="9.53" y2="1.46" layer="25"/>
<rectangle x1="10.33" y1="1.36" x2="10.73" y2="1.46" layer="25"/>
<rectangle x1="11.33" y1="1.36" x2="11.83" y2="1.46" layer="25"/>
<rectangle x1="12.13" y1="1.36" x2="12.53" y2="1.46" layer="25"/>
<rectangle x1="13.23" y1="1.36" x2="13.73" y2="1.46" layer="25"/>
<rectangle x1="14.63" y1="1.36" x2="15.13" y2="1.46" layer="25"/>
<rectangle x1="16.13" y1="1.36" x2="16.53" y2="1.46" layer="25"/>
<rectangle x1="17.53" y1="1.36" x2="17.93" y2="1.46" layer="25"/>
<rectangle x1="4.23" y1="1.46" x2="4.73" y2="1.56" layer="25"/>
<rectangle x1="7.23" y1="1.36" x2="7.63" y2="1.46" layer="25"/>
<rectangle x1="8.33" y1="1.36" x2="8.73" y2="1.46" layer="25"/>
<rectangle x1="9.13" y1="1.46" x2="9.53" y2="1.56" layer="25"/>
<rectangle x1="10.33" y1="1.46" x2="10.73" y2="1.56" layer="25"/>
<rectangle x1="11.33" y1="1.46" x2="11.83" y2="1.56" layer="25"/>
<rectangle x1="12.13" y1="1.46" x2="12.53" y2="1.56" layer="25"/>
<rectangle x1="13.23" y1="1.46" x2="13.73" y2="1.56" layer="25"/>
<rectangle x1="14.63" y1="1.46" x2="15.13" y2="1.56" layer="25"/>
<rectangle x1="16.13" y1="1.46" x2="16.53" y2="1.56" layer="25"/>
<rectangle x1="4.23" y1="1.56" x2="4.73" y2="1.66" layer="25"/>
<rectangle x1="7.23" y1="1.46" x2="7.63" y2="1.56" layer="25"/>
<rectangle x1="8.33" y1="1.46" x2="8.73" y2="1.56" layer="25"/>
<rectangle x1="9.13" y1="1.56" x2="9.53" y2="1.66" layer="25"/>
<rectangle x1="10.33" y1="1.56" x2="10.73" y2="1.66" layer="25"/>
<rectangle x1="11.33" y1="1.56" x2="11.83" y2="1.66" layer="25"/>
<rectangle x1="12.13" y1="1.56" x2="12.53" y2="1.66" layer="25"/>
<rectangle x1="13.23" y1="1.56" x2="13.73" y2="1.66" layer="25"/>
<rectangle x1="14.63" y1="1.56" x2="15.13" y2="1.66" layer="25"/>
<rectangle x1="16.13" y1="1.56" x2="16.53" y2="1.66" layer="25"/>
<rectangle x1="4.23" y1="1.66" x2="4.73" y2="1.76" layer="25"/>
<rectangle x1="7.23" y1="1.56" x2="7.63" y2="1.66" layer="25"/>
<rectangle x1="8.33" y1="1.56" x2="8.73" y2="1.66" layer="25"/>
<rectangle x1="9.13" y1="1.66" x2="9.53" y2="1.76" layer="25"/>
<rectangle x1="10.33" y1="1.66" x2="10.73" y2="1.76" layer="25"/>
<rectangle x1="11.33" y1="1.66" x2="11.83" y2="1.76" layer="25"/>
<rectangle x1="12.13" y1="1.66" x2="12.53" y2="1.76" layer="25"/>
<rectangle x1="16.13" y1="1.66" x2="16.53" y2="1.76" layer="25"/>
<rectangle x1="4.23" y1="1.76" x2="4.73" y2="1.86" layer="25"/>
<rectangle x1="5.43" y1="1.76" x2="5.83" y2="1.86" layer="25"/>
<rectangle x1="6.53" y1="1.76" x2="7.03" y2="1.86" layer="25"/>
<rectangle x1="7.23" y1="1.66" x2="7.63" y2="1.76" layer="25"/>
<rectangle x1="8.33" y1="1.66" x2="8.73" y2="1.76" layer="25"/>
<rectangle x1="9.13" y1="1.76" x2="9.53" y2="1.86" layer="25"/>
<rectangle x1="10.33" y1="1.76" x2="10.73" y2="1.86" layer="25"/>
<rectangle x1="11.33" y1="1.76" x2="11.83" y2="1.86" layer="25"/>
<rectangle x1="12.13" y1="1.76" x2="12.53" y2="1.86" layer="25"/>
<rectangle x1="16.13" y1="1.76" x2="16.53" y2="1.86" layer="25"/>
<rectangle x1="4.23" y1="1.86" x2="4.73" y2="1.96" layer="25"/>
<rectangle x1="5.43" y1="1.86" x2="5.83" y2="1.96" layer="25"/>
<rectangle x1="6.53" y1="1.86" x2="7.03" y2="1.96" layer="25"/>
<rectangle x1="7.23" y1="1.76" x2="7.63" y2="1.86" layer="25"/>
<rectangle x1="8.33" y1="1.76" x2="8.73" y2="1.86" layer="25"/>
<rectangle x1="9.13" y1="1.86" x2="9.53" y2="1.96" layer="25"/>
<rectangle x1="10.33" y1="1.86" x2="10.83" y2="1.96" layer="25"/>
<rectangle x1="11.23" y1="1.86" x2="11.83" y2="1.96" layer="25"/>
<rectangle x1="12.13" y1="1.86" x2="12.53" y2="1.96" layer="25"/>
<rectangle x1="16.13" y1="1.86" x2="16.53" y2="1.96" layer="25"/>
<rectangle x1="17.63" y1="1.86" x2="17.93" y2="1.96" layer="25"/>
<rectangle x1="4.23" y1="1.96" x2="4.73" y2="2.06" layer="25"/>
<rectangle x1="5.43" y1="1.96" x2="5.83" y2="2.06" layer="25"/>
<rectangle x1="6.53" y1="1.96" x2="7.03" y2="2.06" layer="25"/>
<rectangle x1="7.23" y1="1.86" x2="7.63" y2="1.96" layer="25"/>
<rectangle x1="9.13" y1="1.96" x2="9.53" y2="2.06" layer="25"/>
<rectangle x1="10.33" y1="1.96" x2="11.83" y2="2.06" layer="25"/>
<rectangle x1="12.13" y1="1.96" x2="12.53" y2="2.06" layer="25"/>
<rectangle x1="16.13" y1="1.96" x2="16.53" y2="2.06" layer="25"/>
<rectangle x1="4.23" y1="2.06" x2="4.73" y2="2.16" layer="25"/>
<rectangle x1="5.43" y1="2.06" x2="5.83" y2="2.16" layer="25"/>
<rectangle x1="6.53" y1="2.06" x2="7.03" y2="2.16" layer="25"/>
<rectangle x1="7.23" y1="1.96" x2="7.63" y2="2.06" layer="25"/>
<rectangle x1="9.13" y1="2.06" x2="9.53" y2="2.16" layer="25"/>
<rectangle x1="10.33" y1="2.06" x2="11.83" y2="2.16" layer="25"/>
<rectangle x1="12.13" y1="2.06" x2="12.53" y2="2.16" layer="25"/>
<rectangle x1="13.23" y1="2.06" x2="13.73" y2="2.16" layer="25"/>
<rectangle x1="14.63" y1="2.06" x2="15.13" y2="2.16" layer="25"/>
<rectangle x1="16.13" y1="2.06" x2="16.53" y2="2.16" layer="25"/>
<rectangle x1="4.23" y1="2.16" x2="4.73" y2="2.26" layer="25"/>
<rectangle x1="5.43" y1="2.16" x2="5.83" y2="2.26" layer="25"/>
<rectangle x1="6.53" y1="2.16" x2="7.03" y2="2.26" layer="25"/>
<rectangle x1="7.23" y1="2.06" x2="7.63" y2="2.16" layer="25"/>
<rectangle x1="9.13" y1="2.16" x2="9.53" y2="2.26" layer="25"/>
<rectangle x1="10.43" y1="2.16" x2="11.83" y2="2.26" layer="25"/>
<rectangle x1="12.13" y1="2.16" x2="12.53" y2="2.26" layer="25"/>
<rectangle x1="13.23" y1="2.16" x2="13.73" y2="2.26" layer="25"/>
<rectangle x1="14.63" y1="2.16" x2="15.13" y2="2.26" layer="25"/>
<rectangle x1="16.13" y1="2.16" x2="16.53" y2="2.26" layer="25"/>
<rectangle x1="4.23" y1="2.26" x2="4.73" y2="2.36" layer="25"/>
<rectangle x1="5.43" y1="2.26" x2="5.83" y2="2.36" layer="25"/>
<rectangle x1="6.53" y1="2.26" x2="7.03" y2="2.36" layer="25"/>
<rectangle x1="7.23" y1="2.16" x2="7.63" y2="2.26" layer="25"/>
<rectangle x1="9.13" y1="2.26" x2="9.53" y2="2.36" layer="25"/>
<rectangle x1="10.63" y1="2.26" x2="11.83" y2="2.36" layer="25"/>
<rectangle x1="12.13" y1="2.26" x2="12.53" y2="2.36" layer="25"/>
<rectangle x1="13.23" y1="2.26" x2="13.73" y2="2.36" layer="25"/>
<rectangle x1="14.63" y1="2.26" x2="15.13" y2="2.36" layer="25"/>
<rectangle x1="16.13" y1="2.26" x2="16.53" y2="2.36" layer="25"/>
<rectangle x1="4.23" y1="2.36" x2="4.73" y2="2.46" layer="25"/>
<rectangle x1="5.43" y1="2.36" x2="5.83" y2="2.46" layer="25"/>
<rectangle x1="6.53" y1="2.36" x2="7.03" y2="2.46" layer="25"/>
<rectangle x1="7.23" y1="2.26" x2="7.63" y2="2.36" layer="25"/>
<rectangle x1="9.13" y1="2.36" x2="9.53" y2="2.46" layer="25"/>
<rectangle x1="11.33" y1="2.36" x2="11.83" y2="2.46" layer="25"/>
<rectangle x1="12.13" y1="2.36" x2="12.63" y2="2.46" layer="25"/>
<rectangle x1="13.23" y1="2.36" x2="13.73" y2="2.46" layer="25"/>
<rectangle x1="14.63" y1="2.36" x2="15.13" y2="2.46" layer="25"/>
<rectangle x1="16.13" y1="2.36" x2="16.53" y2="2.46" layer="25"/>
<rectangle x1="4.23" y1="2.46" x2="4.73" y2="2.56" layer="25"/>
<rectangle x1="5.33" y1="2.46" x2="5.93" y2="2.56" layer="25"/>
<rectangle x1="6.53" y1="2.46" x2="6.93" y2="2.56" layer="25"/>
<rectangle x1="7.23" y1="2.36" x2="7.63" y2="2.46" layer="25"/>
<rectangle x1="9.13" y1="2.46" x2="9.53" y2="2.56" layer="25"/>
<rectangle x1="11.33" y1="2.46" x2="11.83" y2="2.56" layer="25"/>
<rectangle x1="12.13" y1="2.46" x2="12.63" y2="2.56" layer="25"/>
<rectangle x1="13.23" y1="2.46" x2="13.73" y2="2.56" layer="25"/>
<rectangle x1="14.63" y1="2.46" x2="15.13" y2="2.56" layer="25"/>
<rectangle x1="16.13" y1="2.46" x2="16.53" y2="2.56" layer="25"/>
<rectangle x1="4.33" y1="2.56" x2="6.93" y2="2.66" layer="25"/>
<rectangle x1="7.23" y1="2.46" x2="7.63" y2="2.56" layer="25"/>
<rectangle x1="8.83" y1="2.56" x2="10.23" y2="2.66" layer="25"/>
<rectangle x1="10.53" y1="2.56" x2="11.83" y2="2.66" layer="25"/>
<rectangle x1="12.13" y1="2.56" x2="13.73" y2="2.66" layer="25"/>
<rectangle x1="13.93" y1="2.56" x2="15.73" y2="2.66" layer="25"/>
<rectangle x1="16.13" y1="2.56" x2="16.53" y2="2.66" layer="25"/>
<rectangle x1="4.33" y1="2.66" x2="6.93" y2="2.76" layer="25"/>
<rectangle x1="7.23" y1="2.56" x2="7.63" y2="2.66" layer="25"/>
<rectangle x1="10.63" y1="2.66" x2="11.73" y2="2.76" layer="25"/>
<rectangle x1="12.23" y1="2.66" x2="13.63" y2="2.76" layer="25"/>
<rectangle x1="13.93" y1="2.66" x2="15.83" y2="2.76" layer="25"/>
<rectangle x1="16.13" y1="2.66" x2="16.53" y2="2.76" layer="25"/>
<rectangle x1="17.53" y1="2.66" x2="17.73" y2="2.76" layer="25"/>
<rectangle x1="4.43" y1="2.76" x2="6.83" y2="2.86" layer="25"/>
<rectangle x1="7.23" y1="2.66" x2="7.63" y2="2.76" layer="25"/>
<rectangle x1="8.83" y1="2.76" x2="10.33" y2="2.86" layer="25"/>
<rectangle x1="10.63" y1="2.76" x2="11.73" y2="2.86" layer="25"/>
<rectangle x1="12.23" y1="2.76" x2="13.63" y2="2.86" layer="25"/>
<rectangle x1="13.93" y1="2.76" x2="15.83" y2="2.86" layer="25"/>
<rectangle x1="16.13" y1="2.76" x2="16.53" y2="2.86" layer="25"/>
<rectangle x1="4.53" y1="2.86" x2="5.53" y2="2.96" layer="25"/>
<rectangle x1="5.73" y1="2.86" x2="6.73" y2="2.96" layer="25"/>
<rectangle x1="8.83" y1="2.86" x2="10.33" y2="2.96" layer="25"/>
<rectangle x1="10.73" y1="2.86" x2="11.63" y2="2.96" layer="25"/>
<rectangle x1="12.33" y1="2.86" x2="13.43" y2="2.96" layer="25"/>
<rectangle x1="14.03" y1="2.86" x2="15.83" y2="2.96" layer="25"/>
<rectangle x1="16.13" y1="2.86" x2="16.53" y2="2.96" layer="25"/>
<rectangle x1="16.13" y1="2.96" x2="16.53" y2="3.06" layer="25"/>
<rectangle x1="16.13" y1="3.06" x2="16.53" y2="3.16" layer="25"/>
<rectangle x1="16.13" y1="3.16" x2="16.53" y2="3.26" layer="25"/>
<rectangle x1="17.53" y1="3.16" x2="17.93" y2="3.26" layer="25"/>
<rectangle x1="16.13" y1="3.26" x2="17.93" y2="3.36" layer="25"/>
<rectangle x1="16.13" y1="3.36" x2="17.93" y2="3.46" layer="25"/>
<rectangle x1="16.23" y1="3.46" x2="17.83" y2="3.56" layer="25"/>
<rectangle x1="16.33" y1="3.56" x2="17.73" y2="3.66" layer="25"/>
<rectangle x1="16.63" y1="3.66" x2="17.43" y2="3.76" layer="25"/>
<rectangle x1="11.33" y1="0.76" x2="11.43" y2="0.86" layer="25"/>
<rectangle x1="12.13" y1="0.76" x2="12.23" y2="0.86" layer="25"/>
<rectangle x1="13.23" y1="0.76" x2="13.33" y2="0.86" layer="25"/>
<rectangle x1="4.23" y1="0.76" x2="4.33" y2="0.86" layer="25"/>
<rectangle x1="7.53" y1="2.86" x2="7.63" y2="2.96" layer="25"/>
<rectangle x1="10.33" y1="2.86" x2="10.43" y2="2.96" layer="25"/>
<rectangle x1="13.93" y1="2.86" x2="14.03" y2="2.96" layer="25"/>
<rectangle x1="15.83" y1="2.86" x2="15.93" y2="2.96" layer="25"/>
<rectangle x1="17.53" y1="2.56" x2="17.63" y2="2.66" layer="25"/>
<rectangle x1="17.83" y1="1.96" x2="17.93" y2="2.06" layer="25"/>
<rectangle x1="17.53" y1="3.06" x2="17.93" y2="3.16" layer="25"/>
<rectangle x1="17.53" y1="2.96" x2="17.93" y2="3.06" layer="25"/>
<rectangle x1="17.53" y1="2.86" x2="17.93" y2="2.96" layer="25"/>
<rectangle x1="17.53" y1="2.76" x2="17.93" y2="2.86" layer="25"/>
<rectangle x1="17.53" y1="1.46" x2="17.93" y2="1.56" layer="25"/>
<rectangle x1="17.53" y1="1.56" x2="17.93" y2="1.66" layer="25"/>
<rectangle x1="17.53" y1="1.66" x2="17.93" y2="1.76" layer="25"/>
<rectangle x1="17.53" y1="1.76" x2="17.93" y2="1.86" layer="25"/>
<rectangle x1="17.43" y1="1.36" x2="17.53" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="1.36" x2="16.63" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="1.46" x2="16.63" y2="1.56" layer="25"/>
<rectangle x1="16.63" y1="1.36" x2="16.73" y2="1.46" layer="25"/>
<rectangle x1="16.53" y1="3.16" x2="16.63" y2="3.26" layer="25"/>
<rectangle x1="16.53" y1="3.06" x2="16.63" y2="3.16" layer="25"/>
<rectangle x1="16.63" y1="3.16" x2="16.73" y2="3.26" layer="25"/>
<rectangle x1="17.43" y1="3.16" x2="17.53" y2="3.26" layer="25"/>
<rectangle x1="17.73" y1="2.66" x2="17.83" y2="2.76" layer="25"/>
<rectangle x1="8.83" y1="2.66" x2="10.33" y2="2.76" layer="25"/>
<rectangle x1="7.33" y1="2.76" x2="7.63" y2="2.86" layer="25"/>
<rectangle x1="12.13" y1="0.86" x2="12.43" y2="0.96" layer="25"/>
<rectangle x1="16.53" y1="0.86" x2="16.83" y2="0.96" layer="25"/>
<rectangle x1="2.07" y1="0.05" x2="2.13" y2="0.07" layer="25"/>
<rectangle x1="1.81" y1="0.07" x2="2.37" y2="0.09" layer="25"/>
<rectangle x1="1.71" y1="0.09" x2="2.47" y2="0.11" layer="25"/>
<rectangle x1="1.63" y1="0.11" x2="2.55" y2="0.13" layer="25"/>
<rectangle x1="1.55" y1="0.13" x2="2.61" y2="0.15" layer="25"/>
<rectangle x1="1.49" y1="0.15" x2="2.67" y2="0.17" layer="25"/>
<rectangle x1="1.45" y1="0.17" x2="2.73" y2="0.19" layer="25"/>
<rectangle x1="1.39" y1="0.19" x2="2.79" y2="0.21" layer="25"/>
<rectangle x1="1.35" y1="0.21" x2="2.83" y2="0.23" layer="25"/>
<rectangle x1="1.31" y1="0.23" x2="2.87" y2="0.25" layer="25"/>
<rectangle x1="1.27" y1="0.25" x2="2.91" y2="0.27" layer="25"/>
<rectangle x1="1.23" y1="0.27" x2="1.51" y2="0.29" layer="25"/>
<rectangle x1="1.73" y1="0.27" x2="2.95" y2="0.29" layer="25"/>
<rectangle x1="1.19" y1="0.29" x2="1.41" y2="0.31" layer="25"/>
<rectangle x1="1.83" y1="0.29" x2="2.99" y2="0.31" layer="25"/>
<rectangle x1="1.15" y1="0.31" x2="1.35" y2="0.33" layer="25"/>
<rectangle x1="1.89" y1="0.31" x2="3.01" y2="0.33" layer="25"/>
<rectangle x1="1.13" y1="0.33" x2="1.31" y2="0.35" layer="25"/>
<rectangle x1="1.93" y1="0.33" x2="3.05" y2="0.35" layer="25"/>
<rectangle x1="1.09" y1="0.35" x2="1.25" y2="0.37" layer="25"/>
<rectangle x1="1.97" y1="0.35" x2="3.07" y2="0.37" layer="25"/>
<rectangle x1="1.07" y1="0.37" x2="1.23" y2="0.39" layer="25"/>
<rectangle x1="2.01" y1="0.37" x2="3.11" y2="0.39" layer="25"/>
<rectangle x1="1.03" y1="0.39" x2="1.19" y2="0.41" layer="25"/>
<rectangle x1="2.05" y1="0.39" x2="3.13" y2="0.41" layer="25"/>
<rectangle x1="1.01" y1="0.41" x2="1.15" y2="0.43" layer="25"/>
<rectangle x1="2.07" y1="0.41" x2="3.17" y2="0.43" layer="25"/>
<rectangle x1="0.99" y1="0.43" x2="1.13" y2="0.45" layer="25"/>
<rectangle x1="2.09" y1="0.43" x2="3.19" y2="0.45" layer="25"/>
<rectangle x1="0.95" y1="0.45" x2="1.11" y2="0.47" layer="25"/>
<rectangle x1="2.13" y1="0.45" x2="3.21" y2="0.47" layer="25"/>
<rectangle x1="0.93" y1="0.47" x2="1.09" y2="0.49" layer="25"/>
<rectangle x1="2.15" y1="0.47" x2="3.23" y2="0.49" layer="25"/>
<rectangle x1="0.91" y1="0.49" x2="1.07" y2="0.51" layer="25"/>
<rectangle x1="2.17" y1="0.49" x2="3.25" y2="0.51" layer="25"/>
<rectangle x1="0.89" y1="0.51" x2="1.05" y2="0.53" layer="25"/>
<rectangle x1="2.19" y1="0.51" x2="3.29" y2="0.53" layer="25"/>
<rectangle x1="0.87" y1="0.53" x2="1.03" y2="0.55" layer="25"/>
<rectangle x1="2.21" y1="0.53" x2="3.31" y2="0.55" layer="25"/>
<rectangle x1="0.85" y1="0.55" x2="1.01" y2="0.57" layer="25"/>
<rectangle x1="2.21" y1="0.55" x2="3.33" y2="0.57" layer="25"/>
<rectangle x1="0.83" y1="0.57" x2="0.99" y2="0.59" layer="25"/>
<rectangle x1="2.23" y1="0.57" x2="3.35" y2="0.59" layer="25"/>
<rectangle x1="0.81" y1="0.59" x2="0.97" y2="0.61" layer="25"/>
<rectangle x1="2.25" y1="0.59" x2="3.37" y2="0.61" layer="25"/>
<rectangle x1="0.79" y1="0.61" x2="0.97" y2="0.63" layer="25"/>
<rectangle x1="2.27" y1="0.61" x2="3.39" y2="0.63" layer="25"/>
<rectangle x1="0.77" y1="0.63" x2="0.95" y2="0.65" layer="25"/>
<rectangle x1="2.27" y1="0.63" x2="3.41" y2="0.65" layer="25"/>
<rectangle x1="0.75" y1="0.65" x2="0.93" y2="0.67" layer="25"/>
<rectangle x1="2.29" y1="0.65" x2="3.41" y2="0.67" layer="25"/>
<rectangle x1="0.73" y1="0.67" x2="0.93" y2="0.69" layer="25"/>
<rectangle x1="2.29" y1="0.67" x2="3.43" y2="0.69" layer="25"/>
<rectangle x1="0.71" y1="0.69" x2="0.91" y2="0.71" layer="25"/>
<rectangle x1="2.31" y1="0.69" x2="3.45" y2="0.71" layer="25"/>
<rectangle x1="0.69" y1="0.71" x2="0.91" y2="0.73" layer="25"/>
<rectangle x1="2.31" y1="0.71" x2="3.47" y2="0.73" layer="25"/>
<rectangle x1="0.67" y1="0.73" x2="0.89" y2="0.75" layer="25"/>
<rectangle x1="2.33" y1="0.73" x2="3.49" y2="0.75" layer="25"/>
<rectangle x1="0.65" y1="0.75" x2="0.89" y2="0.77" layer="25"/>
<rectangle x1="2.33" y1="0.75" x2="3.51" y2="0.77" layer="25"/>
<rectangle x1="0.65" y1="0.77" x2="0.87" y2="0.79" layer="25"/>
<rectangle x1="2.33" y1="0.77" x2="3.51" y2="0.79" layer="25"/>
<rectangle x1="0.63" y1="0.79" x2="0.87" y2="0.81" layer="25"/>
<rectangle x1="2.35" y1="0.79" x2="3.53" y2="0.81" layer="25"/>
<rectangle x1="0.61" y1="0.81" x2="0.85" y2="0.83" layer="25"/>
<rectangle x1="2.35" y1="0.81" x2="2.87" y2="0.83" layer="25"/>
<rectangle x1="2.91" y1="0.81" x2="3.55" y2="0.83" layer="25"/>
<rectangle x1="0.61" y1="0.83" x2="0.85" y2="0.85" layer="25"/>
<rectangle x1="2.35" y1="0.83" x2="2.85" y2="0.85" layer="25"/>
<rectangle x1="2.95" y1="0.83" x2="3.57" y2="0.85" layer="25"/>
<rectangle x1="0.59" y1="0.85" x2="0.85" y2="0.87" layer="25"/>
<rectangle x1="2.37" y1="0.85" x2="2.83" y2="0.87" layer="25"/>
<rectangle x1="2.97" y1="0.85" x2="3.57" y2="0.87" layer="25"/>
<rectangle x1="0.57" y1="0.87" x2="0.85" y2="0.89" layer="25"/>
<rectangle x1="2.37" y1="0.87" x2="2.81" y2="0.89" layer="25"/>
<rectangle x1="3.01" y1="0.87" x2="3.59" y2="0.89" layer="25"/>
<rectangle x1="0.55" y1="0.89" x2="0.83" y2="0.91" layer="25"/>
<rectangle x1="2.37" y1="0.89" x2="2.81" y2="0.91" layer="25"/>
<rectangle x1="3.03" y1="0.89" x2="3.59" y2="0.91" layer="25"/>
<rectangle x1="0.55" y1="0.91" x2="0.83" y2="0.93" layer="25"/>
<rectangle x1="2.37" y1="0.91" x2="2.77" y2="0.93" layer="25"/>
<rectangle x1="3.05" y1="0.91" x2="3.61" y2="0.93" layer="25"/>
<rectangle x1="0.53" y1="0.93" x2="0.83" y2="0.95" layer="25"/>
<rectangle x1="2.37" y1="0.93" x2="2.67" y2="0.95" layer="25"/>
<rectangle x1="3.09" y1="0.93" x2="3.63" y2="0.95" layer="25"/>
<rectangle x1="0.53" y1="0.95" x2="0.83" y2="0.97" layer="25"/>
<rectangle x1="2.37" y1="0.95" x2="2.55" y2="0.97" layer="25"/>
<rectangle x1="3.11" y1="0.95" x2="3.63" y2="0.97" layer="25"/>
<rectangle x1="0.51" y1="0.97" x2="0.83" y2="0.99" layer="25"/>
<rectangle x1="2.39" y1="0.97" x2="2.45" y2="0.99" layer="25"/>
<rectangle x1="3.13" y1="0.97" x2="3.65" y2="0.99" layer="25"/>
<rectangle x1="0.49" y1="0.99" x2="0.81" y2="1.01" layer="25"/>
<rectangle x1="3.11" y1="0.99" x2="3.65" y2="1.01" layer="25"/>
<rectangle x1="0.49" y1="1.01" x2="0.81" y2="1.03" layer="25"/>
<rectangle x1="3.09" y1="1.01" x2="3.67" y2="1.03" layer="25"/>
<rectangle x1="0.47" y1="1.03" x2="0.81" y2="1.05" layer="25"/>
<rectangle x1="3.07" y1="1.03" x2="3.67" y2="1.05" layer="25"/>
<rectangle x1="0.47" y1="1.05" x2="0.81" y2="1.07" layer="25"/>
<rectangle x1="3.07" y1="1.05" x2="3.69" y2="1.07" layer="25"/>
<rectangle x1="0.45" y1="1.07" x2="0.81" y2="1.09" layer="25"/>
<rectangle x1="3.05" y1="1.07" x2="3.69" y2="1.09" layer="25"/>
<rectangle x1="0.45" y1="1.09" x2="0.81" y2="1.11" layer="25"/>
<rectangle x1="3.03" y1="1.09" x2="3.71" y2="1.11" layer="25"/>
<rectangle x1="0.43" y1="1.11" x2="0.81" y2="1.13" layer="25"/>
<rectangle x1="3.01" y1="1.11" x2="3.71" y2="1.13" layer="25"/>
<rectangle x1="0.43" y1="1.13" x2="0.81" y2="1.15" layer="25"/>
<rectangle x1="3.01" y1="1.13" x2="3.73" y2="1.15" layer="25"/>
<rectangle x1="0.41" y1="1.15" x2="0.81" y2="1.17" layer="25"/>
<rectangle x1="2.99" y1="1.15" x2="3.73" y2="1.17" layer="25"/>
<rectangle x1="0.41" y1="1.17" x2="0.83" y2="1.19" layer="25"/>
<rectangle x1="2.97" y1="1.17" x2="3.75" y2="1.19" layer="25"/>
<rectangle x1="0.41" y1="1.19" x2="0.83" y2="1.21" layer="25"/>
<rectangle x1="2.95" y1="1.19" x2="3.75" y2="1.21" layer="25"/>
<rectangle x1="0.39" y1="1.21" x2="0.83" y2="1.23" layer="25"/>
<rectangle x1="2.95" y1="1.21" x2="3.75" y2="1.23" layer="25"/>
<rectangle x1="0.39" y1="1.23" x2="0.83" y2="1.25" layer="25"/>
<rectangle x1="2.51" y1="1.23" x2="2.55" y2="1.25" layer="25"/>
<rectangle x1="2.93" y1="1.23" x2="3.77" y2="1.25" layer="25"/>
<rectangle x1="0.37" y1="1.25" x2="0.83" y2="1.27" layer="25"/>
<rectangle x1="2.41" y1="1.25" x2="2.53" y2="1.27" layer="25"/>
<rectangle x1="2.91" y1="1.25" x2="3.77" y2="1.27" layer="25"/>
<rectangle x1="0.37" y1="1.27" x2="0.83" y2="1.29" layer="25"/>
<rectangle x1="2.35" y1="1.27" x2="2.51" y2="1.29" layer="25"/>
<rectangle x1="2.89" y1="1.27" x2="3.79" y2="1.29" layer="25"/>
<rectangle x1="0.37" y1="1.29" x2="0.85" y2="1.31" layer="25"/>
<rectangle x1="2.35" y1="1.29" x2="2.49" y2="1.31" layer="25"/>
<rectangle x1="2.89" y1="1.29" x2="3.79" y2="1.31" layer="25"/>
<rectangle x1="0.35" y1="1.31" x2="0.85" y2="1.33" layer="25"/>
<rectangle x1="2.33" y1="1.31" x2="2.49" y2="1.33" layer="25"/>
<rectangle x1="2.89" y1="1.31" x2="3.79" y2="1.33" layer="25"/>
<rectangle x1="0.35" y1="1.33" x2="0.85" y2="1.35" layer="25"/>
<rectangle x1="2.33" y1="1.33" x2="2.47" y2="1.35" layer="25"/>
<rectangle x1="2.89" y1="1.33" x2="3.81" y2="1.35" layer="25"/>
<rectangle x1="0.35" y1="1.35" x2="0.87" y2="1.37" layer="25"/>
<rectangle x1="2.33" y1="1.35" x2="2.45" y2="1.37" layer="25"/>
<rectangle x1="2.89" y1="1.35" x2="3.81" y2="1.37" layer="25"/>
<rectangle x1="0.33" y1="1.37" x2="0.87" y2="1.39" layer="25"/>
<rectangle x1="2.31" y1="1.37" x2="2.43" y2="1.39" layer="25"/>
<rectangle x1="2.89" y1="1.37" x2="3.81" y2="1.39" layer="25"/>
<rectangle x1="0.33" y1="1.39" x2="0.87" y2="1.41" layer="25"/>
<rectangle x1="2.31" y1="1.39" x2="2.43" y2="1.41" layer="25"/>
<rectangle x1="2.89" y1="1.39" x2="3.81" y2="1.41" layer="25"/>
<rectangle x1="0.33" y1="1.41" x2="0.89" y2="1.43" layer="25"/>
<rectangle x1="2.29" y1="1.41" x2="2.41" y2="1.43" layer="25"/>
<rectangle x1="2.89" y1="1.41" x2="3.83" y2="1.43" layer="25"/>
<rectangle x1="0.33" y1="1.43" x2="0.89" y2="1.45" layer="25"/>
<rectangle x1="2.29" y1="1.43" x2="2.39" y2="1.45" layer="25"/>
<rectangle x1="2.89" y1="1.43" x2="3.83" y2="1.45" layer="25"/>
<rectangle x1="0.31" y1="1.45" x2="0.91" y2="1.47" layer="25"/>
<rectangle x1="2.27" y1="1.45" x2="2.37" y2="1.47" layer="25"/>
<rectangle x1="2.89" y1="1.45" x2="3.83" y2="1.47" layer="25"/>
<rectangle x1="0.31" y1="1.47" x2="0.91" y2="1.49" layer="25"/>
<rectangle x1="2.27" y1="1.47" x2="2.35" y2="1.49" layer="25"/>
<rectangle x1="2.89" y1="1.47" x2="3.83" y2="1.49" layer="25"/>
<rectangle x1="0.31" y1="1.49" x2="0.93" y2="1.51" layer="25"/>
<rectangle x1="2.25" y1="1.49" x2="2.35" y2="1.51" layer="25"/>
<rectangle x1="2.89" y1="1.49" x2="3.85" y2="1.51" layer="25"/>
<rectangle x1="0.31" y1="1.51" x2="0.95" y2="1.53" layer="25"/>
<rectangle x1="2.23" y1="1.51" x2="2.33" y2="1.53" layer="25"/>
<rectangle x1="2.89" y1="1.51" x2="3.85" y2="1.53" layer="25"/>
<rectangle x1="0.29" y1="1.53" x2="0.95" y2="1.55" layer="25"/>
<rectangle x1="2.21" y1="1.53" x2="2.31" y2="1.55" layer="25"/>
<rectangle x1="2.89" y1="1.53" x2="3.85" y2="1.55" layer="25"/>
<rectangle x1="0.29" y1="1.55" x2="0.97" y2="1.57" layer="25"/>
<rectangle x1="2.21" y1="1.55" x2="2.29" y2="1.57" layer="25"/>
<rectangle x1="2.89" y1="1.55" x2="3.85" y2="1.57" layer="25"/>
<rectangle x1="0.29" y1="1.57" x2="0.99" y2="1.59" layer="25"/>
<rectangle x1="2.19" y1="1.57" x2="2.29" y2="1.59" layer="25"/>
<rectangle x1="2.89" y1="1.57" x2="3.85" y2="1.59" layer="25"/>
<rectangle x1="0.29" y1="1.59" x2="1.01" y2="1.61" layer="25"/>
<rectangle x1="2.17" y1="1.59" x2="2.27" y2="1.61" layer="25"/>
<rectangle x1="2.89" y1="1.59" x2="3.85" y2="1.61" layer="25"/>
<rectangle x1="0.29" y1="1.61" x2="1.03" y2="1.63" layer="25"/>
<rectangle x1="2.15" y1="1.61" x2="2.25" y2="1.63" layer="25"/>
<rectangle x1="2.63" y1="1.61" x2="2.65" y2="1.63" layer="25"/>
<rectangle x1="2.89" y1="1.61" x2="3.87" y2="1.63" layer="25"/>
<rectangle x1="0.27" y1="1.63" x2="1.05" y2="1.65" layer="25"/>
<rectangle x1="2.13" y1="1.63" x2="2.23" y2="1.65" layer="25"/>
<rectangle x1="2.61" y1="1.63" x2="2.65" y2="1.65" layer="25"/>
<rectangle x1="2.89" y1="1.63" x2="3.87" y2="1.65" layer="25"/>
<rectangle x1="0.27" y1="1.65" x2="1.07" y2="1.67" layer="25"/>
<rectangle x1="2.11" y1="1.65" x2="2.23" y2="1.67" layer="25"/>
<rectangle x1="2.61" y1="1.65" x2="2.65" y2="1.67" layer="25"/>
<rectangle x1="2.89" y1="1.65" x2="3.87" y2="1.67" layer="25"/>
<rectangle x1="0.27" y1="1.67" x2="1.09" y2="1.69" layer="25"/>
<rectangle x1="2.09" y1="1.67" x2="2.21" y2="1.69" layer="25"/>
<rectangle x1="2.59" y1="1.67" x2="2.65" y2="1.69" layer="25"/>
<rectangle x1="2.89" y1="1.67" x2="3.87" y2="1.69" layer="25"/>
<rectangle x1="0.27" y1="1.69" x2="1.11" y2="1.71" layer="25"/>
<rectangle x1="2.05" y1="1.69" x2="2.19" y2="1.71" layer="25"/>
<rectangle x1="2.57" y1="1.69" x2="2.65" y2="1.71" layer="25"/>
<rectangle x1="2.89" y1="1.69" x2="3.87" y2="1.71" layer="25"/>
<rectangle x1="0.27" y1="1.71" x2="1.13" y2="1.73" layer="25"/>
<rectangle x1="2.03" y1="1.71" x2="2.17" y2="1.73" layer="25"/>
<rectangle x1="2.55" y1="1.71" x2="2.65" y2="1.73" layer="25"/>
<rectangle x1="2.89" y1="1.71" x2="3.87" y2="1.73" layer="25"/>
<rectangle x1="0.27" y1="1.73" x2="1.17" y2="1.75" layer="25"/>
<rectangle x1="2.01" y1="1.73" x2="2.17" y2="1.75" layer="25"/>
<rectangle x1="2.55" y1="1.73" x2="2.65" y2="1.75" layer="25"/>
<rectangle x1="2.89" y1="1.73" x2="3.87" y2="1.75" layer="25"/>
<rectangle x1="0.27" y1="1.75" x2="1.19" y2="1.77" layer="25"/>
<rectangle x1="1.97" y1="1.75" x2="2.15" y2="1.77" layer="25"/>
<rectangle x1="2.53" y1="1.75" x2="2.65" y2="1.77" layer="25"/>
<rectangle x1="2.89" y1="1.75" x2="3.87" y2="1.77" layer="25"/>
<rectangle x1="0.27" y1="1.77" x2="1.23" y2="1.79" layer="25"/>
<rectangle x1="1.93" y1="1.77" x2="2.13" y2="1.79" layer="25"/>
<rectangle x1="2.51" y1="1.77" x2="2.65" y2="1.79" layer="25"/>
<rectangle x1="2.89" y1="1.77" x2="3.87" y2="1.79" layer="25"/>
<rectangle x1="0.27" y1="1.79" x2="1.29" y2="1.81" layer="25"/>
<rectangle x1="1.89" y1="1.79" x2="2.11" y2="1.81" layer="25"/>
<rectangle x1="2.49" y1="1.79" x2="2.65" y2="1.81" layer="25"/>
<rectangle x1="2.89" y1="1.79" x2="3.87" y2="1.81" layer="25"/>
<rectangle x1="0.27" y1="1.81" x2="1.33" y2="1.83" layer="25"/>
<rectangle x1="1.83" y1="1.81" x2="2.09" y2="1.83" layer="25"/>
<rectangle x1="2.49" y1="1.81" x2="2.65" y2="1.83" layer="25"/>
<rectangle x1="2.89" y1="1.81" x2="3.89" y2="1.83" layer="25"/>
<rectangle x1="0.27" y1="1.83" x2="1.41" y2="1.85" layer="25"/>
<rectangle x1="1.75" y1="1.83" x2="2.09" y2="1.85" layer="25"/>
<rectangle x1="2.47" y1="1.83" x2="2.65" y2="1.85" layer="25"/>
<rectangle x1="2.89" y1="1.83" x2="3.89" y2="1.85" layer="25"/>
<rectangle x1="0.25" y1="1.85" x2="1.55" y2="1.87" layer="25"/>
<rectangle x1="1.59" y1="1.85" x2="2.07" y2="1.87" layer="25"/>
<rectangle x1="2.45" y1="1.85" x2="2.65" y2="1.87" layer="25"/>
<rectangle x1="2.89" y1="1.85" x2="3.89" y2="1.87" layer="25"/>
<rectangle x1="0.25" y1="1.87" x2="2.05" y2="1.89" layer="25"/>
<rectangle x1="2.43" y1="1.87" x2="2.65" y2="1.89" layer="25"/>
<rectangle x1="2.89" y1="1.87" x2="3.89" y2="1.89" layer="25"/>
<rectangle x1="0.25" y1="1.89" x2="2.03" y2="1.91" layer="25"/>
<rectangle x1="2.43" y1="1.89" x2="2.65" y2="1.91" layer="25"/>
<rectangle x1="2.89" y1="1.89" x2="3.89" y2="1.91" layer="25"/>
<rectangle x1="0.25" y1="1.91" x2="2.03" y2="1.93" layer="25"/>
<rectangle x1="2.41" y1="1.91" x2="2.65" y2="1.93" layer="25"/>
<rectangle x1="2.89" y1="1.91" x2="3.89" y2="1.93" layer="25"/>
<rectangle x1="0.25" y1="1.93" x2="2.01" y2="1.95" layer="25"/>
<rectangle x1="2.39" y1="1.93" x2="2.65" y2="1.95" layer="25"/>
<rectangle x1="2.89" y1="1.93" x2="3.89" y2="1.95" layer="25"/>
<rectangle x1="0.25" y1="1.95" x2="1.99" y2="1.97" layer="25"/>
<rectangle x1="2.37" y1="1.95" x2="2.65" y2="1.97" layer="25"/>
<rectangle x1="2.89" y1="1.95" x2="3.87" y2="1.97" layer="25"/>
<rectangle x1="0.25" y1="1.97" x2="1.97" y2="1.99" layer="25"/>
<rectangle x1="2.35" y1="1.97" x2="2.65" y2="1.99" layer="25"/>
<rectangle x1="2.89" y1="1.97" x2="3.87" y2="1.99" layer="25"/>
<rectangle x1="0.27" y1="1.99" x2="1.97" y2="2.01" layer="25"/>
<rectangle x1="2.35" y1="1.99" x2="2.65" y2="2.01" layer="25"/>
<rectangle x1="2.89" y1="1.99" x2="3.87" y2="2.01" layer="25"/>
<rectangle x1="0.27" y1="2.01" x2="1.95" y2="2.03" layer="25"/>
<rectangle x1="2.33" y1="2.01" x2="2.65" y2="2.03" layer="25"/>
<rectangle x1="2.89" y1="2.01" x2="3.87" y2="2.03" layer="25"/>
<rectangle x1="0.27" y1="2.03" x2="1.93" y2="2.05" layer="25"/>
<rectangle x1="2.31" y1="2.03" x2="2.63" y2="2.05" layer="25"/>
<rectangle x1="2.97" y1="2.03" x2="3.87" y2="2.05" layer="25"/>
<rectangle x1="0.27" y1="2.05" x2="1.91" y2="2.07" layer="25"/>
<rectangle x1="2.29" y1="2.05" x2="2.59" y2="2.07" layer="25"/>
<rectangle x1="3.03" y1="2.05" x2="3.87" y2="2.07" layer="25"/>
<rectangle x1="0.27" y1="2.07" x2="1.89" y2="2.09" layer="25"/>
<rectangle x1="2.29" y1="2.07" x2="2.55" y2="2.09" layer="25"/>
<rectangle x1="3.07" y1="2.07" x2="3.87" y2="2.09" layer="25"/>
<rectangle x1="0.27" y1="2.09" x2="1.89" y2="2.11" layer="25"/>
<rectangle x1="2.27" y1="2.09" x2="2.51" y2="2.11" layer="25"/>
<rectangle x1="3.11" y1="2.09" x2="3.87" y2="2.11" layer="25"/>
<rectangle x1="0.27" y1="2.11" x2="1.41" y2="2.13" layer="25"/>
<rectangle x1="1.69" y1="2.11" x2="1.87" y2="2.13" layer="25"/>
<rectangle x1="2.25" y1="2.11" x2="2.47" y2="2.13" layer="25"/>
<rectangle x1="3.15" y1="2.11" x2="3.87" y2="2.13" layer="25"/>
<rectangle x1="0.27" y1="2.13" x2="1.35" y2="2.15" layer="25"/>
<rectangle x1="1.75" y1="2.13" x2="1.85" y2="2.15" layer="25"/>
<rectangle x1="2.23" y1="2.13" x2="2.45" y2="2.15" layer="25"/>
<rectangle x1="3.17" y1="2.13" x2="3.87" y2="2.15" layer="25"/>
<rectangle x1="0.27" y1="2.15" x2="1.31" y2="2.17" layer="25"/>
<rectangle x1="1.79" y1="2.15" x2="1.83" y2="2.17" layer="25"/>
<rectangle x1="2.23" y1="2.15" x2="2.43" y2="2.17" layer="25"/>
<rectangle x1="3.19" y1="2.15" x2="3.87" y2="2.17" layer="25"/>
<rectangle x1="0.27" y1="2.17" x2="1.27" y2="2.19" layer="25"/>
<rectangle x1="2.21" y1="2.17" x2="2.41" y2="2.19" layer="25"/>
<rectangle x1="3.21" y1="2.17" x2="3.85" y2="2.19" layer="25"/>
<rectangle x1="0.29" y1="2.19" x2="1.23" y2="2.21" layer="25"/>
<rectangle x1="2.19" y1="2.19" x2="2.39" y2="2.21" layer="25"/>
<rectangle x1="3.23" y1="2.19" x2="3.85" y2="2.21" layer="25"/>
<rectangle x1="0.29" y1="2.21" x2="1.21" y2="2.23" layer="25"/>
<rectangle x1="2.17" y1="2.21" x2="2.37" y2="2.23" layer="25"/>
<rectangle x1="3.25" y1="2.21" x2="3.85" y2="2.23" layer="25"/>
<rectangle x1="0.29" y1="2.23" x2="1.17" y2="2.25" layer="25"/>
<rectangle x1="2.17" y1="2.23" x2="2.35" y2="2.25" layer="25"/>
<rectangle x1="3.27" y1="2.23" x2="3.85" y2="2.25" layer="25"/>
<rectangle x1="0.29" y1="2.25" x2="1.15" y2="2.27" layer="25"/>
<rectangle x1="2.15" y1="2.25" x2="2.33" y2="2.27" layer="25"/>
<rectangle x1="3.29" y1="2.25" x2="3.85" y2="2.27" layer="25"/>
<rectangle x1="0.29" y1="2.27" x2="1.13" y2="2.29" layer="25"/>
<rectangle x1="2.13" y1="2.27" x2="2.33" y2="2.29" layer="25"/>
<rectangle x1="3.31" y1="2.27" x2="3.83" y2="2.29" layer="25"/>
<rectangle x1="0.29" y1="2.29" x2="1.11" y2="2.31" layer="25"/>
<rectangle x1="2.11" y1="2.29" x2="2.31" y2="2.31" layer="25"/>
<rectangle x1="3.31" y1="2.29" x2="3.83" y2="2.31" layer="25"/>
<rectangle x1="0.31" y1="2.31" x2="1.09" y2="2.33" layer="25"/>
<rectangle x1="2.09" y1="2.31" x2="2.31" y2="2.33" layer="25"/>
<rectangle x1="3.33" y1="2.31" x2="3.83" y2="2.33" layer="25"/>
<rectangle x1="0.31" y1="2.33" x2="1.09" y2="2.35" layer="25"/>
<rectangle x1="2.09" y1="2.33" x2="2.29" y2="2.35" layer="25"/>
<rectangle x1="3.33" y1="2.33" x2="3.83" y2="2.35" layer="25"/>
<rectangle x1="0.31" y1="2.35" x2="1.07" y2="2.37" layer="25"/>
<rectangle x1="2.07" y1="2.35" x2="2.29" y2="2.37" layer="25"/>
<rectangle x1="3.35" y1="2.35" x2="3.81" y2="2.37" layer="25"/>
<rectangle x1="0.31" y1="2.37" x2="1.05" y2="2.39" layer="25"/>
<rectangle x1="2.05" y1="2.37" x2="2.27" y2="2.39" layer="25"/>
<rectangle x1="3.35" y1="2.37" x2="3.81" y2="2.39" layer="25"/>
<rectangle x1="0.33" y1="2.39" x2="1.05" y2="2.41" layer="25"/>
<rectangle x1="2.05" y1="2.39" x2="2.27" y2="2.41" layer="25"/>
<rectangle x1="3.37" y1="2.39" x2="3.81" y2="2.41" layer="25"/>
<rectangle x1="0.33" y1="2.41" x2="1.03" y2="2.43" layer="25"/>
<rectangle x1="2.07" y1="2.41" x2="2.27" y2="2.43" layer="25"/>
<rectangle x1="3.37" y1="2.41" x2="3.81" y2="2.43" layer="25"/>
<rectangle x1="0.33" y1="2.43" x2="1.03" y2="2.45" layer="25"/>
<rectangle x1="2.07" y1="2.43" x2="2.27" y2="2.45" layer="25"/>
<rectangle x1="3.37" y1="2.43" x2="3.79" y2="2.45" layer="25"/>
<rectangle x1="0.33" y1="2.45" x2="1.01" y2="2.47" layer="25"/>
<rectangle x1="2.07" y1="2.45" x2="2.25" y2="2.47" layer="25"/>
<rectangle x1="3.39" y1="2.45" x2="3.79" y2="2.47" layer="25"/>
<rectangle x1="0.35" y1="2.47" x2="1.01" y2="2.49" layer="25"/>
<rectangle x1="2.09" y1="2.47" x2="2.25" y2="2.49" layer="25"/>
<rectangle x1="3.39" y1="2.47" x2="3.79" y2="2.49" layer="25"/>
<rectangle x1="0.35" y1="2.49" x2="0.99" y2="2.51" layer="25"/>
<rectangle x1="2.09" y1="2.49" x2="2.25" y2="2.51" layer="25"/>
<rectangle x1="3.39" y1="2.49" x2="3.77" y2="2.51" layer="25"/>
<rectangle x1="0.35" y1="2.51" x2="0.99" y2="2.53" layer="25"/>
<rectangle x1="2.09" y1="2.51" x2="2.25" y2="2.53" layer="25"/>
<rectangle x1="3.39" y1="2.51" x2="3.77" y2="2.53" layer="25"/>
<rectangle x1="0.37" y1="2.53" x2="0.99" y2="2.55" layer="25"/>
<rectangle x1="2.11" y1="2.53" x2="2.25" y2="2.55" layer="25"/>
<rectangle x1="3.39" y1="2.53" x2="3.77" y2="2.55" layer="25"/>
<rectangle x1="0.37" y1="2.55" x2="0.99" y2="2.57" layer="25"/>
<rectangle x1="2.11" y1="2.55" x2="2.25" y2="2.57" layer="25"/>
<rectangle x1="3.39" y1="2.55" x2="3.75" y2="2.57" layer="25"/>
<rectangle x1="0.39" y1="2.57" x2="0.97" y2="2.59" layer="25"/>
<rectangle x1="2.11" y1="2.57" x2="2.25" y2="2.59" layer="25"/>
<rectangle x1="3.41" y1="2.57" x2="3.75" y2="2.59" layer="25"/>
<rectangle x1="0.39" y1="2.59" x2="0.97" y2="2.61" layer="25"/>
<rectangle x1="2.11" y1="2.59" x2="2.25" y2="2.61" layer="25"/>
<rectangle x1="3.41" y1="2.59" x2="3.73" y2="2.61" layer="25"/>
<rectangle x1="0.39" y1="2.61" x2="0.97" y2="2.63" layer="25"/>
<rectangle x1="2.11" y1="2.61" x2="2.25" y2="2.63" layer="25"/>
<rectangle x1="3.41" y1="2.61" x2="3.73" y2="2.63" layer="25"/>
<rectangle x1="0.41" y1="2.63" x2="0.97" y2="2.65" layer="25"/>
<rectangle x1="2.11" y1="2.63" x2="2.25" y2="2.65" layer="25"/>
<rectangle x1="3.41" y1="2.63" x2="3.73" y2="2.65" layer="25"/>
<rectangle x1="0.41" y1="2.65" x2="0.97" y2="2.67" layer="25"/>
<rectangle x1="2.13" y1="2.65" x2="2.25" y2="2.67" layer="25"/>
<rectangle x1="3.39" y1="2.65" x2="3.71" y2="2.67" layer="25"/>
<rectangle x1="0.43" y1="2.67" x2="0.97" y2="2.69" layer="25"/>
<rectangle x1="2.13" y1="2.67" x2="2.25" y2="2.69" layer="25"/>
<rectangle x1="3.39" y1="2.67" x2="3.71" y2="2.69" layer="25"/>
<rectangle x1="0.43" y1="2.69" x2="0.97" y2="2.71" layer="25"/>
<rectangle x1="2.13" y1="2.69" x2="2.25" y2="2.71" layer="25"/>
<rectangle x1="3.39" y1="2.69" x2="3.69" y2="2.71" layer="25"/>
<rectangle x1="0.45" y1="2.71" x2="0.97" y2="2.73" layer="25"/>
<rectangle x1="2.11" y1="2.71" x2="2.27" y2="2.73" layer="25"/>
<rectangle x1="3.39" y1="2.71" x2="3.69" y2="2.73" layer="25"/>
<rectangle x1="0.45" y1="2.73" x2="0.97" y2="2.75" layer="25"/>
<rectangle x1="2.11" y1="2.73" x2="2.27" y2="2.75" layer="25"/>
<rectangle x1="3.39" y1="2.73" x2="3.67" y2="2.75" layer="25"/>
<rectangle x1="0.47" y1="2.75" x2="0.97" y2="2.77" layer="25"/>
<rectangle x1="2.11" y1="2.75" x2="2.27" y2="2.77" layer="25"/>
<rectangle x1="3.39" y1="2.75" x2="3.67" y2="2.77" layer="25"/>
<rectangle x1="0.47" y1="2.77" x2="0.97" y2="2.79" layer="25"/>
<rectangle x1="2.11" y1="2.77" x2="2.29" y2="2.79" layer="25"/>
<rectangle x1="3.37" y1="2.77" x2="3.65" y2="2.79" layer="25"/>
<rectangle x1="0.49" y1="2.79" x2="0.97" y2="2.81" layer="25"/>
<rectangle x1="2.11" y1="2.79" x2="2.29" y2="2.81" layer="25"/>
<rectangle x1="3.37" y1="2.79" x2="3.65" y2="2.81" layer="25"/>
<rectangle x1="0.49" y1="2.81" x2="0.99" y2="2.83" layer="25"/>
<rectangle x1="2.11" y1="2.81" x2="2.31" y2="2.83" layer="25"/>
<rectangle x1="3.37" y1="2.81" x2="3.63" y2="2.83" layer="25"/>
<rectangle x1="0.51" y1="2.83" x2="0.99" y2="2.85" layer="25"/>
<rectangle x1="2.09" y1="2.83" x2="2.31" y2="2.85" layer="25"/>
<rectangle x1="3.35" y1="2.83" x2="3.61" y2="2.85" layer="25"/>
<rectangle x1="0.51" y1="2.85" x2="0.99" y2="2.87" layer="25"/>
<rectangle x1="2.09" y1="2.85" x2="2.33" y2="2.87" layer="25"/>
<rectangle x1="3.35" y1="2.85" x2="3.61" y2="2.87" layer="25"/>
<rectangle x1="0.53" y1="2.87" x2="0.99" y2="2.89" layer="25"/>
<rectangle x1="2.09" y1="2.87" x2="2.33" y2="2.89" layer="25"/>
<rectangle x1="3.33" y1="2.87" x2="3.59" y2="2.89" layer="25"/>
<rectangle x1="0.55" y1="2.89" x2="1.01" y2="2.91" layer="25"/>
<rectangle x1="2.07" y1="2.89" x2="2.35" y2="2.91" layer="25"/>
<rectangle x1="3.33" y1="2.89" x2="3.59" y2="2.91" layer="25"/>
<rectangle x1="0.55" y1="2.91" x2="1.01" y2="2.93" layer="25"/>
<rectangle x1="2.07" y1="2.91" x2="2.35" y2="2.93" layer="25"/>
<rectangle x1="3.31" y1="2.91" x2="3.57" y2="2.93" layer="25"/>
<rectangle x1="0.57" y1="2.93" x2="1.03" y2="2.95" layer="25"/>
<rectangle x1="2.05" y1="2.93" x2="2.37" y2="2.95" layer="25"/>
<rectangle x1="3.29" y1="2.93" x2="3.55" y2="2.95" layer="25"/>
<rectangle x1="0.57" y1="2.95" x2="1.03" y2="2.97" layer="25"/>
<rectangle x1="2.05" y1="2.95" x2="2.39" y2="2.97" layer="25"/>
<rectangle x1="3.27" y1="2.95" x2="3.55" y2="2.97" layer="25"/>
<rectangle x1="0.59" y1="2.97" x2="1.05" y2="2.99" layer="25"/>
<rectangle x1="2.03" y1="2.97" x2="2.41" y2="2.99" layer="25"/>
<rectangle x1="3.27" y1="2.97" x2="3.53" y2="2.99" layer="25"/>
<rectangle x1="0.61" y1="2.99" x2="1.05" y2="3.01" layer="25"/>
<rectangle x1="2.03" y1="2.99" x2="2.43" y2="3.01" layer="25"/>
<rectangle x1="3.25" y1="2.99" x2="3.51" y2="3.01" layer="25"/>
<rectangle x1="0.63" y1="3.01" x2="1.07" y2="3.03" layer="25"/>
<rectangle x1="2.01" y1="3.01" x2="2.45" y2="3.03" layer="25"/>
<rectangle x1="3.23" y1="3.01" x2="3.49" y2="3.03" layer="25"/>
<rectangle x1="0.63" y1="3.03" x2="1.09" y2="3.05" layer="25"/>
<rectangle x1="1.99" y1="3.03" x2="2.47" y2="3.05" layer="25"/>
<rectangle x1="3.19" y1="3.03" x2="3.49" y2="3.05" layer="25"/>
<rectangle x1="0.65" y1="3.05" x2="1.09" y2="3.07" layer="25"/>
<rectangle x1="1.99" y1="3.05" x2="2.51" y2="3.07" layer="25"/>
<rectangle x1="3.17" y1="3.05" x2="3.47" y2="3.07" layer="25"/>
<rectangle x1="0.67" y1="3.07" x2="1.11" y2="3.09" layer="25"/>
<rectangle x1="1.97" y1="3.07" x2="2.53" y2="3.09" layer="25"/>
<rectangle x1="3.15" y1="3.07" x2="3.45" y2="3.09" layer="25"/>
<rectangle x1="0.69" y1="3.09" x2="1.13" y2="3.11" layer="25"/>
<rectangle x1="1.95" y1="3.09" x2="2.57" y2="3.11" layer="25"/>
<rectangle x1="3.11" y1="3.09" x2="3.43" y2="3.11" layer="25"/>
<rectangle x1="0.71" y1="3.11" x2="1.15" y2="3.13" layer="25"/>
<rectangle x1="1.93" y1="3.11" x2="2.61" y2="3.13" layer="25"/>
<rectangle x1="3.07" y1="3.11" x2="3.41" y2="3.13" layer="25"/>
<rectangle x1="0.73" y1="3.13" x2="1.17" y2="3.15" layer="25"/>
<rectangle x1="1.89" y1="3.13" x2="2.67" y2="3.15" layer="25"/>
<rectangle x1="3.01" y1="3.13" x2="3.39" y2="3.15" layer="25"/>
<rectangle x1="0.73" y1="3.15" x2="1.21" y2="3.17" layer="25"/>
<rectangle x1="1.87" y1="3.15" x2="2.77" y2="3.17" layer="25"/>
<rectangle x1="2.91" y1="3.15" x2="3.37" y2="3.17" layer="25"/>
<rectangle x1="0.75" y1="3.17" x2="1.23" y2="3.19" layer="25"/>
<rectangle x1="1.85" y1="3.17" x2="3.35" y2="3.19" layer="25"/>
<rectangle x1="0.77" y1="3.19" x2="1.27" y2="3.21" layer="25"/>
<rectangle x1="1.81" y1="3.19" x2="3.35" y2="3.21" layer="25"/>
<rectangle x1="0.79" y1="3.21" x2="1.31" y2="3.23" layer="25"/>
<rectangle x1="1.77" y1="3.21" x2="3.33" y2="3.23" layer="25"/>
<rectangle x1="0.81" y1="3.23" x2="1.37" y2="3.25" layer="25"/>
<rectangle x1="1.71" y1="3.23" x2="3.29" y2="3.25" layer="25"/>
<rectangle x1="0.83" y1="3.25" x2="1.45" y2="3.27" layer="25"/>
<rectangle x1="1.61" y1="3.25" x2="3.27" y2="3.27" layer="25"/>
<rectangle x1="0.85" y1="3.27" x2="3.25" y2="3.29" layer="25"/>
<rectangle x1="0.89" y1="3.29" x2="3.23" y2="3.31" layer="25"/>
<rectangle x1="0.91" y1="3.31" x2="3.21" y2="3.33" layer="25"/>
<rectangle x1="0.93" y1="3.33" x2="3.19" y2="3.35" layer="25"/>
<rectangle x1="0.95" y1="3.35" x2="3.15" y2="3.37" layer="25"/>
<rectangle x1="0.97" y1="3.37" x2="3.13" y2="3.39" layer="25"/>
<rectangle x1="1.01" y1="3.39" x2="3.11" y2="3.41" layer="25"/>
<rectangle x1="1.03" y1="3.41" x2="3.07" y2="3.43" layer="25"/>
<rectangle x1="1.07" y1="3.43" x2="3.05" y2="3.45" layer="25"/>
<rectangle x1="1.09" y1="3.45" x2="3.01" y2="3.47" layer="25"/>
<rectangle x1="1.13" y1="3.47" x2="2.99" y2="3.49" layer="25"/>
<rectangle x1="1.15" y1="3.49" x2="2.95" y2="3.51" layer="25"/>
<rectangle x1="1.19" y1="3.51" x2="2.91" y2="3.53" layer="25"/>
<rectangle x1="1.23" y1="3.53" x2="2.87" y2="3.55" layer="25"/>
<rectangle x1="1.27" y1="3.55" x2="2.83" y2="3.57" layer="25"/>
<rectangle x1="1.31" y1="3.57" x2="2.79" y2="3.59" layer="25"/>
<rectangle x1="1.35" y1="3.59" x2="2.75" y2="3.61" layer="25"/>
<rectangle x1="1.41" y1="3.61" x2="2.69" y2="3.63" layer="25"/>
<rectangle x1="1.47" y1="3.63" x2="2.65" y2="3.65" layer="25"/>
<rectangle x1="1.53" y1="3.65" x2="2.59" y2="3.67" layer="25"/>
<rectangle x1="1.59" y1="3.67" x2="2.51" y2="3.69" layer="25"/>
<rectangle x1="1.67" y1="3.69" x2="2.43" y2="3.71" layer="25"/>
<rectangle x1="1.77" y1="3.71" x2="2.33" y2="3.73" layer="25"/>
<rectangle x1="1.99" y1="3.73" x2="2.11" y2="3.75" layer="25"/>
<rectangle x1="16.53" y1="1.56" x2="16.63" y2="1.66" layer="25"/>
<rectangle x1="16.53" y1="2.98" x2="16.63" y2="3.08" layer="25"/>
</package>
<package name="EXPANSION-MALE-2X10" urn="urn:adsk.eagle:footprint:22268/1" locally_modified="yes" library_version="3">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<wire x1="-12.7" y1="-1.905" x2="-12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="-1.905" x2="-9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="-2.54" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="-1.905" x2="-6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="-2.54" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="-1.905" x2="-4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="-2.54" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="-1.905" x2="-1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="-2.54" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="-1.905" x2="0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="-2.54" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="-1.905" x2="-12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-12.7" y1="1.905" x2="-12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="2.54" x2="-10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-10.795" y1="2.54" x2="-10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-8.255" y1="2.54" x2="-7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="2.54" x2="-5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-5.715" y1="2.54" x2="-5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="2.54" x2="-3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-3.175" y1="2.54" x2="-2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="2.54" x2="-0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="-0.635" y1="2.54" x2="0" y2="1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0.635" y2="2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="2.54" x2="1.905" y2="2.54" width="0.1524" layer="21"/>
<wire x1="1.905" y1="2.54" x2="2.54" y2="1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="3.175" y2="2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="2.54" x2="4.445" y2="2.54" width="0.1524" layer="21"/>
<wire x1="4.445" y1="2.54" x2="5.08" y2="1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.715" y2="2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="2.54" x2="6.985" y2="2.54" width="0.1524" layer="21"/>
<wire x1="6.985" y1="2.54" x2="7.62" y2="1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="8.255" y2="2.54" width="0.1524" layer="21"/>
<wire x1="8.255" y1="2.54" x2="9.525" y2="2.54" width="0.1524" layer="21"/>
<wire x1="9.525" y1="2.54" x2="10.16" y2="1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="7.62" y1="-1.905" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.08" y1="-1.905" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="2.54" y1="-1.905" x2="3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-10.16" y1="1.905" x2="-10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-7.62" y1="1.905" x2="-7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-5.08" y1="1.905" x2="-5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="-2.54" y1="1.905" x2="-2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="0" y1="1.905" x2="0" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="2.54" y1="1.905" x2="2.54" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="5.08" y1="1.905" x2="5.08" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="7.62" y1="1.905" x2="7.62" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.16" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="8.255" y1="-2.54" x2="9.525" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="5.715" y1="-2.54" x2="6.985" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="3.175" y1="-2.54" x2="4.445" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="0.635" y1="-2.54" x2="1.905" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-1.905" y1="-2.54" x2="-0.635" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-4.445" y1="-2.54" x2="-3.175" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-6.985" y1="-2.54" x2="-5.715" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-8.255" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="1.905" x2="10.795" y2="2.54" width="0.1524" layer="21"/>
<wire x1="10.795" y1="2.54" x2="12.065" y2="2.54" width="0.1524" layer="21"/>
<wire x1="12.065" y1="2.54" x2="12.7" y2="1.905" width="0.1524" layer="21"/>
<wire x1="12.7" y1="-1.905" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="10.16" y1="-1.905" x2="10.795" y2="-2.54" width="0.1524" layer="21"/>
<wire x1="12.7" y1="1.905" x2="12.7" y2="-1.905" width="0.1524" layer="21"/>
<wire x1="10.795" y1="-2.54" x2="12.065" y2="-2.54" width="0.1524" layer="21"/>
<pad name="1" x="-11.43" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-11.43" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-8.89" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="4" x="-8.89" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="5" x="-6.35" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="6" x="-6.35" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="7" x="-3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="8" x="-3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="9" x="-1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="10" x="-1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="11" x="1.27" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="12" x="1.27" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="13" x="3.81" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="14" x="3.81" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="15" x="6.35" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="16" x="6.35" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="17" x="8.89" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="18" x="8.89" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="19" x="11.43" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="20" x="11.43" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<text x="-12.7" y="3.175" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="-12.7" y="-4.445" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-11.684" y1="-1.524" x2="-11.176" y2="-1.016" layer="51"/>
<rectangle x1="-11.684" y1="1.016" x2="-11.176" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="1.016" x2="-8.636" y2="1.524" layer="51"/>
<rectangle x1="-9.144" y1="-1.524" x2="-8.636" y2="-1.016" layer="51"/>
<rectangle x1="-6.604" y1="1.016" x2="-6.096" y2="1.524" layer="51"/>
<rectangle x1="-6.604" y1="-1.524" x2="-6.096" y2="-1.016" layer="51"/>
<rectangle x1="-4.064" y1="1.016" x2="-3.556" y2="1.524" layer="51"/>
<rectangle x1="-1.524" y1="1.016" x2="-1.016" y2="1.524" layer="51"/>
<rectangle x1="1.016" y1="1.016" x2="1.524" y2="1.524" layer="51"/>
<rectangle x1="-4.064" y1="-1.524" x2="-3.556" y2="-1.016" layer="51"/>
<rectangle x1="-1.524" y1="-1.524" x2="-1.016" y2="-1.016" layer="51"/>
<rectangle x1="1.016" y1="-1.524" x2="1.524" y2="-1.016" layer="51"/>
<rectangle x1="3.556" y1="1.016" x2="4.064" y2="1.524" layer="51"/>
<rectangle x1="3.556" y1="-1.524" x2="4.064" y2="-1.016" layer="51"/>
<rectangle x1="6.096" y1="1.016" x2="6.604" y2="1.524" layer="51"/>
<rectangle x1="6.096" y1="-1.524" x2="6.604" y2="-1.016" layer="51"/>
<rectangle x1="8.636" y1="1.016" x2="9.144" y2="1.524" layer="51"/>
<rectangle x1="8.636" y1="-1.524" x2="9.144" y2="-1.016" layer="51"/>
<rectangle x1="11.176" y1="1.016" x2="11.684" y2="1.524" layer="51"/>
<rectangle x1="11.176" y1="-1.524" x2="11.684" y2="-1.016" layer="51"/>
</package>
<package name="OSHW_16MM" library_version="252">
<polygon width="0.127" layer="25" pour="solid">
<vertex x="-5.00380625" y="-5.89279375"/>
<vertex x="-4.673596875" y="-6.1468"/>
<vertex x="-3.03129375" y="-4.977275"/>
<vertex x="-3.022340625" y="-4.970340625"/>
<vertex x="-3.02106875" y="-4.96999375"/>
<vertex x="-3.01999375" y="-4.969228125"/>
<vertex x="-3.00889375" y="-4.966675"/>
<vertex x="-2.99796875" y="-4.963696875"/>
<vertex x="-2.996665625" y="-4.9638625"/>
<vertex x="-2.995375" y="-4.963565625"/>
<vertex x="-2.9841375" y="-4.965453125"/>
<vertex x="-2.972909375" y="-4.96688125"/>
<vertex x="-2.97176875" y="-4.967534375"/>
<vertex x="-2.9704625" y="-4.967753125"/>
<vertex x="-2.960834375" y="-4.97378125"/>
<vertex x="-2.64159375" y="-5.15620625"/>
<vertex x="-2.285971875" y="-5.3340125"/>
<vertex x="-1.9812" y="-5.461003125"/>
<vertex x="-0.7874" y="-2.032"/>
<vertex x="-1.0807625" y="-1.893765625"/>
<vertex x="-1.085696875" y="-1.892753125"/>
<vertex x="-1.092078125" y="-1.888434375"/>
<vertex x="-1.099053125" y="-1.885146875"/>
<vertex x="-1.10244375" y="-1.88141875"/>
<vertex x="-1.360640625" y="-1.7066375"/>
<vertex x="-1.3653625" y="-1.704875"/>
<vertex x="-1.37100625" y="-1.69961875"/>
<vertex x="-1.3773875" y="-1.6953"/>
<vertex x="-1.380159375" y="-1.69109375"/>
<vertex x="-1.60834375" y="-1.478609375"/>
<vertex x="-1.61274375" y="-1.4761375"/>
<vertex x="-1.617515625" y="-1.47006875"/>
<vertex x="-1.62315" y="-1.464821875"/>
<vertex x="-1.625240625" y="-1.46024375"/>
<vertex x="-1.81795" y="-1.215140625"/>
<vertex x="-1.8219125" y="-1.212025"/>
<vertex x="-1.8256875" y="-1.2053"/>
<vertex x="-1.83045" y="-1.19924375"/>
<vertex x="-1.831809375" y="-1.194396875"/>
<vertex x="-1.984446875" y="-0.9225375"/>
<vertex x="-1.98788125" y="-0.918846875"/>
<vertex x="-1.990575" y="-0.91161875"/>
<vertex x="-1.994346875" y="-0.904903125"/>
<vertex x="-1.99494375" y="-0.899903125"/>
<vertex x="-2.103871875" y="-0.6077375"/>
<vertex x="-2.1067" y="-0.603559375"/>
<vertex x="-2.10825" y="-0.596"/>
<vertex x="-2.110940625" y="-0.58878125"/>
<vertex x="-2.110759375" y="-0.58375"/>
<vertex x="-2.1733625" y="-0.27830625"/>
<vertex x="-2.17550625" y="-0.27375625"/>
<vertex x="-2.175871875" y="-0.26606875"/>
<vertex x="-2.177421875" y="-0.258503125"/>
<vertex x="-2.176465625" y="-0.253546875"/>
<vertex x="-2.19125625" y="0.0578875"/>
<vertex x="-2.192675" y="0.062721875"/>
<vertex x="-2.19185" y="0.070384375"/>
<vertex x="-2.192215625" y="0.0780875"/>
<vertex x="-2.190509375" y="0.082828125"/>
<vertex x="-2.15713125" y="0.392821875"/>
<vertex x="-2.1577875" y="0.39780625"/>
<vertex x="-2.155796875" y="0.405234375"/>
<vertex x="-2.15496875" y="0.412915625"/>
<vertex x="-2.152546875" y="0.417346875"/>
<vertex x="-2.07179375" y="0.718525"/>
<vertex x="-2.071671875" y="0.72355625"/>
<vertex x="-2.06855625" y="0.730596875"/>
<vertex x="-2.06655625" y="0.738053125"/>
<vertex x="-2.063484375" y="0.74205625"/>
<vertex x="-1.937303125" y="1.027134375"/>
<vertex x="-1.93640625" y="1.032103125"/>
<vertex x="-1.932228125" y="1.0386"/>
<vertex x="-1.929115625" y="1.045634375"/>
<vertex x="-1.925475" y="1.04910625"/>
<vertex x="-1.756853125" y="1.3113625"/>
<vertex x="-1.755203125" y="1.316121875"/>
<vertex x="-1.7500875" y="1.321884375"/>
<vertex x="-1.745915625" y="1.328371875"/>
<vertex x="-1.741775" y="1.331246875"/>
<vertex x="-1.53474375" y="1.5644125"/>
<vertex x="-1.53238125" y="1.568859375"/>
<vertex x="-1.52644375" y="1.573759375"/>
<vertex x="-1.52131875" y="1.57953125"/>
<vertex x="-1.516778125" y="1.581734375"/>
<vertex x="-1.2763125" y="1.7801875"/>
<vertex x="-1.27329375" y="1.78421875"/>
<vertex x="-1.266665625" y="1.78815"/>
<vertex x="-1.260715625" y="1.793059375"/>
<vertex x="-1.25589375" y="1.7945375"/>
<vertex x="-0.9877125" y="1.953571875"/>
<vertex x="-0.984109375" y="1.9570875"/>
<vertex x="-0.9769625" y="1.959946875"/>
<vertex x="-0.970321875" y="1.963884375"/>
<vertex x="-0.965328125" y="1.9646"/>
<vertex x="-0.67585625" y="2.0804"/>
<vertex x="-0.671753125" y="2.08331875"/>
<vertex x="-0.66424375" y="2.08504375"/>
<vertex x="-0.65708125" y="2.087909375"/>
<vertex x="-0.652040625" y="2.087846875"/>
<vertex x="-0.34816875" y="2.1576625"/>
<vertex x="-0.343665625" y="2.159915625"/>
<vertex x="-0.33598125" y="2.1604625"/>
<vertex x="-0.3284625" y="2.162190625"/>
<vertex x="-0.323490625" y="2.161353125"/>
<vertex x="-0.01250625" y="2.183509375"/>
<vertex x="-0.009640625" y="2.18461875"/>
<vertex x="-0.000009375" y="2.184396875"/>
<vertex x="0.009621875" y="2.185084375"/>
<vertex x="0.01254375" y="2.1841125"/>
<vertex x="0.329975" y="2.176846875"/>
<vertex x="0.334971875" y="2.177925"/>
<vertex x="0.342484375" y="2.176559375"/>
<vertex x="0.35011875" y="2.176384375"/>
<vertex x="0.354796875" y="2.17431875"/>
<vertex x="0.667184375" y="2.11751875"/>
<vertex x="0.672290625" y="2.117803125"/>
<vertex x="0.6795" y="2.115278125"/>
<vertex x="0.6870125" y="2.1139125"/>
<vertex x="0.69130625" y="2.11114375"/>
<vertex x="0.991003125" y="2.006190625"/>
<vertex x="0.996090625" y="2.005675"/>
<vertex x="1.002815625" y="2.00205625"/>
<vertex x="1.010021875" y="1.99953125"/>
<vertex x="1.01383125" y="1.996125"/>
<vertex x="1.293421875" y="1.845615625"/>
<vertex x="1.29836875" y="1.844309375"/>
<vertex x="1.304446875" y="1.83968125"/>
<vertex x="1.31116875" y="1.8360625"/>
<vertex x="1.3144" y="1.8321"/>
<vertex x="1.56701875" y="1.639728125"/>
<vertex x="1.571696875" y="1.637665625"/>
<vertex x="1.576978125" y="1.63214375"/>
<vertex x="1.58305" y="1.62751875"/>
<vertex x="1.585621875" y="1.623103125"/>
<vertex x="1.8050875" y="1.39358125"/>
<vertex x="1.80938125" y="1.390815625"/>
<vertex x="1.813721875" y="1.38455"/>
<vertex x="1.819009375" y="1.379021875"/>
<vertex x="1.8208625" y="1.37425"/>
<vertex x="2.001709375" y="1.11329375"/>
<vertex x="2.00551875" y="1.109890625"/>
<vertex x="2.00883125" y="1.103015625"/>
<vertex x="2.013184375" y="1.096734375"/>
<vertex x="2.014265625" y="1.091734375"/>
<vertex x="2.1521125" y="0.80566875"/>
<vertex x="2.155346875" y="0.801709375"/>
<vertex x="2.157546875" y="0.79439375"/>
<vertex x="2.160859375" y="0.78751875"/>
<vertex x="2.161146875" y="0.78241875"/>
<vertex x="2.252575" y="0.47831875"/>
<vertex x="2.255153125" y="0.473896875"/>
<vertex x="2.25618125" y="0.466321875"/>
<vertex x="2.258378125" y="0.45901875"/>
<vertex x="2.257865625" y="0.4539375"/>
<vertex x="2.30061875" y="0.139303125"/>
<vertex x="2.302475" y="0.13453125"/>
<vertex x="2.30230625" y="0.126884375"/>
<vertex x="2.303334375" y="0.119328125"/>
<vertex x="2.302034375" y="0.114390625"/>
<vertex x="2.295075" y="-0.2030625"/>
<vertex x="2.296159375" y="-0.208053125"/>
<vertex x="2.294803125" y="-0.215559375"/>
<vertex x="2.294634375" y="-0.223203125"/>
<vertex x="2.292571875" y="-0.2278875"/>
<vertex x="2.23606875" y="-0.54035"/>
<vertex x="2.236359375" y="-0.5454625"/>
<vertex x="2.2338375" y="-0.552684375"/>
<vertex x="2.23248125" y="-0.5601875"/>
<vertex x="2.22971875" y="-0.56448125"/>
<vertex x="2.1250625" y="-0.864271875"/>
<vertex x="2.12455" y="-0.869365625"/>
<vertex x="2.12093125" y="-0.876103125"/>
<vertex x="2.11841875" y="-0.8833"/>
<vertex x="2.115021875" y="-0.887109375"/>
<vertex x="1.964771875" y="-1.166859375"/>
<vertex x="1.963471875" y="-1.171809375"/>
<vertex x="1.958846875" y="-1.17789375"/>
<vertex x="1.9552375" y="-1.184615625"/>
<vertex x="1.951284375" y="-1.187846875"/>
<vertex x="1.75915625" y="-1.440659375"/>
<vertex x="1.7571" y="-1.4453375"/>
<vertex x="1.7515875" y="-1.45061875"/>
<vertex x="1.746965625" y="-1.4567"/>
<vertex x="1.74255" y="-1.459275"/>
<vertex x="1.5132625" y="-1.67894375"/>
<vertex x="1.5105" y="-1.683240625"/>
<vertex x="1.50423125" y="-1.68759375"/>
<vertex x="1.498715625" y="-1.692878125"/>
<vertex x="1.49395" y="-1.694734375"/>
<vertex x="1.233128125" y="-1.8758625"/>
<vertex x="1.22973125" y="-1.879675"/>
<vertex x="1.2228625" y="-1.88299375"/>
<vertex x="1.216584375" y="-1.887353125"/>
<vertex x="1.211584375" y="-1.888440625"/>
<vertex x="0.914396875" y="-2.032003125"/>
<vertex x="2.2352" y="-5.3848"/>
<vertex x="2.565403125" y="-5.232396875"/>
<vertex x="2.97179375" y="-5.00380625"/>
<vertex x="3.23046875" y="-4.83135625"/>
<vertex x="3.231015625" y="-4.8305875"/>
<vertex x="3.240975" y="-4.82435"/>
<vertex x="3.2507" y="-4.81786875"/>
<vertex x="3.251621875" y="-4.8176875"/>
<vertex x="3.252428125" y="-4.81718125"/>
<vertex x="3.2640375" y="-4.81523125"/>
<vertex x="3.27548125" y="-4.812965625"/>
<vertex x="3.27640625" y="-4.81315"/>
<vertex x="3.277340625" y="-4.81299375"/>
<vertex x="3.288784375" y="-4.815625"/>
<vertex x="3.30025" y="-4.81791875"/>
<vertex x="3.301034375" y="-4.81844375"/>
<vertex x="3.301959375" y="-4.81865625"/>
<vertex x="3.31154375" y="-4.82548125"/>
<vertex x="3.321240625" y="-4.831975"/>
<vertex x="3.3217625" y="-4.832759375"/>
<vertex x="4.953" y="-5.994403125"/>
<vertex x="5.384803125" y="-5.613396875"/>
<vertex x="5.791203125" y="-5.181596875"/>
<vertex x="6.070590625" y="-4.8514125"/>
<vertex x="6.248396875" y="-4.622803125"/>
<vertex x="5.077796875" y="-2.92915625"/>
<vertex x="5.072153125" y="-2.92295625"/>
<vertex x="5.070678125" y="-2.918859375"/>
<vertex x="5.068203125" y="-2.915278125"/>
<vertex x="5.0664375" y="-2.907078125"/>
<vertex x="5.063596875" y="-2.8991875"/>
<vertex x="5.063803125" y="-2.894840625"/>
<vertex x="5.062884375" y="-2.89058125"/>
<vertex x="5.06439375" y="-2.882328125"/>
<vertex x="5.0647875" y="-2.873953125"/>
<vertex x="5.06664375" y="-2.870009375"/>
<vertex x="5.067425" y="-2.86573125"/>
<vertex x="5.071971875" y="-2.8586875"/>
<vertex x="5.25780625" y="-2.4637875"/>
<vertex x="5.435603125" y="-1.9811875"/>
<vertex x="5.588" y="-1.47320625"/>
<vertex x="5.65403125" y="-1.209071875"/>
<vertex x="5.656421875" y="-1.1978375"/>
<vertex x="5.65708125" y="-1.19688125"/>
<vertex x="5.6573625" y="-1.19575"/>
<vertex x="5.6642125" y="-1.1865"/>
<vertex x="5.67073125" y="-1.17701875"/>
<vertex x="5.67170625" y="-1.176384375"/>
<vertex x="5.6724" y="-1.17545"/>
<vertex x="5.682303125" y="-1.169509375"/>
<vertex x="5.691921875" y="-1.163265625"/>
<vertex x="5.693059375" y="-1.16305625"/>
<vertex x="5.6940625" y="-1.162453125"/>
<vertex x="5.70550625" y="-1.16075"/>
<vertex x="7.7216" y="-0.787403125"/>
<vertex x="7.7216" y="0.0507875"/>
<vertex x="7.696203125" y="0.558784375"/>
<vertex x="7.6454" y="1.0922"/>
<vertex x="5.57788125" y="1.49075625"/>
<vertex x="5.5677375" y="1.491921875"/>
<vertex x="5.565559375" y="1.49313125"/>
<vertex x="5.5631125" y="1.493603125"/>
<vertex x="5.554590625" y="1.499225"/>
<vertex x="5.545653125" y="1.504190625"/>
<vertex x="5.5441" y="1.506146875"/>
<vertex x="5.542025" y="1.507515625"/>
<vertex x="5.53630625" y="1.515965625"/>
<vertex x="5.529946875" y="1.523975"/>
<vertex x="5.5292625" y="1.526371875"/>
<vertex x="5.527865625" y="1.528434375"/>
<vertex x="5.5258125" y="1.53844375"/>
<vertex x="5.384796875" y="2.032"/>
<vertex x="5.1562" y="2.5146"/>
<vertex x="5.003828125" y="2.79395"/>
<vertex x="4.873053125" y="3.011909375"/>
<vertex x="4.866709375" y="3.022040625"/>
<vertex x="4.86660625" y="3.022653125"/>
<vertex x="4.8662875" y="3.023184375"/>
<vertex x="4.8645125" y="3.035103125"/>
<vertex x="4.862521875" y="3.046953125"/>
<vertex x="4.862659375" y="3.04755625"/>
<vertex x="4.86256875" y="3.048171875"/>
<vertex x="4.865478125" y="3.0598125"/>
<vertex x="4.868184375" y="3.071571875"/>
<vertex x="4.868546875" y="3.07208125"/>
<vertex x="4.868696875" y="3.072678125"/>
<vertex x="4.875815625" y="3.082290625"/>
<vertex x="6.0452" y="4.724396875"/>
<vertex x="5.74874375" y="5.070265625"/>
<vertex x="5.748559375" y="5.070359375"/>
<vertex x="5.74065" y="5.079709375"/>
<vertex x="5.732440625" y="5.089284375"/>
<vertex x="5.732375" y="5.0894875"/>
<vertex x="5.461" y="5.410196875"/>
<vertex x="5.089521875" y="5.73215"/>
<vertex x="5.089246875" y="5.73224375"/>
<vertex x="5.08001875" y="5.7403875"/>
<vertex x="5.070653125" y="5.748503125"/>
<vertex x="5.070521875" y="5.748765625"/>
<vertex x="4.6482" y="6.1214"/>
<vertex x="2.904653125" y="4.925825"/>
<vertex x="2.895834375" y="4.919121875"/>
<vertex x="2.894278125" y="4.9187125"/>
<vertex x="2.892953125" y="4.917803125"/>
<vertex x="2.882125" y="4.9155125"/>
<vertex x="2.87140625" y="4.912690625"/>
<vertex x="2.8698125" y="4.912909375"/>
<vertex x="2.8682375" y="4.912575"/>
<vertex x="2.857353125" y="4.914603125"/>
<vertex x="2.846375" y="4.9161"/>
<vertex x="2.844984375" y="4.9169125"/>
<vertex x="2.843403125" y="4.91720625"/>
<vertex x="2.834121875" y="4.923246875"/>
<vertex x="2.565396875" y="5.080003125"/>
<vertex x="2.1336125" y="5.2578"/>
<vertex x="1.727178125" y="5.410209375"/>
<vertex x="1.3853625" y="5.52414375"/>
<vertex x="1.37585" y="5.5263875"/>
<vertex x="1.373453125" y="5.528115625"/>
<vertex x="1.37065625" y="5.529046875"/>
<vertex x="1.363271875" y="5.53545"/>
<vertex x="1.355353125" y="5.54115625"/>
<vertex x="1.353803125" y="5.5436625"/>
<vertex x="1.351571875" y="5.545596875"/>
<vertex x="1.34720625" y="5.554328125"/>
<vertex x="1.342065625" y="5.562640625"/>
<vertex x="1.341590625" y="5.5655625"/>
<vertex x="1.340275" y="5.56819375"/>
<vertex x="1.339584375" y="5.577909375"/>
<vertex x="1.016" y="7.5692"/>
<vertex x="0.533409375" y="7.62"/>
<vertex x="-0.507990625" y="7.62"/>
<vertex x="-0.9398" y="7.5692"/>
<vertex x="-1.03926875" y="6.997275"/>
<vertex x="-1.039165625" y="6.9967875"/>
<vertex x="-1.0414125" y="6.98493125"/>
<vertex x="-1.043509375" y="6.972884375"/>
<vertex x="-1.04378125" y="6.97245625"/>
<vertex x="-1.31313125" y="5.5522375"/>
<vertex x="-1.31395" y="5.542834375"/>
<vertex x="-1.31546875" y="5.5399125"/>
<vertex x="-1.31608125" y="5.536684375"/>
<vertex x="-1.32124375" y="5.52880625"/>
<vertex x="-1.325603125" y="5.520421875"/>
<vertex x="-1.328125" y="5.518303125"/>
<vertex x="-1.329928125" y="5.515553125"/>
<vertex x="-1.337728125" y="5.510240625"/>
<vertex x="-1.34495" y="5.504175"/>
<vertex x="-1.348084375" y="5.503184375"/>
<vertex x="-1.35080625" y="5.50133125"/>
<vertex x="-1.360040625" y="5.499409375"/>
<vertex x="-1.803415625" y="5.35939375"/>
<vertex x="-2.209784375" y="5.20700625"/>
<vertex x="-2.8841125" y="4.869846875"/>
<vertex x="-2.8923375" y="4.86473125"/>
<vertex x="-2.895334375" y="4.8642375"/>
<vertex x="-2.89805" y="4.862878125"/>
<vertex x="-2.907703125" y="4.862190625"/>
<vertex x="-2.9172625" y="4.8606125"/>
<vertex x="-2.920221875" y="4.861303125"/>
<vertex x="-2.923246875" y="4.8610875"/>
<vertex x="-2.93243125" y="4.864146875"/>
<vertex x="-2.9418625" y="4.866346875"/>
<vertex x="-2.944328125" y="4.8681125"/>
<vertex x="-2.9472125" y="4.869075"/>
<vertex x="-2.9545375" y="4.875428125"/>
<vertex x="-4.6228" y="6.0706"/>
<vertex x="-5.0037875" y="5.740409375"/>
<vertex x="-5.334003125" y="5.43559375"/>
<vertex x="-5.67279375" y="5.07074375"/>
<vertex x="-5.9944" y="4.724403125"/>
<vertex x="-4.832015625" y="3.042653125"/>
<vertex x="-4.8316125" y="3.042384375"/>
<vertex x="-4.824865625" y="3.032309375"/>
<vertex x="-4.817928125" y="3.022271875"/>
<vertex x="-4.817825" y="3.021796875"/>
<vertex x="-4.81755625" y="3.02139375"/>
<vertex x="-4.81518125" y="3.009515625"/>
<vertex x="-4.812609375" y="2.997575"/>
<vertex x="-4.812696875" y="2.99709375"/>
<vertex x="-4.812603125" y="2.996625"/>
<vertex x="-4.814953125" y="2.98474375"/>
<vertex x="-4.81715" y="2.972725"/>
<vertex x="-4.8174125" y="2.97231875"/>
<vertex x="-4.81750625" y="2.97184375"/>
<vertex x="-4.824190625" y="2.961815625"/>
<vertex x="-4.83085" y="2.9515"/>
<vertex x="-4.83125625" y="2.95121875"/>
<vertex x="-5.0546" y="2.6162"/>
<vertex x="-5.25780625" y="2.184384375"/>
<vertex x="-5.410190625" y="1.778021875"/>
<vertex x="-5.551075" y="1.284928125"/>
<vertex x="-5.553884375" y="1.273721875"/>
<vertex x="-5.554521875" y="1.2728625"/>
<vertex x="-5.55481875" y="1.271825"/>
<vertex x="-5.56203125" y="1.2627375"/>
<vertex x="-5.568934375" y="1.253434375"/>
<vertex x="-5.56985625" y="1.25288125"/>
<vertex x="-5.570525" y="1.252040625"/>
<vertex x="-5.580675" y="1.246403125"/>
<vertex x="-5.59060625" y="1.240453125"/>
<vertex x="-5.591665625" y="1.240296875"/>
<vertex x="-5.592609375" y="1.239771875"/>
<vertex x="-5.604184375" y="1.238440625"/>
<vertex x="-7.620003125" y="0.9398"/>
<vertex x="-7.6454" y="0.60960625"/>
<vertex x="-7.6454" y="-0.533409375"/>
<vertex x="-7.619996875" y="-0.9398"/>
<vertex x="-5.6043" y="-1.313078125"/>
<vertex x="-5.592040625" y="-1.315234375"/>
<vertex x="-5.5917875" y="-1.315396875"/>
<vertex x="-5.59149375" y="-1.31545"/>
<vertex x="-5.581034375" y="-1.322240625"/>
<vertex x="-5.570728125" y="-1.328796875"/>
<vertex x="-5.570559375" y="-1.3290375"/>
<vertex x="-5.570303125" y="-1.329203125"/>
<vertex x="-5.563259375" y="-1.339453125"/>
<vertex x="-5.556228125" y="-1.34948125"/>
<vertex x="-5.5561625" y="-1.349775"/>
<vertex x="-5.55599375" y="-1.350021875"/>
<vertex x="-5.553434375" y="-1.362046875"/>
<vertex x="-5.461" y="-1.77800625"/>
<vertex x="-5.308584375" y="-2.159034375"/>
<vertex x="-5.1054125" y="-2.616175"/>
<vertex x="-4.845175" y="-3.112990625"/>
<vertex x="-4.839834375" y="-3.1213875"/>
<vertex x="-4.839359375" y="-3.1240875"/>
<vertex x="-4.838084375" y="-3.126525"/>
<vertex x="-4.837190625" y="-3.136459375"/>
<vertex x="-4.83546875" y="-3.14626875"/>
<vertex x="-4.836065625" y="-3.148946875"/>
<vertex x="-4.83581875" y="-3.151684375"/>
<vertex x="-4.83879375" y="-3.16120625"/>
<vertex x="-4.840959375" y="-3.170925"/>
<vertex x="-4.842534375" y="-3.173171875"/>
<vertex x="-4.84335625" y="-3.175796875"/>
<vertex x="-4.849753125" y="-3.183459375"/>
<vertex x="-6.0198" y="-4.8514"/>
<vertex x="-5.715015625" y="-5.20698125"/>
<vertex x="-5.410190625" y="-5.53720625"/>
</polygon>
</package>
<package name="OSHW_8MM" library_version="252">
<polygon width="0.0508" layer="25" pour="solid">
<vertex x="1.20990625" y="2.59544375"/>
<vertex x="1.016003125" y="2.6924"/>
<vertex x="0.848315625" y="2.740309375"/>
<vertex x="0.681434375" y="2.78799375"/>
<vertex x="0.676425" y="2.787259375"/>
<vertex x="0.671709375" y="2.790771875"/>
<vertex x="0.6660625" y="2.792384375"/>
<vertex x="0.66360625" y="2.79680625"/>
<vertex x="0.659546875" y="2.799828125"/>
<vertex x="0.658696875" y="2.80564375"/>
<vertex x="0.65584375" y="2.810778125"/>
<vertex x="0.657234375" y="2.815640625"/>
<vertex x="0.508" y="3.8354"/>
<vertex x="-0.4572" y="3.8354"/>
<vertex x="-0.65624375" y="2.815309375"/>
<vertex x="-0.6550375" y="2.8108875"/>
<vertex x="-0.658178125" y="2.8053875"/>
<vertex x="-0.65939375" y="2.7991625"/>
<vertex x="-0.6632" y="2.7966"/>
<vertex x="-0.665475" y="2.792615625"/>
<vertex x="-0.67159375" y="2.790946875"/>
<vertex x="-0.676846875" y="2.787409375"/>
<vertex x="-0.681346875" y="2.7882875"/>
<vertex x="-0.9398" y="2.7178"/>
<vertex x="-1.4285" y="2.473446875"/>
<vertex x="-1.430884375" y="2.47006875"/>
<vertex x="-1.43755625" y="2.46891875"/>
<vertex x="-1.44361875" y="2.4658875"/>
<vertex x="-1.44754375" y="2.467196875"/>
<vertex x="-1.451621875" y="2.46649375"/>
<vertex x="-1.457159375" y="2.470403125"/>
<vertex x="-1.46358125" y="2.47254375"/>
<vertex x="-1.46543125" y="2.476240625"/>
<vertex x="-2.3114" y="3.0734"/>
<vertex x="-2.48918125" y="2.92101875"/>
<vertex x="-2.6744375" y="2.735759375"/>
<vertex x="-2.84481875" y="2.56538125"/>
<vertex x="-2.9972" y="2.387603125"/>
<vertex x="-2.4251375" y="1.541946875"/>
<vertex x="-2.42125" y="1.5397875"/>
<vertex x="-2.419475" y="1.533575"/>
<vertex x="-2.415846875" y="1.5282125"/>
<vertex x="-2.416690625" y="1.523834375"/>
<vertex x="-2.41546875" y="1.51955625"/>
<vertex x="-2.41860625" y="1.51390625"/>
<vertex x="-2.419834375" y="1.50755"/>
<vertex x="-2.423528125" y="1.505053125"/>
<vertex x="-2.539990625" y="1.29541875"/>
<vertex x="-2.6416" y="1.066803125"/>
<vertex x="-2.71410625" y="0.873453125"/>
<vertex x="-2.714103125" y="0.87345"/>
<vertex x="-2.78644375" y="0.680553125"/>
<vertex x="-2.785859375" y="0.676665625"/>
<vertex x="-2.790003125" y="0.671059375"/>
<vertex x="-2.79245" y="0.664534375"/>
<vertex x="-2.796028125" y="0.662909375"/>
<vertex x="-2.798365625" y="0.65974375"/>
<vertex x="-2.8052625" y="0.658709375"/>
<vertex x="-2.81160625" y="0.655825"/>
<vertex x="-2.8152875" y="0.65720625"/>
<vertex x="-3.81" y="0.508003125"/>
<vertex x="-3.8354" y="0.2032125"/>
<vertex x="-3.8354" y="-0.4572"/>
<vertex x="-2.79048125" y="-0.631353125"/>
<vertex x="-2.784734375" y="-0.630203125"/>
<vertex x="-2.78051875" y="-0.6330125"/>
<vertex x="-2.775515625" y="-0.633846875"/>
<vertex x="-2.772103125" y="-0.638621875"/>
<vertex x="-2.767225" y="-0.641875"/>
<vertex x="-2.76623125" y="-0.64684375"/>
<vertex x="-2.763284375" y="-0.65096875"/>
<vertex x="-2.76425" y="-0.65675625"/>
<vertex x="-2.717803125" y="-0.888996875"/>
<vertex x="-2.6162" y="-1.142996875"/>
<vertex x="-2.51458125" y="-1.3716375"/>
<vertex x="-2.422696875" y="-1.5554"/>
<vertex x="-2.41908125" y="-1.5581125"/>
<vertex x="-2.418178125" y="-1.5644375"/>
<vertex x="-2.4153125" y="-1.57016875"/>
<vertex x="-2.416746875" y="-1.574465625"/>
<vertex x="-2.41610625" y="-1.578940625"/>
<vertex x="-2.41994375" y="-1.58405625"/>
<vertex x="-2.42196875" y="-1.59013125"/>
<vertex x="-2.42601875" y="-1.59215625"/>
<vertex x="-3.0226" y="-2.3876"/>
<vertex x="-2.87021875" y="-2.56538125"/>
<vertex x="-2.684959375" y="-2.7506375"/>
<vertex x="-2.5145875" y="-2.9210125"/>
<vertex x="-2.3622" y="-3.048"/>
<vertex x="-1.51659375" y="-2.47596875"/>
<vertex x="-1.51430625" y="-2.4719625"/>
<vertex x="-1.50821875" y="-2.470303125"/>
<vertex x="-1.502990625" y="-2.466765625"/>
<vertex x="-1.49845625" y="-2.467640625"/>
<vertex x="-1.49400625" y="-2.466428125"/>
<vertex x="-1.48853125" y="-2.46955625"/>
<vertex x="-1.482328125" y="-2.470753125"/>
<vertex x="-1.4797375" y="-2.47458125"/>
<vertex x="-1.320790625" y="-2.56540625"/>
<vertex x="-1.15255625" y="-2.63750625"/>
<vertex x="-1.151734375" y="-2.63723125"/>
<vertex x="-1.14298125" y="-2.641609375"/>
<vertex x="-1.134009375" y="-2.645453125"/>
<vertex x="-1.1336875" y="-2.646253125"/>
<vertex x="-0.9906" y="-2.717803125"/>
<vertex x="-0.381" y="-0.9906"/>
<vertex x="-0.550484375" y="-0.90961875"/>
<vertex x="-0.553140625" y="-0.910028125"/>
<vertex x="-0.559715625" y="-0.90520625"/>
<vertex x="-0.56705" y="-0.901703125"/>
<vertex x="-0.56794375" y="-0.899175"/>
<vertex x="-0.71119375" y="-0.79415"/>
<vertex x="-0.71388125" y="-0.794059375"/>
<vertex x="-0.719446875" y="-0.7881"/>
<vertex x="-0.726003125" y="-0.78329375"/>
<vertex x="-0.7264125" y="-0.78064375"/>
<vertex x="-0.847659375" y="-0.65083125"/>
<vertex x="-0.850275" y="-0.65024375"/>
<vertex x="-0.854621875" y="-0.643378125"/>
<vertex x="-0.8601875" y="-0.63741875"/>
<vertex x="-0.860096875" y="-0.63473125"/>
<vertex x="-0.9551" y="-0.484671875"/>
<vertex x="-0.95756875" y="-0.48360625"/>
<vertex x="-0.960575" y="-0.476025"/>
<vertex x="-0.964921875" y="-0.469159375"/>
<vertex x="-0.964334375" y="-0.46654375"/>
<vertex x="-1.02981875" y="-0.301415625"/>
<vertex x="-1.032040625" y="-0.2999125"/>
<vertex x="-1.033584375" y="-0.291921875"/>
<vertex x="-1.036584375" y="-0.284353125"/>
<vertex x="-1.03551875" y="-0.2818875"/>
<vertex x="-1.06918125" y="-0.10748125"/>
<vertex x="-1.071084375" y="-0.105590625"/>
<vertex x="-1.071115625" y="-0.097459375"/>
<vertex x="-1.072659375" y="-0.089459375"/>
<vertex x="-1.071153125" y="-0.087234375"/>
<vertex x="-1.071825" y="0.09039375"/>
<vertex x="-1.07334375" y="0.092603125"/>
<vertex x="-1.0718625" y="0.1006"/>
<vertex x="-1.07189375" y="0.108746875"/>
<vertex x="-1.07" y="0.110653125"/>
<vertex x="-1.037659375" y="0.285303125"/>
<vertex x="-1.038740625" y="0.287759375"/>
<vertex x="-1.0358" y="0.29534375"/>
<vertex x="-1.034315625" y="0.303353125"/>
<vertex x="-1.032103125" y="0.304875"/>
<vertex x="-0.967875" y="0.470478125"/>
<vertex x="-0.96848125" y="0.473090625"/>
<vertex x="-0.964184375" y="0.479990625"/>
<vertex x="-0.9612375" y="0.487590625"/>
<vertex x="-0.958778125" y="0.488675"/>
<vertex x="-0.864896875" y="0.639465625"/>
<vertex x="-0.865009375" y="0.64214375"/>
<vertex x="-0.85950625" y="0.648125"/>
<vertex x="-0.855196875" y="0.655046875"/>
<vertex x="-0.852578125" y="0.65565625"/>
<vertex x="-0.73231875" y="0.786375"/>
<vertex x="-0.73193125" y="0.78903125"/>
<vertex x="-0.7254" y="0.79389375"/>
<vertex x="-0.719890625" y="0.799884375"/>
<vertex x="-0.717209375" y="0.799996875"/>
<vertex x="-0.57474375" y="0.9061"/>
<vertex x="-0.57386875" y="0.9086375"/>
<vertex x="-0.566553125" y="0.9122"/>
<vertex x="-0.560021875" y="0.917065625"/>
<vertex x="-0.557365625" y="0.916678125"/>
<vertex x="-0.397690625" y="0.99445625"/>
<vertex x="-0.396359375" y="0.996784375"/>
<vertex x="-0.3885125" y="0.998925"/>
<vertex x="-0.3811875" y="1.00249375"/>
<vertex x="-0.378646875" y="1.00161875"/>
<vertex x="-0.2072875" y="1.04838125"/>
<vertex x="-0.205546875" y="1.050421875"/>
<vertex x="-0.197434375" y="1.05106875"/>
<vertex x="-0.189578125" y="1.0532125"/>
<vertex x="-0.187246875" y="1.05188125"/>
<vertex x="-0.010290625" y="1.06598125"/>
<vertex x="-0.009109375" y="1.067090625"/>
<vertex x="-0.000028125" y="1.0668"/>
<vertex x="0.0090625" y="1.067525"/>
<vertex x="0.0103" y="1.06646875"/>
<vertex x="0.190590625" y="1.06070625"/>
<vertex x="0.19288125" y="1.062159375"/>
<vertex x="0.200815625" y="1.06038125"/>
<vertex x="0.20891875" y="1.060121875"/>
<vertex x="0.21076875" y="1.05815"/>
<vertex x="0.38686875" y="1.018671875"/>
<vertex x="0.389384375" y="1.019665625"/>
<vertex x="0.396821875" y="1.016440625"/>
<vertex x="0.404753125" y="1.0146625"/>
<vertex x="0.406203125" y="1.012371875"/>
<vertex x="0.57176875" y="0.94058125"/>
<vertex x="0.574428125" y="0.9410875"/>
<vertex x="0.58113125" y="0.936521875"/>
<vertex x="0.5885875" y="0.933290625"/>
<vertex x="0.589584375" y="0.93076875"/>
<vertex x="0.7387625" y="0.82920625"/>
<vertex x="0.741471875" y="0.829203125"/>
<vertex x="0.747209375" y="0.82345625"/>
<vertex x="0.75391875" y="0.8188875"/>
<vertex x="0.754425" y="0.816228125"/>
<vertex x="0.88191875" y="0.688490625"/>
<vertex x="0.88458125" y="0.68798125"/>
<vertex x="0.889140625" y="0.681259375"/>
<vertex x="0.894871875" y="0.675515625"/>
<vertex x="0.89486875" y="0.672809375"/>
<vertex x="0.996153125" y="0.523440625"/>
<vertex x="0.998671875" y="0.522440625"/>
<vertex x="1.001890625" y="0.51498125"/>
<vertex x="1.00644375" y="0.508265625"/>
<vertex x="1.005934375" y="0.50560625"/>
<vertex x="1.077415625" y="0.339903125"/>
<vertex x="1.0797" y="0.33845"/>
<vertex x="1.081459375" y="0.330528125"/>
<vertex x="1.084678125" y="0.32306875"/>
<vertex x="1.083678125" y="0.32055"/>
<vertex x="1.122825" y="0.144384375"/>
<vertex x="1.124796875" y="0.142528125"/>
<vertex x="1.125040625" y="0.13441875"/>
<vertex x="1.126803125" y="0.1264875"/>
<vertex x="1.125346875" y="0.1242"/>
<vertex x="1.130778125" y="-0.056184375"/>
<vertex x="1.132365625" y="-0.058378125"/>
<vertex x="1.131084375" y="-0.06639375"/>
<vertex x="1.131328125" y="-0.074509375"/>
<vertex x="1.129471875" y="-0.07648125"/>
<vertex x="1.100978125" y="-0.25469375"/>
<vertex x="1.102128125" y="-0.25714375"/>
<vertex x="1.099365625" y="-0.264778125"/>
<vertex x="1.098084375" y="-0.272796875"/>
<vertex x="1.095890625" y="-0.274384375"/>
<vertex x="1.034496875" y="-0.4440875"/>
<vertex x="1.035165625" y="-0.4467125"/>
<vertex x="1.031021875" y="-0.45369375"/>
<vertex x="1.028259375" y="-0.461328125"/>
<vertex x="1.02580625" y="-0.462478125"/>
<vertex x="0.933684375" y="-0.617665625"/>
<vertex x="0.93385" y="-0.620365625"/>
<vertex x="0.928478125" y="-0.6264375"/>
<vertex x="0.924328125" y="-0.633428125"/>
<vertex x="0.9217" y="-0.634096875"/>
<vertex x="0.802128125" y="-0.76925"/>
<vertex x="0.801784375" y="-0.7719375"/>
<vertex x="0.795359375" y="-0.7769"/>
<vertex x="0.78998125" y="-0.78298125"/>
<vertex x="0.787278125" y="-0.783146875"/>
<vertex x="0.644459375" y="-0.893503125"/>
<vertex x="0.643615625" y="-0.896078125"/>
<vertex x="0.63636875" y="-0.899753125"/>
<vertex x="0.62995" y="-0.9047125"/>
<vertex x="0.627265625" y="-0.90436875"/>
<vertex x="0.4572" y="-0.9906"/>
<vertex x="1.117596875" y="-2.6924"/>
<vertex x="1.286265625" y="-2.596021875"/>
<vertex x="1.286265625" y="-2.59601875"/>
<vertex x="1.63200625" y="-2.39845625"/>
<vertex x="1.634990625" y="-2.39435625"/>
<vertex x="1.6407875" y="-2.393440625"/>
<vertex x="1.645878125" y="-2.39053125"/>
<vertex x="1.650765625" y="-2.3918625"/>
<vertex x="1.655775" y="-2.391071875"/>
<vertex x="1.660521875" y="-2.394521875"/>
<vertex x="1.666178125" y="-2.396065625"/>
<vertex x="1.668690625" y="-2.400465625"/>
<vertex x="2.489196875" y="-2.9972"/>
<vertex x="2.659559375" y="-2.8268375"/>
<vertex x="2.81941875" y="-2.66698125"/>
<vertex x="2.978646875" y="-2.4812125"/>
<vertex x="2.97865" y="-2.4812125"/>
<vertex x="3.1242" y="-2.3114"/>
<vertex x="2.526734375" y="-1.4152"/>
<vertex x="2.522446875" y="-1.41251875"/>
<vertex x="2.521121875" y="-1.40678125"/>
<vertex x="2.5178625" y="-1.401890625"/>
<vertex x="2.518853125" y="-1.396940625"/>
<vertex x="2.517715625" y="-1.392015625"/>
<vertex x="2.520834375" y="-1.387025"/>
<vertex x="2.5219875" y="-1.381259375"/>
<vertex x="2.5261875" y="-1.378459375"/>
<vertex x="2.641596875" y="-1.193803125"/>
<vertex x="2.714471875" y="-0.975184375"/>
<vertex x="2.714471875" y="-0.97518125"/>
<vertex x="2.793996875" y="-0.736615625"/>
<vertex x="2.838825" y="-0.5797125"/>
<vertex x="2.838015625" y="-0.575090625"/>
<vertex x="2.84160625" y="-0.569978125"/>
<vertex x="2.84331875" y="-0.56398125"/>
<vertex x="2.847415625" y="-0.56170625"/>
<vertex x="2.850109375" y="-0.55786875"/>
<vertex x="2.856259375" y="-0.55679375"/>
<vertex x="2.8617125" y="-0.5537625"/>
<vertex x="2.86621875" y="-0.55505"/>
<vertex x="3.8608" y="-0.381"/>
<vertex x="3.8862" y="-0.10159375"/>
<vertex x="3.8862" y="0.126990625"/>
<vertex x="3.86210625" y="0.319734375"/>
<vertex x="3.861821875" y="0.319965625"/>
<vertex x="3.860803125" y="0.3301375"/>
<vertex x="3.859540625" y="0.34025"/>
<vertex x="3.859765625" y="0.3405375"/>
<vertex x="3.8354" y="0.5842"/>
<vertex x="2.8151875" y="0.758384375"/>
<vertex x="2.811621875" y="0.7570875"/>
<vertex x="2.80518125" y="0.76009375"/>
<vertex x="2.79818125" y="0.7612875"/>
<vertex x="2.795990625" y="0.76438125"/>
<vertex x="2.792553125" y="0.765984375"/>
<vertex x="2.790125" y="0.772659375"/>
<vertex x="2.78601875" y="0.77845625"/>
<vertex x="2.78665625" y="0.782196875"/>
<vertex x="2.695959375" y="1.031615625"/>
<vertex x="2.695603125" y="1.03179375"/>
<vertex x="2.692390625" y="1.041428125"/>
<vertex x="2.68890625" y="1.0510125"/>
<vertex x="2.689075" y="1.051375"/>
<vertex x="2.641596875" y="1.1938"/>
<vertex x="2.540021875" y="1.371565625"/>
<vertex x="2.424796875" y="1.555925"/>
<vertex x="2.4200375" y="1.55964375"/>
<vertex x="2.419440625" y="1.56449375"/>
<vertex x="2.41685" y="1.568640625"/>
<vertex x="2.418209375" y="1.574528125"/>
<vertex x="2.417471875" y="1.580528125"/>
<vertex x="2.42048125" y="1.58438125"/>
<vertex x="2.42158125" y="1.58914375"/>
<vertex x="2.42670625" y="1.592346875"/>
<vertex x="3.048" y="2.3876"/>
<vertex x="2.89561875" y="2.56538125"/>
<vertex x="2.717803125" y="2.743196875"/>
<vertex x="2.522515625" y="2.914071875"/>
<vertex x="2.3114" y="3.0988"/>
<vertex x="1.465303125" y="2.50155"/>
<vertex x="1.463871875" y="2.498334375"/>
<vertex x="1.457003125" y="2.49569375"/>
<vertex x="1.4509875" y="2.491446875"/>
<vertex x="1.44751875" y="2.49204375"/>
<vertex x="1.44423125" y="2.49078125"/>
<vertex x="1.437503125" y="2.493771875"/>
<vertex x="1.43025" y="2.495021875"/>
<vertex x="1.42821875" y="2.497896875"/>
<vertex x="1.228765625" y="2.586546875"/>
<vertex x="1.228128125" y="2.586334375"/>
<vertex x="1.21920625" y="2.59079375"/>
<vertex x="1.210146875" y="2.594821875"/>
</polygon>
</package>
<package name="OSHW_5MM" library_version="252">
<polygon width="0.0508" layer="25" pour="solid">
<vertex x="-1.498575" y="-1.752621875"/>
<vertex x="-1.4097" y="-1.8288"/>
<vertex x="-0.906828125" y="-1.485371875"/>
<vertex x="-0.904903125" y="-1.481684375"/>
<vertex x="-0.898465625" y="-1.4796625"/>
<vertex x="-0.8928875" y="-1.475853125"/>
<vertex x="-0.88879375" y="-1.476625"/>
<vertex x="-0.884828125" y="-1.475378125"/>
<vertex x="-0.878846875" y="-1.4785"/>
<vertex x="-0.872209375" y="-1.47975"/>
<vertex x="-0.8698625" y="-1.4831875"/>
<vertex x="-0.5969" y="-1.625596875"/>
<vertex x="-0.2413" y="-0.609596875"/>
<vertex x="-0.358453125" y="-0.55098125"/>
<vertex x="-0.361521875" y="-0.55130625"/>
<vertex x="-0.36756875" y="-0.54641875"/>
<vertex x="-0.374515625" y="-0.54294375"/>
<vertex x="-0.375490625" y="-0.54001875"/>
<vertex x="-0.4694625" y="-0.464075"/>
<vertex x="-0.472528125" y="-0.4637375"/>
<vertex x="-0.477384375" y="-0.457675"/>
<vertex x="-0.483428125" y="-0.452790625"/>
<vertex x="-0.483753125" y="-0.449721875"/>
<vertex x="-0.559275" y="-0.355434375"/>
<vertex x="-0.5622" y="-0.354446875"/>
<vertex x="-0.56565" y="-0.347475"/>
<vertex x="-0.570503125" y="-0.341415625"/>
<vertex x="-0.570165625" y="-0.338353125"/>
<vertex x="-0.623753125" y="-0.2300625"/>
<vertex x="-0.62639375" y="-0.228475"/>
<vertex x="-0.62826875" y="-0.2209375"/>
<vertex x="-0.631715625" y="-0.213971875"/>
<vertex x="-0.630728125" y="-0.21105"/>
<vertex x="-0.65989375" y="-0.09380625"/>
<vertex x="-0.6621375" y="-0.091684375"/>
<vertex x="-0.66235625" y="-0.08390625"/>
<vertex x="-0.664228125" y="-0.076378125"/>
<vertex x="-0.662640625" y="-0.0737375"/>
<vertex x="-0.666021875" y="0.047034375"/>
<vertex x="-0.667759375" y="0.0495875"/>
<vertex x="-0.66630625" y="0.057228125"/>
<vertex x="-0.666525" y="0.0649875"/>
<vertex x="-0.664409375" y="0.067228125"/>
<vertex x="-0.641859375" y="0.185915625"/>
<vertex x="-0.64300625" y="0.188775"/>
<vertex x="-0.639959375" y="0.1959125"/>
<vertex x="-0.63850625" y="0.20355625"/>
<vertex x="-0.635953125" y="0.20529375"/>
<vertex x="-0.5885125" y="0.316409375"/>
<vertex x="-0.589021875" y="0.31945625"/>
<vertex x="-0.58450625" y="0.3257875"/>
<vertex x="-0.581459375" y="0.332928125"/>
<vertex x="-0.5786" y="0.334078125"/>
<vertex x="-0.508475" y="0.43244375"/>
<vertex x="-0.508321875" y="0.435521875"/>
<vertex x="-0.5025625" y="0.440734375"/>
<vertex x="-0.49805" y="0.447065625"/>
<vertex x="-0.49500625" y="0.447575"/>
<vertex x="-0.405428125" y="0.528665625"/>
<vertex x="-0.40461875" y="0.531640625"/>
<vertex x="-0.397875" y="0.5355"/>
<vertex x="-0.392115625" y="0.540715625"/>
<vertex x="-0.389034375" y="0.5405625"/>
<vertex x="-0.284190625" y="0.600590625"/>
<vertex x="-0.2827625" y="0.603325"/>
<vertex x="-0.27535625" y="0.60565"/>
<vertex x="-0.268609375" y="0.6095125"/>
<vertex x="-0.26563125" y="0.608703125"/>
<vertex x="-0.150359375" y="0.644884375"/>
<vertex x="-0.148378125" y="0.64725"/>
<vertex x="-0.1406375" y="0.6479375"/>
<vertex x="-0.133225" y="0.6502625"/>
<vertex x="-0.130490625" y="0.648834375"/>
<vertex x="-0.010153125" y="0.6595"/>
<vertex x="-0.007775" y="0.66135625"/>
<vertex x="0.0000125" y="0.6604"/>
<vertex x="0.007809375" y="0.661090625"/>
<vertex x="0.010115625" y="0.659159375"/>
<vertex x="0.125928125" y="0.6449375"/>
<vertex x="0.128565625" y="0.6462375"/>
<vertex x="0.13605" y="0.64369375"/>
<vertex x="0.14389375" y="0.64273125"/>
<vertex x="0.145703125" y="0.640415625"/>
<vertex x="0.256196875" y="0.602875"/>
<vertex x="0.25904375" y="0.603609375"/>
<vertex x="0.265846875" y="0.599596875"/>
<vertex x="0.273334375" y="0.597053125"/>
<vertex x="0.274634375" y="0.594415625"/>
<vertex x="0.375140625" y="0.53514375"/>
<vertex x="0.378078125" y="0.53528125"/>
<vertex x="0.383928125" y="0.529959375"/>
<vertex x="0.39073125" y="0.525946875"/>
<vertex x="0.391465625" y="0.5231"/>
<vertex x="0.477775" y="0.44456875"/>
<vertex x="0.48068125" y="0.44410625"/>
<vertex x="0.485321875" y="0.437703125"/>
<vertex x="0.491165625" y="0.4323875"/>
<vertex x="0.491303125" y="0.429453125"/>
<vertex x="0.559790625" y="0.334978125"/>
<vertex x="0.562540625" y="0.33393125"/>
<vertex x="0.565778125" y="0.32671875"/>
<vertex x="0.570415625" y="0.320321875"/>
<vertex x="0.569953125" y="0.31741875"/>
<vertex x="0.61773125" y="0.21096875"/>
<vertex x="0.620209375" y="0.209384375"/>
<vertex x="0.62190625" y="0.20166875"/>
<vertex x="0.62514375" y="0.19445625"/>
<vertex x="0.624096875" y="0.19170625"/>
<vertex x="0.64916875" y="0.07774375"/>
<vertex x="0.651271875" y="0.0756875"/>
<vertex x="0.651359375" y="0.067784375"/>
<vertex x="0.653059375" y="0.0600625"/>
<vertex x="0.651475" y="0.057584375"/>
<vertex x="0.65278125" y="-0.05908125"/>
<vertex x="0.654421875" y="-0.061525"/>
<vertex x="0.652896875" y="-0.0692875"/>
<vertex x="0.652984375" y="-0.077184375"/>
<vertex x="0.65093125" y="-0.079284375"/>
<vertex x="0.62841875" y="-0.193784375"/>
<vertex x="0.629525" y="-0.19650625"/>
<vertex x="0.626453125" y="-0.20378125"/>
<vertex x="0.624928125" y="-0.211540625"/>
<vertex x="0.622484375" y="-0.21318125"/>
<vertex x="0.577096875" y="-0.320678125"/>
<vertex x="0.577625" y="-0.32356875"/>
<vertex x="0.57313125" y="-0.33006875"/>
<vertex x="0.57005625" y="-0.337353125"/>
<vertex x="0.56733125" y="-0.338459375"/>
<vertex x="0.500978125" y="-0.434453125"/>
<vertex x="0.50090625" y="-0.437390625"/>
<vertex x="0.49518125" y="-0.4428375"/>
<vertex x="0.4906875" y="-0.449340625"/>
<vertex x="0.48779375" y="-0.44986875"/>
<vertex x="0.403278125" y="-0.5303"/>
<vertex x="0.40260625" y="-0.533165625"/>
<vertex x="0.395884375" y="-0.537334375"/>
<vertex x="0.3901625" y="-0.54278125"/>
<vertex x="0.387225" y="-0.542709375"/>
<vertex x="0.2794" y="-0.6096"/>
<vertex x="0.6731" y="-1.6002"/>
<vertex x="0.82550625" y="-1.536696875"/>
<vertex x="0.95948125" y="-1.44738125"/>
<vertex x="0.962775" y="-1.442675"/>
<vertex x="0.96789375" y="-1.441771875"/>
<vertex x="0.972215625" y="-1.438890625"/>
<vertex x="0.97784375" y="-1.440015625"/>
<vertex x="0.983496875" y="-1.43901875"/>
<vertex x="0.98775" y="-1.441996875"/>
<vertex x="0.992846875" y="-1.443015625"/>
<vertex x="0.99603125" y="-1.44779375"/>
<vertex x="1.4859" y="-1.7907"/>
<vertex x="1.5875" y="-1.7018"/>
<vertex x="1.676390625" y="-1.612909375"/>
<vertex x="1.77798125" y="-1.498625"/>
<vertex x="1.8669" y="-1.3843"/>
<vertex x="1.510990625" y="-0.868840625"/>
<vertex x="1.5067875" y="-0.866390625"/>
<vertex x="1.50524375" y="-0.86051875"/>
<vertex x="1.501796875" y="-0.855528125"/>
<vertex x="1.502671875" y="-0.850746875"/>
<vertex x="1.501434375" y="-0.84604375"/>
<vertex x="1.504490625" y="-0.840803125"/>
<vertex x="1.505584375" y="-0.834828125"/>
<vertex x="1.509590625" y="-0.8320625"/>
<vertex x="1.587496875" y="-0.69850625"/>
<vertex x="1.638296875" y="-0.546115625"/>
<vertex x="1.6838" y="-0.3641"/>
<vertex x="1.68289375" y="-0.35916875"/>
<vertex x="1.686246875" y="-0.354303125"/>
<vertex x="1.687684375" y="-0.348559375"/>
<vertex x="1.691990625" y="-0.345975"/>
<vertex x="1.6948375" y="-0.341846875"/>
<vertex x="1.700653125" y="-0.340778125"/>
<vertex x="1.705728125" y="-0.337734375"/>
<vertex x="1.7106" y="-0.338953125"/>
<vertex x="2.3114" y="-0.2286"/>
<vertex x="2.3114" y="0.06349375"/>
<vertex x="2.298721875" y="0.20295"/>
<vertex x="2.286" y="0.3302"/>
<vertex x="1.684975" y="0.440590625"/>
<vertex x="1.6808625" y="0.439325"/>
<vertex x="1.675015625" y="0.442421875"/>
<vertex x="1.668509375" y="0.443615625"/>
<vertex x="1.666065625" y="0.447159375"/>
<vertex x="1.662265625" y="0.449171875"/>
<vertex x="1.660321875" y="0.455490625"/>
<vertex x="1.656565625" y="0.4609375"/>
<vertex x="1.65734375" y="0.46516875"/>
<vertex x="1.61289375" y="0.60961875"/>
<vertex x="1.549396875" y="0.74930625"/>
<vertex x="1.46574375" y="0.892715625"/>
<vertex x="1.464840625" y="0.893015625"/>
<vertex x="1.460490625" y="0.901721875"/>
<vertex x="1.45659375" y="0.908396875"/>
<vertex x="1.45401875" y="0.910246875"/>
<vertex x="1.4529375" y="0.916825"/>
<vertex x="1.449953125" y="0.922796875"/>
<vertex x="1.451296875" y="0.926825"/>
<vertex x="1.450609375" y="0.931009375"/>
<vertex x="1.454496875" y="0.936425"/>
<vertex x="1.456609375" y="0.942759375"/>
<vertex x="1.460409375" y="0.944659375"/>
<vertex x="1.8034" y="1.4224"/>
<vertex x="1.7018" y="1.5494"/>
<vertex x="1.574803125" y="1.676396875"/>
<vertex x="1.47320625" y="1.76529375"/>
<vertex x="1.3843" y="1.8288"/>
<vertex x="0.8813625" y="1.4730625"/>
<vertex x="0.879065625" y="1.468928125"/>
<vertex x="0.873109375" y="1.467225"/>
<vertex x="0.86805" y="1.463646875"/>
<vertex x="0.863384375" y="1.464446875"/>
<vertex x="0.858834375" y="1.463146875"/>
<vertex x="0.85341875" y="1.46615625"/>
<vertex x="0.847309375" y="1.467203125"/>
<vertex x="0.844575" y="1.47106875"/>
<vertex x="0.74930625" y="1.523996875"/>
<vertex x="0.644615625" y="1.570525"/>
<vertex x="0.6446125" y="1.570525"/>
<vertex x="0.520703125" y="1.6256"/>
<vertex x="0.427859375" y="1.64623125"/>
<vertex x="0.42258125" y="1.6452625"/>
<vertex x="0.417996875" y="1.648421875"/>
<vertex x="0.412559375" y="1.64963125"/>
<vertex x="0.409678125" y="1.654159375"/>
<vertex x="0.405259375" y="1.65720625"/>
<vertex x="0.404253125" y="1.662684375"/>
<vertex x="0.4012625" y="1.667384375"/>
<vertex x="0.402428125" y="1.672625"/>
<vertex x="0.2921" y="2.273303125"/>
<vertex x="0.1397125" y="2.286"/>
<vertex x="-0.1523875" y="2.286"/>
<vertex x="-0.2794" y="2.2733"/>
<vertex x="-0.377446875" y="1.685009375"/>
<vertex x="-0.3761125" y="1.6810125"/>
<vertex x="-0.3791125" y="1.675015625"/>
<vertex x="-0.3802125" y="1.668409375"/>
<vertex x="-0.383640625" y="1.665959375"/>
<vertex x="-0.385525" y="1.66219375"/>
<vertex x="-0.391878125" y="1.660075"/>
<vertex x="-0.397334375" y="1.656178125"/>
<vertex x="-0.401490625" y="1.656871875"/>
<vertex x="-0.57151875" y="1.600190625"/>
<vertex x="-0.698496875" y="1.5494"/>
<vertex x="-0.857309375" y="1.45865625"/>
<vertex x="-0.860278125" y="1.4545625"/>
<vertex x="-0.866096875" y="1.453634375"/>
<vertex x="-0.8712" y="1.45071875"/>
<vertex x="-0.876065625" y="1.452046875"/>
<vertex x="-0.88105625" y="1.45125"/>
<vertex x="-0.885821875" y="1.45470625"/>
<vertex x="-0.8915" y="1.456253125"/>
<vertex x="-0.894003125" y="1.4606375"/>
<vertex x="-1.384296875" y="1.8161"/>
<vertex x="-1.498603125" y="1.727196875"/>
<vertex x="-1.625590625" y="1.60020625"/>
<vertex x="-1.720309375" y="1.493653125"/>
<vertex x="-1.720309375" y="1.4927875"/>
<vertex x="-1.727234375" y="1.4858625"/>
<vertex x="-1.733715625" y="1.478571875"/>
<vertex x="-1.734575" y="1.478521875"/>
<vertex x="-1.7907" y="1.4224"/>
<vertex x="-1.447415625" y="0.919734375"/>
<vertex x="-1.4431625" y="0.917184375"/>
<vertex x="-1.4417125" y="0.9113875"/>
<vertex x="-1.43834375" y="0.906453125"/>
<vertex x="-1.4392625" y="0.90158125"/>
<vertex x="-1.438059375" y="0.89676875"/>
<vertex x="-1.441134375" y="0.89164375"/>
<vertex x="-1.442240625" y="0.885775"/>
<vertex x="-1.44633125" y="0.88298125"/>
<vertex x="-1.51130625" y="0.774690625"/>
<vertex x="-1.587503125" y="0.609596875"/>
<vertex x="-1.622896875" y="0.479809375"/>
<vertex x="-1.622153125" y="0.477946875"/>
<vertex x="-1.6256" y="0.469903125"/>
<vertex x="-1.62790625" y="0.461446875"/>
<vertex x="-1.62965" y="0.46045"/>
<vertex x="-1.65529375" y="0.4006125"/>
<vertex x="-1.654790625" y="0.39753125"/>
<vertex x="-1.6593" y="0.391259375"/>
<vertex x="-1.66235" y="0.384146875"/>
<vertex x="-1.665253125" y="0.382984375"/>
<vertex x="-1.667078125" y="0.38045"/>
<vertex x="-1.674703125" y="0.37920625"/>
<vertex x="-1.6818875" y="0.37633125"/>
<vertex x="-1.6847625" y="0.3775625"/>
<vertex x="-2.286" y="0.279403125"/>
<vertex x="-2.286" y="-0.279403125"/>
<vertex x="-1.685209375" y="-0.377490625"/>
<vertex x="-1.68055625" y="-0.376159375"/>
<vertex x="-1.675228125" y="-0.37911875"/>
<vertex x="-1.66921875" y="-0.3801"/>
<vertex x="-1.66639375" y="-0.384028125"/>
<vertex x="-1.6621625" y="-0.386378125"/>
<vertex x="-1.6604875" y="-0.392234375"/>
<vertex x="-1.65693125" y="-0.39718125"/>
<vertex x="-1.6577125" y="-0.401959375"/>
<vertex x="-1.612915625" y="-0.558753125"/>
<vertex x="-1.574796875" y="-0.685809375"/>
<vertex x="-1.5239875" y="-0.787425"/>
<vertex x="-1.44614375" y="-0.92086875"/>
<vertex x="-1.44195625" y="-0.92390625"/>
<vertex x="-1.441046875" y="-0.92960625"/>
<vertex x="-1.43814375" y="-0.934584375"/>
<vertex x="-1.43945625" y="-0.939578125"/>
<vertex x="-1.43864375" y="-0.944684375"/>
<vertex x="-1.44203125" y="-0.94935625"/>
<vertex x="-1.443496875" y="-0.95493125"/>
<vertex x="-1.447959375" y="-0.957534375"/>
<vertex x="-1.8034" y="-1.4478"/>
<vertex x="-1.701790625" y="-1.56210625"/>
<vertex x="-1.61290625" y="-1.650990625"/>
</polygon>
</package>
<package name="RFMHCW_SMT" library_version="242">
<wire x1="-8" y1="8" x2="8" y2="8" width="0.127" layer="51"/>
<wire x1="8" y1="8" x2="8" y2="-8" width="0.127" layer="51"/>
<wire x1="8" y1="-8" x2="-8" y2="-8" width="0.127" layer="51"/>
<wire x1="-8" y1="-8" x2="-8" y2="8" width="0.127" layer="51"/>
<smd name="1P" x="8.1" y="-7" dx="1.5" dy="1" layer="1" rot="R180"/>
<smd name="2P" x="8.1" y="-5" dx="1.5" dy="1" layer="1" rot="R180"/>
<smd name="3P" x="8.1" y="-3" dx="1.5" dy="1" layer="1" rot="R180"/>
<smd name="4P" x="8.1" y="-1" dx="1.5" dy="1" layer="1" rot="R180"/>
<smd name="5P" x="8.1" y="1" dx="1.5" dy="1" layer="1" rot="R180"/>
<smd name="6P" x="8.1" y="3" dx="1.5" dy="1" layer="1" rot="R180"/>
<smd name="7P" x="8.1" y="5" dx="1.5" dy="1" layer="1" rot="R180"/>
<wire x1="-8.1" y1="7.7" x2="-8.1" y2="8.1" width="0.127" layer="21"/>
<wire x1="-8.1" y1="8.1" x2="8.1" y2="8.1" width="0.127" layer="21"/>
<wire x1="8.1" y1="8.1" x2="8.1" y2="7.7" width="0.127" layer="21"/>
<wire x1="-8.1" y1="-7.7" x2="-8.1" y2="-8.1" width="0.127" layer="21"/>
<wire x1="-8.1" y1="-8.1" x2="8.1" y2="-8.1" width="0.127" layer="21"/>
<wire x1="8.1" y1="-8.1" x2="8.1" y2="-7.7" width="0.127" layer="21"/>
<smd name="8P" x="8.1" y="7" dx="1.5" dy="1" layer="1" rot="R180"/>
<smd name="9P" x="-8.1" y="7" dx="1.5" dy="1" layer="1"/>
<smd name="10P" x="-8.1" y="5" dx="1.5" dy="1" layer="1"/>
<smd name="11P" x="-8.1" y="3" dx="1.5" dy="1" layer="1"/>
<smd name="12P" x="-8.1" y="1" dx="1.5" dy="1" layer="1"/>
<smd name="13P" x="-8.1" y="-1" dx="1.5" dy="1" layer="1"/>
<smd name="14P" x="-8.1" y="-3" dx="1.5" dy="1" layer="1"/>
<smd name="15P" x="-8.1" y="-5" dx="1.5" dy="1" layer="1"/>
<smd name="16P" x="-8.1" y="-7" dx="1.5" dy="1" layer="1"/>
<circle x="10.1" y="-7" radius="0.3" width="0.6096" layer="21"/>
<rectangle x1="-8.9" y1="-8.2" x2="8.9" y2="8.2" layer="21"/>
</package>
<package name="WROOM32">
<smd name="1" x="-8.6" y="5" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="2" x="-8.6" y="3.73" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="3" x="-8.6" y="2.46" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="4" x="-8.6" y="1.19" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="5" x="-8.6" y="-0.08" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="6" x="-8.6" y="-1.35" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="7" x="-8.6" y="-2.62" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="8" x="-8.6" y="-3.89" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="9" x="-8.6" y="-5.16" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="10" x="-8.6" y="-6.43" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="11" x="-8.6" y="-7.7" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="12" x="-8.6" y="-8.97" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="13" x="-8.6" y="-10.24" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="14" x="-8.6" y="-11.51" dx="1.7" dy="0.9" layer="1" rot="R180"/>
<smd name="15" x="-5.7" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="16" x="-4.43" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="17" x="-3.16" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="18" x="-1.89" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="19" x="-0.62" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="20" x="0.65" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="21" x="1.92" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="22" x="3.19" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="23" x="4.46" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="24" x="5.73" y="-12.9" dx="1.7" dy="0.9" layer="1" rot="R270"/>
<smd name="25" x="8.6" y="-11.51" dx="1.7" dy="0.9" layer="1"/>
<smd name="26" x="8.6" y="-10.24" dx="1.7" dy="0.9" layer="1"/>
<smd name="27" x="8.6" y="-8.97" dx="1.7" dy="0.9" layer="1"/>
<smd name="28" x="8.6" y="-7.7" dx="1.7" dy="0.9" layer="1"/>
<smd name="29" x="8.6" y="-6.43" dx="1.7" dy="0.9" layer="1"/>
<smd name="30" x="8.6" y="-5.16" dx="1.7" dy="0.9" layer="1"/>
<smd name="31" x="8.6" y="-3.89" dx="1.7" dy="0.9" layer="1"/>
<smd name="32" x="8.6" y="-2.62" dx="1.7" dy="0.9" layer="1"/>
<smd name="33" x="8.6" y="-1.35" dx="1.7" dy="0.9" layer="1"/>
<smd name="34" x="8.6" y="-0.08" dx="1.7" dy="0.9" layer="1"/>
<smd name="35" x="8.6" y="1.19" dx="1.7" dy="0.9" layer="1"/>
<smd name="36" x="8.6" y="2.46" dx="1.7" dy="0.9" layer="1"/>
<smd name="37" x="8.6" y="3.73" dx="1.7" dy="0.9" layer="1"/>
<smd name="38" x="8.6" y="5" dx="1.7" dy="0.9" layer="1"/>
<smd name="P$1" x="0.34" y="-2.7" dx="6" dy="6" layer="1"/>
<wire x1="-9" y1="-13" x2="9" y2="-13" width="0.127" layer="51"/>
<wire x1="9" y1="-13" x2="9" y2="12.5" width="0.127" layer="51"/>
<wire x1="9" y1="12.5" x2="-9" y2="12.5" width="0.127" layer="51"/>
<wire x1="-9" y1="12.5" x2="-9" y2="-13" width="0.127" layer="51"/>
<rectangle x1="-9" y1="6.5" x2="9" y2="12.5" layer="41"/>
<rectangle x1="-9" y1="6.5" x2="9" y2="12.5" layer="42"/>
</package>
<package name="NRF24L01_MODULE_THROUGHHOLE">
<description>&lt;b&gt;2.4 GHz Wireless Module&lt;/b&gt; based on &lt;b&gt;NRF24L01&lt;/b&gt; chip</description>
<pad name="1" x="-12.7" y="6.35" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="8" x="-10.16" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="6" x="-10.16" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="4" x="-10.16" y="3.81" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="2" x="-10.16" y="6.35" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="3" x="-12.7" y="3.81" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="5" x="-12.7" y="1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<pad name="7" x="-12.7" y="-1.27" drill="0.9" diameter="1.6764" shape="octagon"/>
<wire x1="-14.3764" y1="8.001" x2="8.128" y2="8.001" width="0.127" layer="21"/>
<wire x1="8.128" y1="8.001" x2="14.6304" y2="8.001" width="0.127" layer="21"/>
<wire x1="14.6304" y1="8.001" x2="14.6304" y2="-6.985" width="0.127" layer="21"/>
<wire x1="14.6304" y1="-6.985" x2="8.128" y2="-6.985" width="0.127" layer="21"/>
<wire x1="8.128" y1="-6.985" x2="-14.3764" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-14.3764" y1="-6.985" x2="-14.3764" y2="8.001" width="0.127" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.335" y2="7.62" width="0.127" layer="21"/>
<wire x1="-13.335" y1="7.62" x2="-9.525" y2="7.62" width="0.127" layer="21"/>
<wire x1="-9.525" y1="7.62" x2="-8.89" y2="6.985" width="0.127" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="5.715" width="0.127" layer="21"/>
<wire x1="-8.89" y1="5.715" x2="-9.525" y2="5.08" width="0.127" layer="21"/>
<wire x1="-9.525" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="3.175" x2="-9.525" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.89" y2="1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.905" x2="-8.89" y2="0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-9.525" y2="0" width="0.127" layer="21"/>
<wire x1="-9.525" y1="0" x2="-8.89" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.89" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.905" x2="-9.525" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-10.795" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-11.43" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-1.905" x2="-12.065" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-13.335" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.97" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-1.905" x2="-13.97" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-0.635" x2="-13.335" y2="0" width="0.127" layer="21"/>
<wire x1="-13.335" y1="0" x2="-13.97" y2="0.635" width="0.127" layer="21"/>
<wire x1="-13.97" y1="0.635" x2="-13.97" y2="1.905" width="0.127" layer="21"/>
<wire x1="-13.97" y1="1.905" x2="-13.335" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="3.175" x2="-13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-13.335" y2="5.08" width="0.127" layer="21"/>
<wire x1="-13.335" y1="5.08" x2="-13.97" y2="5.715" width="0.127" layer="21"/>
<wire x1="-13.97" y1="5.715" x2="-13.97" y2="6.985" width="0.127" layer="21"/>
<wire x1="8.128" y1="8.001" x2="8.128" y2="-6.985" width="0.127" layer="21"/>
<text x="12.192" y="0.508" size="1.778" layer="21" rot="R90" align="bottom-center">ANTENNA</text>
<rectangle x1="8.1" y1="-7" x2="14.7" y2="8" layer="42"/>
<rectangle x1="8.1" y1="-7" x2="14.7" y2="8" layer="41"/>
<rectangle x1="-14.4" y1="-7" x2="14.7" y2="8" layer="21"/>
</package>
<package name="NRF24L01_MODULE_SMD" library_version="245">
<wire x1="-14.3764" y1="8.001" x2="8.128" y2="8.001" width="0.127" layer="21"/>
<wire x1="8.128" y1="8.001" x2="14.6304" y2="8.001" width="0.127" layer="21"/>
<wire x1="14.6304" y1="8.001" x2="14.6304" y2="-6.985" width="0.127" layer="21"/>
<wire x1="14.6304" y1="-6.985" x2="8.128" y2="-6.985" width="0.127" layer="21"/>
<wire x1="8.128" y1="-6.985" x2="-14.3764" y2="-6.985" width="0.127" layer="21"/>
<wire x1="-14.3764" y1="-6.985" x2="-14.3764" y2="8.001" width="0.127" layer="21"/>
<wire x1="-13.97" y1="6.985" x2="-13.335" y2="7.62" width="0.127" layer="21"/>
<wire x1="-13.335" y1="7.62" x2="-9.525" y2="7.62" width="0.127" layer="21"/>
<wire x1="-9.525" y1="7.62" x2="-8.89" y2="6.985" width="0.127" layer="21"/>
<wire x1="-8.89" y1="6.985" x2="-8.89" y2="5.715" width="0.127" layer="21"/>
<wire x1="-8.89" y1="5.715" x2="-9.525" y2="5.08" width="0.127" layer="21"/>
<wire x1="-9.525" y1="5.08" x2="-8.89" y2="4.445" width="0.127" layer="21"/>
<wire x1="-8.89" y1="4.445" x2="-8.89" y2="3.175" width="0.127" layer="21"/>
<wire x1="-8.89" y1="3.175" x2="-9.525" y2="2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="2.54" x2="-8.89" y2="1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="1.905" x2="-8.89" y2="0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="0.635" x2="-9.525" y2="0" width="0.127" layer="21"/>
<wire x1="-9.525" y1="0" x2="-8.89" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-0.635" x2="-8.89" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-8.89" y1="-1.905" x2="-9.525" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-9.525" y1="-2.54" x2="-10.795" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-10.795" y1="-2.54" x2="-11.43" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-11.43" y1="-1.905" x2="-12.065" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-12.065" y1="-2.54" x2="-13.335" y2="-2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="-2.54" x2="-13.97" y2="-1.905" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-1.905" x2="-13.97" y2="-0.635" width="0.127" layer="21"/>
<wire x1="-13.97" y1="-0.635" x2="-13.335" y2="0" width="0.127" layer="21"/>
<wire x1="-13.335" y1="0" x2="-13.97" y2="0.635" width="0.127" layer="21"/>
<wire x1="-13.97" y1="0.635" x2="-13.97" y2="1.905" width="0.127" layer="21"/>
<wire x1="-13.97" y1="1.905" x2="-13.335" y2="2.54" width="0.127" layer="21"/>
<wire x1="-13.335" y1="2.54" x2="-13.97" y2="3.175" width="0.127" layer="21"/>
<wire x1="-13.97" y1="3.175" x2="-13.97" y2="4.445" width="0.127" layer="21"/>
<wire x1="-13.97" y1="4.445" x2="-13.335" y2="5.08" width="0.127" layer="21"/>
<wire x1="-13.335" y1="5.08" x2="-13.97" y2="5.715" width="0.127" layer="21"/>
<wire x1="-13.97" y1="5.715" x2="-13.97" y2="6.985" width="0.127" layer="21"/>
<wire x1="8.128" y1="8.001" x2="8.128" y2="-6.985" width="0.127" layer="21"/>
<text x="12.192" y="0.508" size="1.778" layer="21" rot="R90" align="bottom-center">ANTENNA</text>
<rectangle x1="8.1" y1="-7" x2="14.7" y2="8" layer="42"/>
<rectangle x1="8.1" y1="-7" x2="14.7" y2="8" layer="41"/>
<rectangle x1="-14.4" y1="-7" x2="14.7" y2="8" layer="21"/>
<smd name="VCC" x="-10.16" y="6.35" dx="1.4224" dy="1.4224" layer="1"/>
<smd name="CSN" x="-10.16" y="3.81" dx="1.4224" dy="1.4224" layer="1"/>
<smd name="MOSI" x="-10.16" y="1.27" dx="1.4224" dy="1.4224" layer="1"/>
<smd name="IRQ" x="-10.16" y="-1.27" dx="1.4224" dy="1.4224" layer="1"/>
<smd name="MISO" x="-12.7" y="-1.27" dx="1.4224" dy="1.4224" layer="1"/>
<smd name="SCK" x="-12.7" y="1.27" dx="1.4224" dy="1.4224" layer="1"/>
<smd name="CE" x="-12.7" y="3.81" dx="1.4224" dy="1.4224" layer="1"/>
<smd name="GND" x="-12.7" y="6.35" dx="1.4224" dy="1.4224" layer="1"/>
</package>
<package name="ANTENNA" urn="urn:adsk.eagle:footprint:27905/1" library_version="3">
<description>&lt;b&gt;TEST PAD&lt;/b&gt;</description>
<circle x="0" y="0" radius="0.8128" width="0.1524" layer="51"/>
<pad name="TP" x="0" y="0" drill="1.7018" diameter="2.1208" shape="long" rot="R90"/>
<text x="-1.143" y="2.286" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-1.27" y="-3.81" size="1" layer="37">&gt;TP_SIGNAL_NAME</text>
<rectangle x1="-0.3302" y1="-0.3302" x2="0.3302" y2="0.3302" layer="51"/>
</package>
<package name="LED3535" library_version="290">
<smd name="3" x="-1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="2" x="1.75" y="0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="4" x="-1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<smd name="1" x="1.75" y="-0.875" dx="0.85" dy="1" layer="1" rot="R90"/>
<wire x1="-1.75" y1="1.75" x2="1.75" y2="1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="1.75" x2="1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="1.75" y1="-1.75" x2="-1.75" y2="-1.75" width="0.127" layer="51"/>
<wire x1="-1.75" y1="-1.75" x2="-1.75" y2="1.75" width="0.127" layer="51"/>
<circle x="0" y="0" radius="1.4" width="0.127" layer="51"/>
<wire x1="1.9" y1="-1.6" x2="1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="-1.9" x2="-1.9" y2="-1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="-1.9" x2="-1.9" y2="-1.6" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.6" x2="1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="1.9" y1="1.9" x2="-1.9" y2="1.9" width="0.127" layer="21"/>
<wire x1="-1.9" y1="1.9" x2="-1.9" y2="1.6" width="0.127" layer="21"/>
<polygon width="0.127" layer="21" pour="solid">
<vertex x="1.143" y="-1.905"/>
<vertex x="1.905" y="-1.905"/>
<vertex x="1.905" y="-1.524"/>
<vertex x="1.523996875" y="-1.524"/>
<vertex x="1.143" y="-1.904996875"/>
</polygon>
</package>
<package name="C1206" urn="urn:adsk.eagle:footprint:23125/1" locally_modified="yes" library_version="11">
<description>&lt;b&gt;CAPACITOR&lt;/b&gt;</description>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-0.965" y1="0.787" x2="0.965" y2="0.787" width="0.1016" layer="51"/>
<wire x1="-0.965" y1="-0.787" x2="0.965" y2="-0.787" width="0.1016" layer="51"/>
<smd name="1" x="-1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<smd name="2" x="1.4" y="0" dx="1.6" dy="1.8" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.7018" y1="-0.8509" x2="-0.9517" y2="0.8491" layer="51"/>
<rectangle x1="0.9517" y1="-0.8491" x2="1.7018" y2="0.8509" layer="51"/>
<rectangle x1="-0.1999" y1="-0.4001" x2="0.1999" y2="0.4001" layer="35"/>
<rectangle x1="-0.6" y1="-0.9" x2="0.6" y2="0.9" layer="21"/>
</package>
<package name="R1206" urn="urn:adsk.eagle:footprint:23047/1" locally_modified="yes" library_version="11">
<description>&lt;b&gt;RESISTOR&lt;/b&gt;</description>
<wire x1="0.9525" y1="-0.8128" x2="-0.9652" y2="-0.8128" width="0.1524" layer="51"/>
<wire x1="0.9525" y1="0.8128" x2="-0.9652" y2="0.8128" width="0.1524" layer="51"/>
<wire x1="-2.473" y1="0.983" x2="2.473" y2="0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="0.983" x2="2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="2.473" y1="-0.983" x2="-2.473" y2="-0.983" width="0.0508" layer="39"/>
<wire x1="-2.473" y1="-0.983" x2="-2.473" y2="0.983" width="0.0508" layer="39"/>
<smd name="2" x="1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<smd name="1" x="-1.422" y="0" dx="1.6" dy="1.803" layer="1"/>
<text x="-1.27" y="1.27" size="1.27" layer="25">&gt;NAME</text>
<text x="-1.27" y="-2.54" size="1.27" layer="27">&gt;VALUE</text>
<rectangle x1="-1.6891" y1="-0.8763" x2="-0.9525" y2="0.8763" layer="51"/>
<rectangle x1="0.9525" y1="-0.8763" x2="1.6891" y2="0.8763" layer="51"/>
<rectangle x1="-0.3" y1="-0.7" x2="0.3" y2="0.7" layer="35"/>
<rectangle x1="-0.6" y1="-0.9" x2="0.6" y2="0.9" layer="21"/>
</package>
</packages>
<packages3d>
<package3d name="EXPANSION-MALE-2X10" urn="urn:adsk.eagle:package:22405/2" locally_modified="yes" type="model" library_version="3">
<description>PIN HEADER</description>
<packageinstances>
<packageinstance name="EXPANSION-MALE-2X10"/>
</packageinstances>
</package3d>
<package3d name="C1206" urn="urn:adsk.eagle:package:23618/2" locally_modified="yes" type="model" library_version="11" footprint_synced="no">
<description>CAPACITOR</description>
<packageinstances>
<packageinstance name="C1206"/>
</packageinstances>
</package3d>
<package3d name="R1206" urn="urn:adsk.eagle:package:23540/2" locally_modified="yes" type="model" library_version="11" footprint_synced="no">
<description>RESISTOR</description>
<packageinstances>
<packageinstance name="R1206"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="MUTANTC">
<text x="0" y="2.54" size="3.81" layer="94" font="vector" ratio="20">mutantc</text>
<rectangle x1="0" y1="0" x2="20.32" y2="2.54" layer="94"/>
</symbol>
<symbol name="EXPANSION-2X10" library_version="245">
<wire x1="-6.35" y1="-15.24" x2="8.89" y2="-15.24" width="0.4064" layer="94"/>
<wire x1="8.89" y1="-15.24" x2="8.89" y2="12.7" width="0.4064" layer="94"/>
<wire x1="8.89" y1="12.7" x2="-6.35" y2="12.7" width="0.4064" layer="94"/>
<wire x1="-6.35" y1="12.7" x2="-6.35" y2="-15.24" width="0.4064" layer="94"/>
<text x="-6.35" y="13.335" size="1.778" layer="95">&gt;NAME</text>
<text x="-6.35" y="-17.78" size="1.778" layer="96">&gt;VALUE</text>
<pin name="1" x="-2.54" y="10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="2" x="5.08" y="10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="3" x="-2.54" y="7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="4" x="5.08" y="7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="5" x="-2.54" y="5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="6" x="5.08" y="5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="7" x="-2.54" y="2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="8" x="5.08" y="2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="9" x="-2.54" y="0" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="10" x="5.08" y="0" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="11" x="-2.54" y="-2.54" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="12" x="5.08" y="-2.54" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="13" x="-2.54" y="-5.08" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="14" x="5.08" y="-5.08" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="15" x="-2.54" y="-7.62" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="16" x="5.08" y="-7.62" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="17" x="-2.54" y="-10.16" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="18" x="5.08" y="-10.16" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
<pin name="19" x="-2.54" y="-12.7" visible="pad" length="short" direction="pas" function="dot"/>
<pin name="20" x="5.08" y="-12.7" visible="pad" length="short" direction="pas" function="dot" rot="R180"/>
</symbol>
<symbol name="OSHWLOGO" library_version="252">
<wire x1="0" y1="0" x2="0" y2="7.62" width="0.254" layer="94"/>
<wire x1="0" y1="7.62" x2="17.78" y2="7.62" width="0.254" layer="94"/>
<wire x1="17.78" y1="7.62" x2="17.78" y2="0" width="0.254" layer="94"/>
<wire x1="17.78" y1="0" x2="0" y2="0" width="0.254" layer="94"/>
<text x="2.54" y="2.54" size="1.778" layer="94">OSHW Logo</text>
</symbol>
<symbol name="RFM69HCW" library_version="242">
<pin name="MISO" x="-12.7" y="5.08" length="short" direction="out"/>
<pin name="DIO0" x="12.7" y="2.54" length="short" rot="R180"/>
<pin name="DIO2" x="12.7" y="7.62" length="short" rot="R180"/>
<pin name="DIO1" x="12.7" y="5.08" length="short" rot="R180"/>
<pin name="DIO5" x="-12.7" y="-7.62" length="short"/>
<pin name="RESET" x="-12.7" y="-5.08" length="short" direction="in"/>
<pin name="GND@3" x="-12.7" y="-10.16" length="short" direction="pwr"/>
<pin name="ANT" x="12.7" y="-10.16" length="short" rot="R180"/>
<pin name="GND@1" x="12.7" y="-7.62" length="short" direction="pwr" rot="R180"/>
<pin name="DIO3" x="12.7" y="-5.08" length="short" rot="R180"/>
<pin name="MOSI" x="-12.7" y="2.54" length="short" direction="in"/>
<pin name="SCK" x="-12.7" y="0" length="short" direction="in"/>
<pin name="NSS" x="-12.7" y="-2.54" length="short" direction="in"/>
<wire x1="-10.16" y1="10.16" x2="10.16" y2="10.16" width="0.254" layer="94" style="shortdash"/>
<wire x1="10.16" y1="10.16" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<wire x1="10.16" y1="-12.7" x2="-10.16" y2="-12.7" width="0.254" layer="94" style="shortdash"/>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="10.16" width="0.254" layer="94"/>
<text x="-10.16" y="17.78" size="1.27" layer="95">&gt;NAME</text>
<text x="-10.16" y="-20.32" size="1.27" layer="95">&gt;VALUE</text>
<wire x1="-10.16" y1="10.16" x2="-10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="-10.16" y1="15.24" x2="10.16" y2="15.24" width="0.254" layer="94"/>
<wire x1="10.16" y1="15.24" x2="10.16" y2="10.16" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-12.7" x2="-10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="-10.16" y1="-17.78" x2="10.16" y2="-17.78" width="0.254" layer="94"/>
<wire x1="10.16" y1="-17.78" x2="10.16" y2="-12.7" width="0.254" layer="94"/>
<text x="0" y="12.7" size="1.27" layer="94" align="center">RFM69HCW
ISM BAND RADIO</text>
<text x="0" y="-15.24" size="1.27" layer="94" align="center">VDD: 1.8-3.6V
Temp: -40~+85°C</text>
<pin name="DIO4" x="12.7" y="-2.54" length="short" rot="R180"/>
<pin name="VDD" x="12.7" y="0" length="short" direction="pwr" rot="R180"/>
<pin name="GND@2" x="-12.7" y="7.62" length="short" direction="pwr"/>
</symbol>
<symbol name="WROOM32" library_version="85">
<pin name="GND" x="0" y="-27.94" length="middle" direction="pas" rot="R90"/>
<pin name="3.3V" x="0" y="40.64" length="middle" direction="pas" rot="R270"/>
<pin name="EN" x="-22.86" y="17.78" length="middle" direction="in"/>
<pin name="IO36/SEN_VP/A1_0" x="-22.86" y="15.24" length="middle" direction="pas"/>
<pin name="IO39/SEN_VN/A1_3" x="-22.86" y="12.7" length="middle" direction="pas"/>
<pin name="IO0/A2_1" x="27.94" y="33.02" length="middle" rot="R180"/>
<pin name="IO2/A2_2" x="27.94" y="30.48" length="middle" rot="R180"/>
<pin name="IO4/A2_0" x="27.94" y="27.94" length="middle" rot="R180"/>
<pin name="IO5" x="27.94" y="25.4" length="middle" rot="R180"/>
<pin name="IO15/A2_3" x="27.94" y="15.24" length="middle" rot="R180"/>
<pin name="IO16" x="27.94" y="12.7" length="middle" rot="R180"/>
<pin name="IO17" x="27.94" y="10.16" length="middle" rot="R180"/>
<pin name="IO18" x="27.94" y="7.62" length="middle" rot="R180"/>
<pin name="IO19" x="27.94" y="5.08" length="middle" rot="R180"/>
<pin name="IO21" x="27.94" y="2.54" length="middle" rot="R180"/>
<pin name="IO23" x="27.94" y="-2.54" length="middle" rot="R180"/>
<pin name="IO13/A2_4" x="27.94" y="20.32" length="middle" rot="R180"/>
<pin name="IO26/DAC2/A2_9" x="27.94" y="-7.62" length="middle" rot="R180"/>
<pin name="IO25/DAC1/A2_8" x="27.94" y="-5.08" length="middle" rot="R180"/>
<pin name="IO27/A2_7" x="27.94" y="-10.16" length="middle" rot="R180"/>
<pin name="IO32/A1_4/X32P" x="27.94" y="-12.7" length="middle" rot="R180"/>
<pin name="IO33/A1_5/X32N" x="27.94" y="-15.24" length="middle" rot="R180"/>
<pin name="I34/A1_6" x="27.94" y="-17.78" length="middle" rot="R180"/>
<pin name="I35/A1_7" x="27.94" y="-20.32" length="middle" rot="R180"/>
<pin name="SD2" x="-22.86" y="2.54" length="middle"/>
<pin name="SD3" x="-22.86" y="0" length="middle"/>
<pin name="CMD" x="-22.86" y="-2.54" length="middle"/>
<pin name="CLK" x="-22.86" y="-5.08" length="middle"/>
<pin name="SD0" x="-22.86" y="-7.62" length="middle"/>
<pin name="SD1" x="-22.86" y="-10.16" length="middle"/>
<pin name="RXD0" x="-22.86" y="-15.24" length="middle" direction="in"/>
<pin name="TXD0" x="-22.86" y="-17.78" length="middle" direction="out"/>
<wire x1="-17.78" y1="35.56" x2="22.86" y2="35.56" width="0.254" layer="94"/>
<wire x1="22.86" y1="35.56" x2="22.86" y2="-22.86" width="0.254" layer="94"/>
<wire x1="22.86" y1="-22.86" x2="-17.78" y2="-22.86" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-22.86" x2="-17.78" y2="35.56" width="0.254" layer="94"/>
<pin name="IO14/A2_6" x="27.94" y="17.78" length="middle" rot="R180"/>
<pin name="IO12/A2_5" x="27.94" y="22.86" length="middle" rot="R180"/>
<pin name="IO22" x="27.94" y="0" length="middle" rot="R180"/>
</symbol>
<symbol name="NRF24L01_MODULE" library_version="245">
<description>&lt;b&gt;2.4 GHz Wireless Module&lt;/b&gt; based on &lt;b&gt;NRF24L01&lt;/b&gt; chip</description>
<pin name="GND" x="-22.86" y="10.16" length="middle" direction="pwr"/>
<pin name="VCC" x="-22.86" y="7.62" length="middle" direction="pwr"/>
<pin name="CE" x="-22.86" y="5.08" length="middle"/>
<pin name="CSN" x="-22.86" y="2.54" length="middle"/>
<pin name="SCK" x="-22.86" y="0" length="middle"/>
<pin name="MOSI" x="-22.86" y="-2.54" length="middle"/>
<pin name="MISO" x="-22.86" y="-5.08" length="middle"/>
<pin name="IRQ" x="-22.86" y="-7.62" length="middle"/>
<wire x1="-17.78" y1="12.7" x2="7.62" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="17.78" y1="12.7" x2="17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="17.78" y1="-10.16" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="-10.16" x2="-17.78" y2="-10.16" width="0.254" layer="94"/>
<wire x1="-17.78" y1="-10.16" x2="-17.78" y2="12.7" width="0.254" layer="94"/>
<wire x1="7.62" y1="12.7" x2="7.62" y2="-10.16" width="0.254" layer="94"/>
<text x="12.7" y="1.27" size="2.54" layer="94" rot="R90" align="center">ANTENNA</text>
<text x="20.32" y="10.16" size="1.778" layer="95">&gt;NAME</text>
<text x="20.32" y="7.62" size="1.778" layer="96">&gt;VALUE</text>
</symbol>
<symbol name="ANTENNA" library_version="308">
<wire x1="-0.762" y1="-0.762" x2="0" y2="0" width="0.254" layer="94"/>
<wire x1="0" y1="0" x2="0.762" y2="-0.762" width="0.254" layer="94"/>
<wire x1="0.762" y1="-0.762" x2="0" y2="-1.524" width="0.254" layer="94"/>
<wire x1="0" y1="-1.524" x2="-0.762" y2="-0.762" width="0.254" layer="94"/>
<text x="-1.27" y="1.27" size="1.778" layer="95">&gt;NAME</text>
<text x="1.27" y="-1.27" size="1.778" layer="97">&gt;TP_SIGNAL_NAME</text>
<pin name="TP" x="0" y="-2.54" visible="off" length="short" direction="in" rot="R90"/>
</symbol>
<symbol name="WS2812BLED" library_version="290">
<pin name="VDD" x="-12.7" y="5.08" visible="pin" length="middle" direction="pwr"/>
<pin name="DI" x="12.7" y="5.08" visible="pin" length="middle" direction="in" rot="R180"/>
<pin name="GND" x="12.7" y="-2.54" visible="pin" length="middle" direction="pwr" rot="R180"/>
<pin name="DO" x="-12.7" y="-2.54" visible="pin" length="middle" direction="out"/>
<wire x1="-7.62" y1="10.16" x2="-7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-7.62" y1="-5.08" x2="0" y2="-5.08" width="0.254" layer="94"/>
<wire x1="0" y1="-5.08" x2="7.62" y2="-5.08" width="0.254" layer="94"/>
<wire x1="7.62" y1="-5.08" x2="7.62" y2="10.16" width="0.254" layer="94"/>
<wire x1="7.62" y1="10.16" x2="-7.62" y2="10.16" width="0.254" layer="94"/>
<text x="-4.064" y="8.382" size="1.27" layer="94">WS2812B</text>
</symbol>
<symbol name="C-EU" library_version="310">
<wire x1="0" y1="0" x2="0" y2="-0.508" width="0.1524" layer="94"/>
<wire x1="0" y1="-2.54" x2="0" y2="-2.032" width="0.1524" layer="94"/>
<text x="1.524" y="0.381" size="1.778" layer="95">&gt;NAME</text>
<text x="1.524" y="-4.699" size="1.778" layer="96">&gt;VALUE</text>
<rectangle x1="-2.032" y1="-2.032" x2="2.032" y2="-1.524" layer="94"/>
<rectangle x1="-2.032" y1="-1.016" x2="2.032" y2="-0.508" layer="94"/>
<pin name="1" x="0" y="2.54" visible="off" length="short" direction="pas" swaplevel="1" rot="R270"/>
<pin name="2" x="0" y="-5.08" visible="off" length="short" direction="pas" swaplevel="1" rot="R90"/>
</symbol>
<symbol name="R-EU" library_version="310">
<wire x1="-2.54" y1="-0.889" x2="2.54" y2="-0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="2.54" y1="-0.889" x2="2.54" y2="0.889" width="0.254" layer="94"/>
<wire x1="-2.54" y1="-0.889" x2="-2.54" y2="0.889" width="0.254" layer="94"/>
<text x="-3.81" y="1.4986" size="1.778" layer="95">&gt;NAME</text>
<text x="-3.81" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="2" x="5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1" rot="R180"/>
<pin name="1" x="-5.08" y="0" visible="off" length="short" direction="pas" swaplevel="1"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="MUTANTC">
<gates>
<gate name="G$1" symbol="MUTANTC" x="22.86" y="2.54"/>
</gates>
<devices>
<device name="" package="MUTANTC_TEXT">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MUTANTC_LOGO" package="MUTANTC_LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="MUTANTC_TEXT+LOGO" package="MUTANTC_TEXT+LOGO">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="EXPANSION-MALE-2X10" prefix="JP" library_version="245">
<description>&lt;b&gt;PIN HEADER&lt;/b&gt;</description>
<gates>
<gate name="A" symbol="EXPANSION-2X10" x="0" y="0"/>
</gates>
<devices>
<device name="" package="EXPANSION-MALE-2X10">
<connects>
<connect gate="A" pin="1" pad="1"/>
<connect gate="A" pin="10" pad="10"/>
<connect gate="A" pin="11" pad="11"/>
<connect gate="A" pin="12" pad="12"/>
<connect gate="A" pin="13" pad="13"/>
<connect gate="A" pin="14" pad="14"/>
<connect gate="A" pin="15" pad="15"/>
<connect gate="A" pin="16" pad="16"/>
<connect gate="A" pin="17" pad="17"/>
<connect gate="A" pin="18" pad="18"/>
<connect gate="A" pin="19" pad="19"/>
<connect gate="A" pin="2" pad="2"/>
<connect gate="A" pin="20" pad="20"/>
<connect gate="A" pin="3" pad="3"/>
<connect gate="A" pin="4" pad="4"/>
<connect gate="A" pin="5" pad="5"/>
<connect gate="A" pin="6" pad="6"/>
<connect gate="A" pin="7" pad="7"/>
<connect gate="A" pin="8" pad="8"/>
<connect gate="A" pin="9" pad="9"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:22405/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="CATEGORY" value="CONNECTOR" constant="no"/>
<attribute name="DESCRIPTION" value="" constant="no"/>
<attribute name="MANUFACTURER" value="" constant="no"/>
<attribute name="MPN" value="" constant="no"/>
<attribute name="OPERATING_TEMP" value="" constant="no"/>
<attribute name="PART_STATUS" value="" constant="no"/>
<attribute name="ROHS_COMPLIANT" value="" constant="no"/>
<attribute name="SERIES" value="" constant="no"/>
<attribute name="SUB-CATEGORY" value="PIN-HEADER" constant="no"/>
<attribute name="THERMALLOSS" value="" constant="no"/>
<attribute name="TYPE" value="" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="OSHWLOGO" prefix="GOLD_ORB_SM" library_version="252">
<description>&lt;p&gt;The OSHW logo for PCB layout</description>
<gates>
<gate name="G$1" symbol="OSHWLOGO" x="0" y="0"/>
</gates>
<devices>
<device name="LOGO16MM" package="OSHW_16MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO8MM" package="OSHW_8MM">
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="LOGO5MM" package="OSHW_5MM">
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="RFM69HCW" prefix="U" uservalue="yes">
<description>&lt;p&gt;&lt;b&gt;RFM69HCW - ISM Band RF Transceiver&lt;/b&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="RFM69HCW" x="0" y="0"/>
</gates>
<devices>
<device name="_SMT" package="RFMHCW_SMT">
<connects>
<connect gate="G$1" pin="ANT" pad="1P"/>
<connect gate="G$1" pin="DIO0" pad="6P"/>
<connect gate="G$1" pin="DIO1" pad="7P"/>
<connect gate="G$1" pin="DIO2" pad="8P"/>
<connect gate="G$1" pin="DIO3" pad="3P"/>
<connect gate="G$1" pin="DIO4" pad="4P"/>
<connect gate="G$1" pin="DIO5" pad="15P"/>
<connect gate="G$1" pin="GND@1" pad="2P"/>
<connect gate="G$1" pin="GND@2" pad="9P"/>
<connect gate="G$1" pin="GND@3" pad="16P"/>
<connect gate="G$1" pin="MISO" pad="10P"/>
<connect gate="G$1" pin="MOSI" pad="11P"/>
<connect gate="G$1" pin="NSS" pad="13P"/>
<connect gate="G$1" pin="RESET" pad="14P"/>
<connect gate="G$1" pin="SCK" pad="12P"/>
<connect gate="G$1" pin="VDD" pad="5P"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ESP32_WROOM32" prefix="X">
<gates>
<gate name="G$1" symbol="WROOM32" x="-5.08" y="-2.54"/>
</gates>
<devices>
<device name="" package="WROOM32">
<connects>
<connect gate="G$1" pin="3.3V" pad="2"/>
<connect gate="G$1" pin="CLK" pad="20"/>
<connect gate="G$1" pin="CMD" pad="19"/>
<connect gate="G$1" pin="EN" pad="3"/>
<connect gate="G$1" pin="GND" pad="1 15 38 P$1"/>
<connect gate="G$1" pin="I34/A1_6" pad="6"/>
<connect gate="G$1" pin="I35/A1_7" pad="7"/>
<connect gate="G$1" pin="IO0/A2_1" pad="25"/>
<connect gate="G$1" pin="IO12/A2_5" pad="14"/>
<connect gate="G$1" pin="IO13/A2_4" pad="16"/>
<connect gate="G$1" pin="IO14/A2_6" pad="13"/>
<connect gate="G$1" pin="IO15/A2_3" pad="23"/>
<connect gate="G$1" pin="IO16" pad="27"/>
<connect gate="G$1" pin="IO17" pad="28"/>
<connect gate="G$1" pin="IO18" pad="30"/>
<connect gate="G$1" pin="IO19" pad="31"/>
<connect gate="G$1" pin="IO2/A2_2" pad="24"/>
<connect gate="G$1" pin="IO21" pad="33"/>
<connect gate="G$1" pin="IO22" pad="36"/>
<connect gate="G$1" pin="IO23" pad="37"/>
<connect gate="G$1" pin="IO25/DAC1/A2_8" pad="10"/>
<connect gate="G$1" pin="IO26/DAC2/A2_9" pad="11"/>
<connect gate="G$1" pin="IO27/A2_7" pad="12"/>
<connect gate="G$1" pin="IO32/A1_4/X32P" pad="8"/>
<connect gate="G$1" pin="IO33/A1_5/X32N" pad="9"/>
<connect gate="G$1" pin="IO36/SEN_VP/A1_0" pad="4"/>
<connect gate="G$1" pin="IO39/SEN_VN/A1_3" pad="5"/>
<connect gate="G$1" pin="IO4/A2_0" pad="26"/>
<connect gate="G$1" pin="IO5" pad="29"/>
<connect gate="G$1" pin="RXD0" pad="34"/>
<connect gate="G$1" pin="SD0" pad="21"/>
<connect gate="G$1" pin="SD1" pad="22"/>
<connect gate="G$1" pin="SD2" pad="17"/>
<connect gate="G$1" pin="SD3" pad="18"/>
<connect gate="G$1" pin="TXD0" pad="35"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="NRF24L01_MODULE" prefix="M">
<description>&lt;b&gt;2.4 GHz Wireless Module&lt;/b&gt; based on &lt;b&gt;NRF24L01&lt;/b&gt; chip
&lt;p&gt;More details available here:&lt;br&gt;
&lt;a href="http://www.electrodragon.com/w/NRF24L01"&gt;http://www.electrodragon.com/w/NRF24L01&lt;/a&gt;&lt;/p&gt;
&lt;p&gt;&lt;b&gt;&lt;a href="http://www.ebay.com/sch/nrf24l01+arduino"&gt;Click here to find device on ebay.com&lt;/a&gt;&lt;/b&gt;&lt;/p&gt;

&lt;p&gt;&lt;img alt="photo" src="http://www.diymodules.org/img/device-photo.php?name=WIRELESS-NRF24L01"&gt;&lt;/p&gt;</description>
<gates>
<gate name="G$1" symbol="NRF24L01_MODULE" x="0" y="0"/>
</gates>
<devices>
<device name="" package="NRF24L01_MODULE_THROUGHHOLE">
<connects>
<connect gate="G$1" pin="CE" pad="3"/>
<connect gate="G$1" pin="CSN" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="IRQ" pad="8"/>
<connect gate="G$1" pin="MISO" pad="7"/>
<connect gate="G$1" pin="MOSI" pad="6"/>
<connect gate="G$1" pin="SCK" pad="5"/>
<connect gate="G$1" pin="VCC" pad="2"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
<device name="WIRELESS-NRF24L01_SMD" package="NRF24L01_MODULE_SMD">
<connects>
<connect gate="G$1" pin="CE" pad="CE"/>
<connect gate="G$1" pin="CSN" pad="CSN"/>
<connect gate="G$1" pin="GND" pad="GND"/>
<connect gate="G$1" pin="IRQ" pad="IRQ"/>
<connect gate="G$1" pin="MISO" pad="MISO"/>
<connect gate="G$1" pin="MOSI" pad="MOSI"/>
<connect gate="G$1" pin="SCK" pad="SCK"/>
<connect gate="G$1" pin="VCC" pad="VCC"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="ANTENNA" library_version="308">
<gates>
<gate name="G$1" symbol="ANTENNA" x="-10.16" y="5.08"/>
</gates>
<devices>
<device name="" package="ANTENNA">
<connects>
<connect gate="G$1" pin="TP" pad="TP"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="WS2812B" prefix="LED" library_version="290">
<gates>
<gate name="G$1" symbol="WS2812BLED" x="0" y="-2.54"/>
</gates>
<devices>
<device name="3535" package="LED3535">
<connects>
<connect gate="G$1" pin="DI" pad="2"/>
<connect gate="G$1" pin="DO" pad="4"/>
<connect gate="G$1" pin="GND" pad="1"/>
<connect gate="G$1" pin="VDD" pad="3"/>
</connects>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
<deviceset name="CAPASITOR-1206" prefix="C" uservalue="yes" library_version="310">
<description>&lt;B&gt;CAPACITOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="C-EU" x="0" y="0"/>
</gates>
<devices>
<device name="C1206" package="C1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23618/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="54" constant="no"/>
<attribute name="SPICEPREFIX" value="C" constant="no"/>
</technology>
</technologies>
</device>
</devices>
<spice>
<pinmapping spiceprefix="C">
<pinmap gate="G$1" pin="1" pinorder="1"/>
<pinmap gate="G$1" pin="2" pinorder="2"/>
</pinmapping>
</spice>
</deviceset>
<deviceset name="RESISTOR-1206" prefix="R" uservalue="yes" library_version="310">
<description>&lt;B&gt;RESISTOR&lt;/B&gt;, European symbol</description>
<gates>
<gate name="G$1" symbol="R-EU" x="0" y="0"/>
</gates>
<devices>
<device name="R1206" package="R1206">
<connects>
<connect gate="G$1" pin="1" pad="1"/>
<connect gate="G$1" pin="2" pad="2"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:23540/2"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="41" constant="no"/>
<attribute name="SPICEPREFIX" value="R" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="wirepad" urn="urn:adsk.eagle:library:412">
<description>&lt;b&gt;Single Pads&lt;/b&gt;&lt;p&gt;
&lt;author&gt;Created by librarian@cadsoft.de&lt;/author&gt;</description>
<packages>
<package name="SMD1,27-2,54" urn="urn:adsk.eagle:footprint:30822/1" library_version="2">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<smd name="1" x="0" y="0" dx="1.27" dy="2.54" layer="1"/>
<text x="0" y="0" size="0.0254" layer="27">&gt;VALUE</text>
<text x="-0.8" y="-2.4" size="1.27" layer="25" rot="R90">&gt;NAME</text>
</package>
</packages>
<packages3d>
<package3d name="SMD1,27-2,54" urn="urn:adsk.eagle:package:30839/1" type="box" library_version="2">
<description>SMD PAD</description>
<packageinstances>
<packageinstance name="SMD1,27-2,54"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PAD" urn="urn:adsk.eagle:symbol:30808/1" library_version="2">
<wire x1="-1.016" y1="1.016" x2="1.016" y2="-1.016" width="0.254" layer="94"/>
<wire x1="-1.016" y1="-1.016" x2="1.016" y2="1.016" width="0.254" layer="94"/>
<text x="-1.143" y="1.8542" size="1.778" layer="95">&gt;NAME</text>
<text x="-1.143" y="-3.302" size="1.778" layer="96">&gt;VALUE</text>
<pin name="P" x="2.54" y="0" visible="off" length="short" direction="pas" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="SMD2" urn="urn:adsk.eagle:component:30857/2" prefix="PAD" uservalue="yes" library_version="2">
<description>&lt;b&gt;SMD PAD&lt;/b&gt;</description>
<gates>
<gate name="1" symbol="PAD" x="0" y="0"/>
</gates>
<devices>
<device name="" package="SMD1,27-2,54">
<connects>
<connect gate="1" pin="P" pad="1"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:30839/1"/>
</package3dinstances>
<technologies>
<technology name="">
<attribute name="POPULARITY" value="15" constant="no"/>
</technology>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
<library name="adafruit" urn="urn:adsk.eagle:library:420">
<packages>
<package name="PNA460XM" urn="urn:adsk.eagle:footprint:6240011/1" library_version="2">
<wire x1="-3.81" y1="-1.27" x2="-2.54" y2="-1.27" width="0.127" layer="21"/>
<wire x1="-2.54" y1="-1.27" x2="2.54" y2="-1.27" width="0.4064" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="-1.27" x2="3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="3.81" y1="1.27" x2="-3.81" y2="1.27" width="0.127" layer="21"/>
<wire x1="-3.81" y1="1.27" x2="-3.81" y2="-1.27" width="0.127" layer="21"/>
<wire x1="2.54" y1="-1.27" x2="-2.54" y2="-1.27" width="0.4064" layer="21" curve="-112.624029"/>
<pad name="1" x="-2.54" y="0" drill="1.016" diameter="1.9304" shape="square" rot="R90"/>
<pad name="2" x="0" y="0" drill="1.016" diameter="1.9304" rot="R90"/>
<pad name="3" x="2.54" y="0" drill="1.016" diameter="1.9304" rot="R90"/>
<text x="-3.8862" y="1.8288" size="1.27" layer="25" ratio="10">&gt;NAME</text>
<rectangle x1="2.286" y1="-0.254" x2="2.794" y2="0.254" layer="51"/>
<rectangle x1="-0.254" y1="-0.254" x2="0.254" y2="0.254" layer="51"/>
<rectangle x1="-2.794" y1="-0.254" x2="-2.286" y2="0.254" layer="51"/>
</package>
</packages>
<packages3d>
<package3d name="PNA460XM" urn="urn:adsk.eagle:package:6240657/1" type="box" library_version="2">
<packageinstances>
<packageinstance name="PNA460XM"/>
</packageinstances>
</package3d>
</packages3d>
<symbols>
<symbol name="PNA460XM" urn="urn:adsk.eagle:symbol:6239507/1" library_version="2">
<wire x1="-5.08" y1="7.62" x2="5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="7.62" x2="5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="5.08" y1="-7.62" x2="-5.08" y2="-7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-7.62" x2="-5.08" y2="-5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94"/>
<wire x1="-5.08" y1="5.08" x2="-5.08" y2="7.62" width="0.254" layer="94"/>
<wire x1="-5.08" y1="-5.08" x2="-5.08" y2="5.08" width="0.254" layer="94" curve="-180"/>
<text x="-5.08" y="7.62" size="1.778" layer="95">&gt;NAME</text>
<text x="-5.08" y="-10.16" size="1.778" layer="95">&gt;VALUE</text>
<pin name="OUT" x="10.16" y="5.08" length="middle" direction="out" rot="R180"/>
<pin name="GND" x="10.16" y="0" length="middle" direction="pwr" rot="R180"/>
<pin name="VCC" x="10.16" y="-5.08" length="middle" direction="pwr" rot="R180"/>
</symbol>
</symbols>
<devicesets>
<deviceset name="PNA460XM" urn="urn:adsk.eagle:component:6240998/1" library_version="2">
<description>&lt;b&gt;IR 38KHz receiver&lt;/b&gt;
&lt;p&gt;
PNA4602 and related receivers
&lt;p&gt;http://www.ladyada.net/library/pcb/eaglelibrary.html&lt;p&gt;</description>
<gates>
<gate name="G$1" symbol="PNA460XM" x="0" y="0"/>
</gates>
<devices>
<device name="" package="PNA460XM">
<connects>
<connect gate="G$1" pin="GND" pad="2"/>
<connect gate="G$1" pin="OUT" pad="1"/>
<connect gate="G$1" pin="VCC" pad="3"/>
</connects>
<package3dinstances>
<package3dinstance package3d_urn="urn:adsk.eagle:package:6240657/1"/>
</package3dinstances>
<technologies>
<technology name=""/>
</technologies>
</device>
</devices>
</deviceset>
</devicesets>
</library>
</libraries>
<attributes>
</attributes>
<variantdefs>
</variantdefs>
<classes>
<class number="0" name="default" width="0" drill="0">
</class>
<class number="2" name="3.3V" width="0.508" drill="0">
</class>
</classes>
<parts>
<part name="JP1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="EXPANSION-MALE-2X10" device="" package3d_urn="urn:adsk.eagle:package:22405/2"/>
<part name="U$4" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="MUTANTC" device="MUTANTC_TEXT+LOGO" value="MUTANTCMUTANTC_TEXT+LOGO"/>
<part name="PAD1" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="PAD2" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="GOLD_ORB_SM1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="OSHWLOGO" device="LOGO5MM"/>
<part name="U$2" library="adafruit" library_urn="urn:adsk.eagle:library:420" deviceset="PNA460XM" device="" package3d_urn="urn:adsk.eagle:package:6240657/1"/>
<part name="U1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RFM69HCW" device="_SMT"/>
<part name="10K1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR-1206" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2" value="1k"/>
<part name="10K2" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR-1206" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2" value="1k"/>
<part name="TP2" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="ANTENNA" device=""/>
<part name="C2_10UF2" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="CAPASITOR-1206" device="C1206" package3d_urn="urn:adsk.eagle:package:23618/2"/>
<part name="X1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="ESP32_WROOM32" device=""/>
<part name="M1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="NRF24L01_MODULE" device="" value="NRF24L01_MODULE"/>
<part name="R5_10K5" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="RESISTOR-1206" device="R1206" package3d_urn="urn:adsk.eagle:package:23540/2"/>
<part name="PAD3" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="PAD7" library="wirepad" library_urn="urn:adsk.eagle:library:412" deviceset="SMD2" device="" package3d_urn="urn:adsk.eagle:package:30839/1"/>
<part name="LED1" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="WS2812B" device="3535"/>
<part name="LED2" library="ch" library_urn="urn:adsk.wipprod:fs.file:vf.ByELmly1SGCTmLqjK3Rd6A" deviceset="WS2812B" device="3535"/>
</parts>
<sheets>
<sheet>
<plain>
</plain>
<instances>
<instance part="JP1" gate="A" x="25.4" y="63.5" smashed="yes">
<attribute name="VALUE" x="19.05" y="45.72" size="1.778" layer="96"/>
</instance>
<instance part="U$4" gate="G$1" x="88.9" y="63.5" smashed="yes"/>
<instance part="PAD1" gate="1" x="-15.24" y="60.96" smashed="yes" rot="R180">
<attribute name="NAME" x="-14.097" y="59.1058" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-14.097" y="64.262" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PAD2" gate="1" x="-15.24" y="71.12" smashed="yes" rot="R180">
<attribute name="NAME" x="-14.097" y="69.2658" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="-14.097" y="74.422" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="GOLD_ORB_SM1" gate="G$1" x="121.92" y="63.5" smashed="yes"/>
<instance part="U$2" gate="G$1" x="-2.54" y="-81.28" smashed="yes">
<attribute name="NAME" x="-7.62" y="-73.66" size="1.778" layer="95"/>
<attribute name="VALUE" x="-7.62" y="-91.44" size="1.778" layer="95"/>
</instance>
<instance part="U1" gate="G$1" x="137.16" y="20.32" smashed="yes">
<attribute name="NAME" x="127" y="38.1" size="1.27" layer="95"/>
<attribute name="VALUE" x="127" y="0" size="1.27" layer="95"/>
</instance>
<instance part="10K1" gate="G$1" x="203.2" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="201.676" y="17.78" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
</instance>
<instance part="10K2" gate="G$1" x="195.58" y="17.78" smashed="yes" rot="R90">
<attribute name="NAME" x="194.056" y="17.78" size="1.778" layer="95" font="vector" rot="R90" align="bottom-center"/>
</instance>
<instance part="TP2" gate="G$1" x="162.56" y="10.16" smashed="yes" rot="R270">
<attribute name="NAME" x="163.83" y="11.43" size="1.778" layer="95" rot="R270"/>
</instance>
<instance part="C2_10UF2" gate="G$1" x="167.64" y="35.56" smashed="yes">
<attribute name="NAME" x="164.719" y="34.544" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="169.164" y="30.861" size="1.778" layer="96"/>
</instance>
<instance part="X1" gate="G$1" x="127" y="-60.96" smashed="yes"/>
<instance part="M1" gate="G$1" x="27.94" y="10.16" smashed="yes">
<attribute name="NAME" x="20.32" y="27.94" size="1.778" layer="95"/>
<attribute name="VALUE" x="20.32" y="25.4" size="1.778" layer="96"/>
</instance>
<instance part="R5_10K5" gate="G$1" x="215.9" y="-40.64" smashed="yes" rot="R90">
<attribute name="NAME" x="214.4014" y="-44.45" size="1.778" layer="95" rot="R90"/>
<attribute name="VALUE" x="219.202" y="-44.45" size="1.778" layer="96" rot="R90"/>
</instance>
<instance part="PAD3" gate="1" x="223.52" y="-78.74" smashed="yes" rot="R180">
<attribute name="NAME" x="224.663" y="-80.5942" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="224.663" y="-75.438" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="PAD7" gate="1" x="223.52" y="-86.36" smashed="yes" rot="R180">
<attribute name="NAME" x="224.663" y="-88.2142" size="1.778" layer="95" rot="R180"/>
<attribute name="VALUE" x="224.663" y="-83.058" size="1.778" layer="96" rot="R180"/>
</instance>
<instance part="LED1" gate="G$1" x="10.16" y="-25.4" smashed="yes"/>
<instance part="LED2" gate="G$1" x="10.16" y="-53.34" smashed="yes"/>
</instances>
<busses>
</busses>
<nets>
<net name="GND" class="0">
<segment>
<label x="-38.1" y="60.96" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-17.78" y1="60.96" x2="-38.1" y2="60.96" width="0.1524" layer="91"/>
<pinref part="PAD1" gate="1" pin="P"/>
</segment>
<segment>
<wire x1="22.86" y1="58.42" x2="15.24" y2="58.42" width="0.1524" layer="91"/>
<label x="15.24" y="58.42" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="13"/>
</segment>
<segment>
<label x="40.64" y="50.8" size="1.778" layer="95" rot="MR180" xref="yes"/>
<wire x1="30.48" y1="50.8" x2="40.64" y2="50.8" width="0.1524" layer="91"/>
<pinref part="JP1" gate="A" pin="20"/>
</segment>
<segment>
<label x="15.24" y="-81.28" size="1.778" layer="95" xref="yes"/>
<wire x1="7.62" y1="-81.28" x2="15.24" y2="-81.28" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="GND"/>
</segment>
<segment>
<label x="165.1" y="12.7" size="1.778" layer="95" xref="yes"/>
<wire x1="149.86" y1="12.7" x2="165.1" y2="12.7" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@1"/>
</segment>
<segment>
<label x="104.14" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="124.46" y1="10.16" x2="104.14" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@3"/>
</segment>
<segment>
<label x="119.38" y="27.94" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="124.46" y1="27.94" x2="119.38" y2="27.94" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="GND@2"/>
</segment>
<segment>
<label x="167.64" y="43.18" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="167.64" y1="38.1" x2="167.64" y2="43.18" width="0.1524" layer="91"/>
<pinref part="C2_10UF2" gate="G$1" pin="1"/>
</segment>
<segment>
<label x="127" y="-96.52" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="127" y1="-88.9" x2="127" y2="-96.52" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="-20.32" y1="20.32" x2="5.08" y2="20.32" width="0.1524" layer="91"/>
<label x="-20.32" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="M1" gate="G$1" pin="GND"/>
</segment>
<segment>
<label x="30.48" y="-27.94" size="1.778" layer="95" xref="yes"/>
<wire x1="22.86" y1="-27.94" x2="30.48" y2="-27.94" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="GND"/>
</segment>
<segment>
<wire x1="22.86" y1="-55.88" x2="30.48" y2="-55.88" width="0.1524" layer="91"/>
<label x="30.48" y="-55.88" size="1.778" layer="95" xref="yes"/>
<pinref part="LED2" gate="G$1" pin="GND"/>
</segment>
</net>
<net name="PRX" class="0">
<segment>
<wire x1="40.64" y1="60.96" x2="30.48" y2="60.96" width="0.1524" layer="91"/>
<label x="40.64" y="60.96" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="12"/>
</segment>
<segment>
<wire x1="99.06" y1="-78.74" x2="104.14" y2="-78.74" width="0.1524" layer="91"/>
<label x="99.06" y="-78.74" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="X1" gate="G$1" pin="TXD0"/>
</segment>
</net>
<net name="GP22" class="0">
<segment>
<wire x1="22.86" y1="63.5" x2="15.24" y2="63.5" width="0.1524" layer="91"/>
<label x="15.24" y="63.5" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="9"/>
</segment>
<segment>
<wire x1="154.94" y1="-66.04" x2="162.56" y2="-66.04" width="0.1524" layer="91"/>
<label x="162.56" y="-66.04" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO25/DAC1/A2_8"/>
</segment>
</net>
<net name="PTX" class="0">
<segment>
<wire x1="53.34" y1="58.42" x2="30.48" y2="58.42" width="0.1524" layer="91"/>
<label x="53.34" y="58.42" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="14"/>
</segment>
<segment>
<wire x1="91.44" y1="-76.2" x2="104.14" y2="-76.2" width="0.1524" layer="91"/>
<label x="91.44" y="-76.2" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="X1" gate="G$1" pin="RXD0"/>
</segment>
</net>
<net name="P_SCL" class="0">
<segment>
<wire x1="40.64" y1="55.88" x2="30.48" y2="55.88" width="0.1524" layer="91"/>
<label x="40.64" y="55.88" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="16"/>
</segment>
<segment>
<wire x1="162.56" y1="-78.74" x2="154.94" y2="-78.74" width="0.1524" layer="91"/>
<label x="162.56" y="-78.74" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="I34/A1_6"/>
</segment>
</net>
<net name="P_SDA" class="0">
<segment>
<wire x1="15.24" y1="53.34" x2="22.86" y2="53.34" width="0.1524" layer="91"/>
<label x="15.24" y="53.34" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="17"/>
</segment>
<segment>
<wire x1="177.8" y1="-81.28" x2="154.94" y2="-81.28" width="0.1524" layer="91"/>
<label x="177.8" y="-81.28" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="I35/A1_7"/>
</segment>
</net>
<net name="5V" class="0">
<segment>
<wire x1="-35.56" y1="71.12" x2="-17.78" y2="71.12" width="0.1524" layer="91"/>
<label x="-35.56" y="71.12" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="PAD2" gate="1" pin="P"/>
</segment>
<segment>
<wire x1="2.54" y1="50.8" x2="22.86" y2="50.8" width="0.1524" layer="91"/>
<label x="2.54" y="50.8" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="19"/>
</segment>
<segment>
<wire x1="2.54" y1="66.04" x2="22.86" y2="66.04" width="0.1524" layer="91"/>
<label x="2.54" y="66.04" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="7"/>
</segment>
</net>
<net name="3V" class="0">
<segment>
<wire x1="53.34" y1="53.34" x2="30.48" y2="53.34" width="0.1524" layer="91"/>
<label x="53.34" y="53.34" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="18"/>
</segment>
<segment>
<label x="15.24" y="-86.36" size="1.778" layer="95" xref="yes"/>
<wire x1="7.62" y1="-86.36" x2="15.24" y2="-86.36" width="0.1524" layer="91"/>
<pinref part="U$2" gate="G$1" pin="VCC"/>
</segment>
<segment>
<wire x1="203.2" y1="12.7" x2="203.2" y2="2.54" width="0.1524" layer="91"/>
<label x="203.2" y="2.54" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="10K1" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="195.58" y1="12.7" x2="195.58" y2="2.54" width="0.1524" layer="91"/>
<label x="195.58" y="2.54" size="1.778" layer="95" rot="R270" xref="yes"/>
<pinref part="10K2" gate="G$1" pin="1"/>
</segment>
<segment>
<wire x1="149.86" y1="20.32" x2="167.64" y2="20.32" width="0.1524" layer="91"/>
<label x="172.72" y="20.32" size="1.778" layer="95" xref="yes"/>
<wire x1="167.64" y1="20.32" x2="172.72" y2="20.32" width="0.1524" layer="91"/>
<wire x1="167.64" y1="30.48" x2="167.64" y2="20.32" width="0.1524" layer="91"/>
<junction x="167.64" y="20.32"/>
<pinref part="U1" gate="G$1" pin="VDD"/>
<pinref part="C2_10UF2" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="-2.54" y1="17.78" x2="5.08" y2="17.78" width="0.1524" layer="91"/>
<label x="-2.54" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="M1" gate="G$1" pin="VCC"/>
</segment>
<segment>
<label x="215.9" y="-33.02" size="1.778" layer="95" rot="R90" xref="yes"/>
<wire x1="215.9" y1="-35.56" x2="215.9" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="R5_10K5" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="127" y1="-15.24" x2="127" y2="-20.32" width="0.1524" layer="91"/>
<label x="127" y="-15.24" size="1.778" layer="95" rot="MR90" xref="yes"/>
<pinref part="X1" gate="G$1" pin="3.3V"/>
</segment>
<segment>
<wire x1="-7.62" y1="-48.26" x2="-2.54" y2="-48.26" width="0.1524" layer="91"/>
<label x="-7.62" y="-48.26" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="LED2" gate="G$1" pin="VDD"/>
</segment>
</net>
<net name="GP16" class="0">
<segment>
<wire x1="2.54" y1="71.12" x2="22.86" y2="71.12" width="0.1524" layer="91"/>
<label x="2.54" y="71.12" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="3"/>
</segment>
<segment>
<wire x1="170.18" y1="27.94" x2="149.86" y2="27.94" width="0.1524" layer="91"/>
<label x="170.18" y="27.94" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="DIO2"/>
</segment>
</net>
<net name="GP27" class="0">
<segment>
<wire x1="2.54" y1="60.96" x2="22.86" y2="60.96" width="0.1524" layer="91"/>
<label x="2.54" y="60.96" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="11"/>
</segment>
<segment>
<wire x1="162.56" y1="-73.66" x2="154.94" y2="-73.66" width="0.1524" layer="91"/>
<label x="162.56" y="-73.66" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO32/A1_4/X32P"/>
</segment>
</net>
<net name="GP13" class="0">
<segment>
<wire x1="22.86" y1="73.66" x2="15.24" y2="73.66" width="0.1524" layer="91"/>
<label x="15.24" y="73.66" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="1"/>
</segment>
<segment>
<wire x1="15.24" y1="-76.2" x2="7.62" y2="-76.2" width="0.1524" layer="91"/>
<label x="15.24" y="-76.2" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="U$2" gate="G$1" pin="OUT"/>
</segment>
</net>
<net name="GP4" class="0">
<segment>
<wire x1="22.86" y1="55.88" x2="2.54" y2="55.88" width="0.1524" layer="91"/>
<label x="2.54" y="55.88" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="15"/>
</segment>
</net>
<net name="GP12" class="0">
<segment>
<wire x1="15.24" y1="68.58" x2="22.86" y2="68.58" width="0.1524" layer="91"/>
<label x="15.24" y="68.58" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="JP1" gate="A" pin="5"/>
</segment>
<segment>
<wire x1="157.48" y1="17.78" x2="149.86" y2="17.78" width="0.1524" layer="91"/>
<label x="157.48" y="17.78" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="DIO4"/>
</segment>
</net>
<net name="ID_SD" class="0">
<segment>
<wire x1="30.48" y1="66.04" x2="40.64" y2="66.04" width="0.1524" layer="91"/>
<label x="40.64" y="66.04" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="8"/>
</segment>
</net>
<net name="GP23" class="0">
<segment>
<wire x1="53.34" y1="63.5" x2="30.48" y2="63.5" width="0.1524" layer="91"/>
<label x="53.34" y="63.5" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="10"/>
</segment>
<segment>
<wire x1="177.8" y1="-76.2" x2="154.94" y2="-76.2" width="0.1524" layer="91"/>
<label x="177.8" y="-76.2" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO33/A1_5/X32N"/>
</segment>
</net>
<net name="ID_SC" class="0">
<segment>
<wire x1="53.34" y1="68.58" x2="30.48" y2="68.58" width="0.1524" layer="91"/>
<label x="53.34" y="68.58" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="6"/>
</segment>
</net>
<net name="GP6" class="0">
<segment>
<wire x1="53.34" y1="73.66" x2="30.48" y2="73.66" width="0.1524" layer="91"/>
<label x="53.34" y="73.66" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="2"/>
</segment>
<segment>
<wire x1="45.72" y1="-48.26" x2="22.86" y2="-48.26" width="0.1524" layer="91"/>
<label x="45.72" y="-48.26" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="LED2" gate="G$1" pin="DI"/>
</segment>
</net>
<net name="GP5" class="0">
<segment>
<wire x1="40.64" y1="71.12" x2="30.48" y2="71.12" width="0.1524" layer="91"/>
<label x="40.64" y="71.12" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="JP1" gate="A" pin="4"/>
</segment>
<segment>
<wire x1="154.94" y1="25.4" x2="149.86" y2="25.4" width="0.1524" layer="91"/>
<label x="154.94" y="25.4" size="1.778" layer="95" rot="MR180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="DIO1"/>
</segment>
</net>
<net name="MOSI" class="0">
<segment>
<wire x1="119.38" y1="22.86" x2="124.46" y2="22.86" width="0.1524" layer="91"/>
<label x="119.38" y="22.86" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="MOSI"/>
</segment>
<segment>
<wire x1="154.94" y1="-63.5" x2="177.8" y2="-63.5" width="0.1524" layer="91"/>
<label x="177.8" y="-63.5" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO23"/>
</segment>
<segment>
<wire x1="-2.54" y1="7.62" x2="5.08" y2="7.62" width="0.1524" layer="91"/>
<label x="-2.54" y="7.62" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="M1" gate="G$1" pin="MOSI"/>
</segment>
</net>
<net name="MISO" class="0">
<segment>
<wire x1="104.14" y1="25.4" x2="124.46" y2="25.4" width="0.1524" layer="91"/>
<label x="104.14" y="25.4" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="MISO"/>
</segment>
<segment>
<wire x1="154.94" y1="-55.88" x2="162.56" y2="-55.88" width="0.1524" layer="91"/>
<label x="162.56" y="-55.88" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO19"/>
</segment>
<segment>
<wire x1="-20.32" y1="5.08" x2="5.08" y2="5.08" width="0.1524" layer="91"/>
<label x="-20.32" y="5.08" size="1.778" layer="95" rot="MR0" xref="yes"/>
<pinref part="M1" gate="G$1" pin="MISO"/>
</segment>
</net>
<net name="SCK" class="0">
<segment>
<wire x1="104.14" y1="20.32" x2="124.46" y2="20.32" width="0.1524" layer="91"/>
<label x="104.14" y="20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="SCK"/>
</segment>
<segment>
<wire x1="-20.32" y1="10.16" x2="5.08" y2="10.16" width="0.1524" layer="91"/>
<label x="-20.32" y="10.16" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="M1" gate="G$1" pin="SCK"/>
</segment>
<segment>
<wire x1="154.94" y1="-53.34" x2="177.8" y2="-53.34" width="0.1524" layer="91"/>
<label x="177.8" y="-53.34" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO18"/>
</segment>
</net>
<net name="IRQ" class="0">
<segment>
<wire x1="-2.54" y1="2.54" x2="5.08" y2="2.54" width="0.1524" layer="91"/>
<label x="-2.54" y="2.54" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="M1" gate="G$1" pin="IRQ"/>
</segment>
</net>
<net name="N$2" class="0">
<segment>
<wire x1="149.86" y1="10.16" x2="160.02" y2="10.16" width="0.1524" layer="91"/>
<pinref part="U1" gate="G$1" pin="ANT"/>
<pinref part="TP2" gate="G$1" pin="TP"/>
</segment>
</net>
<net name="R_RESET" class="0">
<segment>
<wire x1="104.14" y1="15.24" x2="124.46" y2="15.24" width="0.1524" layer="91"/>
<label x="104.14" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="RESET"/>
</segment>
<segment>
<wire x1="203.2" y1="30.48" x2="203.2" y2="22.86" width="0.1524" layer="91"/>
<label x="203.2" y="30.48" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="10K1" gate="G$1" pin="2"/>
</segment>
<segment>
<wire x1="162.56" y1="-43.18" x2="154.94" y2="-43.18" width="0.1524" layer="91"/>
<label x="162.56" y="-43.18" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO14/A2_6"/>
</segment>
</net>
<net name="DIO0" class="0">
<segment>
<wire x1="162.56" y1="22.86" x2="149.86" y2="22.86" width="0.1524" layer="91"/>
<label x="162.56" y="22.86" size="1.778" layer="95" xref="yes"/>
<pinref part="U1" gate="G$1" pin="DIO0"/>
</segment>
<segment>
<wire x1="154.94" y1="-30.48" x2="162.56" y2="-30.48" width="0.1524" layer="91"/>
<label x="162.56" y="-30.48" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO2/A2_2"/>
</segment>
</net>
<net name="CE" class="0">
<segment>
<label x="-20.32" y="15.24" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="5.08" y1="15.24" x2="-20.32" y2="15.24" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="CE"/>
</segment>
<segment>
<label x="177.8" y="-33.02" size="1.778" layer="95" xref="yes"/>
<wire x1="154.94" y1="-33.02" x2="177.8" y2="-33.02" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="IO4/A2_0"/>
</segment>
</net>
<net name="CS_NRF" class="0">
<segment>
<label x="-2.54" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="5.08" y1="12.7" x2="-2.54" y2="12.7" width="0.1524" layer="91"/>
<pinref part="M1" gate="G$1" pin="CSN"/>
</segment>
<segment>
<label x="177.8" y="-38.1" size="1.778" layer="95" xref="yes"/>
<wire x1="154.94" y1="-38.1" x2="177.8" y2="-38.1" width="0.1524" layer="91"/>
<pinref part="X1" gate="G$1" pin="IO12/A2_5"/>
</segment>
</net>
<net name="IO0" class="0">
<segment>
<wire x1="208.28" y1="-78.74" x2="220.98" y2="-78.74" width="0.1524" layer="91"/>
<label x="208.28" y="-78.74" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="PAD3" gate="1" pin="P"/>
</segment>
<segment>
<wire x1="177.8" y1="-27.94" x2="154.94" y2="-27.94" width="0.1524" layer="91"/>
<label x="177.8" y="-27.94" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO0/A2_1"/>
</segment>
</net>
<net name="EN" class="0">
<segment>
<wire x1="203.2" y1="-86.36" x2="220.98" y2="-86.36" width="0.1524" layer="91"/>
<label x="203.2" y="-86.36" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="PAD7" gate="1" pin="P"/>
</segment>
<segment>
<wire x1="96.52" y1="-43.18" x2="104.14" y2="-43.18" width="0.1524" layer="91"/>
<label x="96.52" y="-43.18" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="X1" gate="G$1" pin="EN"/>
</segment>
<segment>
<label x="215.9" y="-55.88" size="1.778" layer="95" rot="R270" xref="yes"/>
<wire x1="215.9" y1="-45.72" x2="215.9" y2="-55.88" width="0.1524" layer="91"/>
<pinref part="R5_10K5" gate="G$1" pin="1"/>
</segment>
</net>
<net name="CS_LORA" class="0">
<segment>
<wire x1="124.46" y1="17.78" x2="119.38" y2="17.78" width="0.1524" layer="91"/>
<label x="119.38" y="17.78" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="NSS"/>
</segment>
<segment>
<wire x1="154.94" y1="-35.56" x2="162.56" y2="-35.56" width="0.1524" layer="91"/>
<label x="162.56" y="-35.56" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO5"/>
</segment>
<segment>
<wire x1="195.58" y1="22.86" x2="195.58" y2="30.48" width="0.1524" layer="91"/>
<label x="195.58" y="30.48" size="1.778" layer="95" rot="R90" xref="yes"/>
<pinref part="10K2" gate="G$1" pin="2"/>
</segment>
</net>
<net name="DIO3" class="0">
<segment>
<pinref part="U1" gate="G$1" pin="DIO3"/>
<wire x1="149.86" y1="15.24" x2="172.72" y2="15.24" width="0.1524" layer="91"/>
<label x="172.72" y="15.24" size="1.778" layer="95" xref="yes"/>
</segment>
<segment>
<wire x1="154.94" y1="-58.42" x2="177.8" y2="-58.42" width="0.1524" layer="91"/>
<label x="177.8" y="-58.42" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO21"/>
</segment>
</net>
<net name="DIO5" class="0">
<segment>
<wire x1="124.46" y1="12.7" x2="119.38" y2="12.7" width="0.1524" layer="91"/>
<label x="119.38" y="12.7" size="1.778" layer="95" rot="R180" xref="yes"/>
<pinref part="U1" gate="G$1" pin="DIO5"/>
</segment>
</net>
<net name="3.3V" class="0">
<segment>
<label x="-15.24" y="-20.32" size="1.778" layer="95" rot="R180" xref="yes"/>
<wire x1="-2.54" y1="-20.32" x2="-15.24" y2="-20.32" width="0.1524" layer="91"/>
<pinref part="LED1" gate="G$1" pin="VDD"/>
</segment>
</net>
<net name="LED" class="0">
<segment>
<wire x1="22.86" y1="-20.32" x2="40.64" y2="-20.32" width="0.1524" layer="91"/>
<label x="40.64" y="-20.32" size="1.778" layer="95" xref="yes"/>
<pinref part="LED1" gate="G$1" pin="DI"/>
</segment>
<segment>
<wire x1="154.94" y1="-60.96" x2="162.56" y2="-60.96" width="0.1524" layer="91"/>
<label x="162.56" y="-60.96" size="1.778" layer="95" xref="yes"/>
<pinref part="X1" gate="G$1" pin="IO22"/>
</segment>
</net>
</nets>
</sheet>
</sheets>
</schematic>
</drawing>
<compatibility>
<note version="6.3" minversion="6.2.2" severity="warning">
Since Version 6.2.2 text objects can contain more than one line,
which will not be processed correctly with this version.
</note>
<note version="8.2" severity="warning">
Since Version 8.2, EAGLE supports online libraries. The ids
of those online libraries will not be understood (or retained)
with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports URNs for individual library
assets (packages, symbols, and devices). The URNs of those assets
will not be understood (or retained) with this version.
</note>
<note version="8.3" severity="warning">
Since Version 8.3, EAGLE supports the association of 3D packages
with devices in libraries, schematics, and board files. Those 3D
packages will not be understood (or retained) with this version.
</note>
<note version="8.4" severity="warning">
Since Version 8.4, EAGLE supports properties for SPICE simulation. 
Probes in schematics and SPICE mapping objects found in parts and library devices
will not be understood with this version. Update EAGLE to the latest version
for full support of SPICE simulation. 
</note>
</compatibility>
</eagle>
