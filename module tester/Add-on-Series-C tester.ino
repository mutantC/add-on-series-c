/*
  If your serial output has these values same then Your nrf24l01 module is in working condition :
  
  EN_AA          = 0x3f
  EN_RXADDR      = 0x02
  RF_CH          = 0x4c
  RF_SETUP       = 0x03
  CONFIG         = 0x0f

  As for the lora module you can see the output in the serial output if it's working or not. 
 */

// Code v1.2
// Add-on-Series-C v1.4

#include <SPI.h>
#include <RF24.h>
#include <printf.h>
#include <LoRa.h>

//define the pins used by the transceiver module
#define ss 5
#define rst 14
#define dio0 2

RF24 radio(4, 12);

byte addresses[][6] = {"1Node", "2Node"};


void setup() {
  
  //initialize Serial Monitor
  Serial.begin(115200);
  while (!Serial);
  Serial.println("LoRa and nrf24l01 module tester");

  radio.begin();
  radio.setPALevel(RF24_PA_LOW);
  radio.openWritingPipe(addresses[0]);
  radio.openReadingPipe(1, addresses[1]); 
  radio.startListening();
  Serial.println("NRF24l01 - Match these values with the given value in the code to see if  module is working");

  printf_begin();

  radio.printDetails();

  Serial.println("");
  Serial.println("");
  
  //setup LoRa transceiver module
  LoRa.setPins(ss, rst, dio0);
  
  //replace the LoRa.begin(---E-) argument with your location's frequency 
  //433E6 for Asia
  //866E6 for Europe
  //915E6 for North America
  while (!LoRa.begin(866E6)) {
    Serial.println("Wating from Lora module to respond");
    Serial.println("Maybe Lora module is missing");
    delay(500);
  }
  // Change sync word (0xF3) to match the receiver
  // The sync word assures you don't get LoRa messages from other LoRa transceivers
  // ranges from 0-0xFF
  Serial.println("");
  Serial.println("");
  LoRa.setSyncWord(0xF3);
  Serial.println("LoRa - Module is working");
}

void loop() {
//  empty

}

// rahmanshaber
// https://dhirajkushwaha.com/elekkrypt
